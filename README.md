# Personalised Gamified Health Assistant App

[![java documents](https://sc17jw.gitlab.io/individual-project-comp-3931-dissertation/MarkDownDocuments/javadocs.png)](https://bit.ly/2KPcRTx)
[![pipeline status](https://gitlab.com/sc17jw/individual-project-comp-3931-dissertation/badges/master/pipeline.svg)](https://gitlab.com/sc17jw/individual-project-comp-3931-dissertation/-/commits/master)
[![coverage report](https://gitlab.com/sc17jw/individual-project-comp-3931-dissertation/badges/master/coverage.svg)](https://gitlab.com/sc17jw/individual-project-comp-3931-dissertation)

This is a proof of concept for a user adaptive system, adapting to the users gamer type based upon their notes. 
This PoC also provides the user with quantified self; where the users reflect upon collected data about themselves. This data is collected from Samsung health and displayed to the user. Following the growing trend of quantified self apps in the forever growing network of IoT devices gathering data. 
#
These are the features of the PoC:
  - Provides a test for the user's gamer type
  
  - Adapts to user's gamer type due to game type  
  - Provides gamer type implemention features for:
    - Achiever
    - Player
    - Free Spirit
    - Socialiser
  - Provides visual aids to assist in quantified self 
  - Collecting and processing Health data from SHealth
  - Suggesting meals based upon users age and gender with occordance to government guidelines 
  - Adding the meal to SHealth
  - User diary with machine learning anlsyis allwoing a method of adaption
  - Displaying Health data from SHealth in graphs


### Technologies Used

The PoC makes uses of the following API's:

* [Fire-Base](https://firebase.google.com/) - For User Authentication 
* [Saftey-Net](https://developer.android.com/training/safetynet) - For Device And User Verification
* [MP Android](https://github.com/PhilJay/MPAndroidChart) - For Graph Generation
* [Snap-Kit](https://kit.snapchat.com/) - For Snapchat Intergration And Bitemojie 
* [SHealth SDK](https://developer.samsung.com/health/android/overview.html) - For Samsung Health Intergration
* [Samsung Accessory SDK](https://developer.samsung.com/health/android/overview.html) - For Conneting To Samsung Watches
* [Root Beer](https://github.com/scottyab/rootbeer) - For Root And BusyBox Detection As A Secondary Method
* [Volley](https://github.com/scottyab/rootbeer) - For REST API Calls
* [Picasso](https://github.com/scottyab/rootbeer) - For Loading Images
* [Nimbus JWT](https://bitbucket.org/connect2id/nimbus-jose-jwt/wiki/Home) - For JWT Phasing ( Due To No Server For SafteyNet Verification)
* [Jetpack](https://developer.android.com/jetpack/docs/getting-started) - For Better UI Navigation And Security Features
* [Android Material](https://developer.android.com/guide/topics/ui/look-and-feel) - For Better Looking UI
* [Java Standered Libraries](https://docs.oracle.com/javase/7/docs/api/) - For Basic Functionality

#
#
### Installation
You have two methods of installation you can either download and install the precompiled apk files or compile it yourself. 

#### Method 1
To get the pipeline artefacts and precompile apk file you need to go to this link and download the latest assembleDebug artefacts:

[PIPELINE ARTIFACTS](https://gitlab.com/sc17jw/individual-project-comp-3931-dissertation/-/commits/master)

From this, you can extract the archive and navigate to the apk. 

You can either install the application thought ADB on a terminal or copy the apk file to the device and navigate to the apk and install. 

If you don't have ADB installed depending on your distribution or pc you can do one of the following:
##### Debian / Ubuntu or any distributions that use apt for their package manager

```sh
sudo apt update && sudo apt install adb
```

then you can execute ADB using 
```sh
adb command
```
where the command is the command, you want to execute. 

##### Fordora / OpenSuse

````sh
sudo yum install android-tools
````

then you can execute adb using 

```sh
adb command
```
where the command is the command you want to execute. 

##### macOS
download the latest platform-tools
https://dl.google.com/android/repository/platform-tools-latest-darwin.zip
extract the zip.
open a terminal and cd to the extraction location.
```sh
adb command
```
where the command is the command you want to execute. 

##### Windows: 
download the latest platform-tools
https://dl.google.com/android/repository/platform-tools-latest-windows.zip
extract and hold shift-click and open a new command window. 
then type the following to use ADB
```bat
adb.exe command
```
where the command is the command you want to execute. 
##### Other Linux: 
download the latest platform-tools
https://dl.google.com/android/repository/platform-tools-latest-linux.zip
extract and open a new terminal navigating to the location.
you may need to give the executable execution permissions using chmod as shown
```sh
chmod +x adb
./adb devices
```
##### ADB Install 
If there is only one device.
```sh
adb install -t debug.apk
```
If you know your devices serial number replacing $deviceSerialNumber with the number. 
```sh
adb install -t -s $deviceSerialNumber debug.apk
```
If you don't know your devices serial number you can check with, this 
```sh
adb devices
```
USB debugging may need enabling in the developer settings 
For this, you first go to settings then About Phone. 

From this, you got to Software Information.

Tap build number 7 times or until your heart joy. 

Then developer settings will be enabled if you press the back arrow twice. 

Selecting Developer settings will give you a list of settings. Search in the top box USB Debugging

Finally select enable. Now you should be able to install through adb when you allow the connection to your computer. 

#### Method 2

If you want to compile the PoC then it requires Android Studios to compile and install: 

First clone the repo

```sh
$ cd ~/ && git clone https://gitlab.com/sc17jw/individual-project-comp-3931-dissertation.git 
```
If you have already installed android studios you can now directory open, the project

```sh
$ cd ~/individual-project-comp-3931-dissertation && android-studio .
```

## If you don't have Android Studios installed:

First download android studios using your prefered browser

```sh
$ firefox https://developer.android.com/studio 
```
```sh
$ chromium-browser https://developer.android.com/studio 
```
or 

```sh
$ elinks https://developer.android.com/studio 
```
Then agree to the ULA and wait for the download to be complete.
#
Next, extract and run android studios. Replacing $VERSION_NUMBER with the latest version you downloaded

```sh
$ tar -zxvf android-studio-ide-$VERSION_NUMBER-linux.tar.gz && cd android-studio/bin
$ chmod +x studio.sh
$ ./studio.sh
```
We then can load the project as shown:

![Open Project Pictrue 1](https://sc17jw.gitlab.io/individual-project-comp-3931-dissertation/MarkDownDocuments/AndroidInstall/openProject2.png)

Navigate to git clone repo directory

![Open Project Picture 2](https://sc17jw.gitlab.io/individual-project-comp-3931-dissertation/MarkDownDocuments/AndroidInstall/openProject1.png)
## Compiling and Dependencies
Then wait for Android Studios To Download the dependencies and SDK version required. 
If the SDK does not install you can install it manually by doing the following:

First clicking on Tools then SDK Manager
then Navigate to the Android SDK option and click Android 10

![Picture To SDK Install](https://sc17jw.gitlab.io/individual-project-comp-3931-dissertation/MarkDownDocuments/AndroidInstall/AndroidSDK.png)

Then click Apply and then OK (If you have enough disk space)

![Picture To SDK Install](https://sc17jw.gitlab.io/individual-project-comp-3931-dissertation/MarkDownDocuments/AndroidInstall/ConfirmSDK.png)

![Picture To SDK Install](https://sc17jw.gitlab.io/individual-project-comp-3931-dissertation/MarkDownDocuments/AndroidInstall/Installing.png)

Now you wait until the installer has finished.

## Running / Compiling

Then you can do one of the following: connect your android phone and click the play icon, Use the build-in emulator to run the application or Compile to an APK to install.

Select The target device 

![Select Device Picture](https://sc17jw.gitlab.io/individual-project-comp-3931-dissertation/MarkDownDocuments/AndroidInstall/selectDevice.png)

Click the play button or press shift + F10

![Run App picture](https://sc17jw.gitlab.io/individual-project-comp-3931-dissertation/MarkDownDocuments/AndroidInstall/runApp.png) 

## Distribution

You can generate a signed apk or bundle package. This can then be distributed to uses thought the play store or android. 

As shown here. 

```sh
$ cd ~/individual-project-comp-3931-dissertation && android-studio .
```
Assuming you have generated a signing key

![Apk Generation picture](https://sc17jw.gitlab.io/individual-project-comp-3931-dissertation/MarkDownDocuments/AndroidInstall/apkGeneration.png) 

Then distribute.

