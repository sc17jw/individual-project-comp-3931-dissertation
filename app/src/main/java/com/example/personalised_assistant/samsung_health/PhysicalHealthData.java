package com.example.personalised_assistant.samsung_health;

import android.util.Log;

import com.samsung.android.sdk.healthdata.HealthConstants;
import com.samsung.android.sdk.healthdata.HealthConstants.HeartRate;
import com.samsung.android.sdk.healthdata.HealthDataResolver;
import com.samsung.android.sdk.healthdata.HealthDataStore;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static com.samsung.android.sdk.healthdata.HealthConstants.SessionMeasurement.END_TIME;
import static com.samsung.android.sdk.healthdata.HealthConstants.SessionMeasurement.START_TIME;
import static com.samsung.android.sdk.healthdata.HealthConstants.SessionMeasurement.TIME_OFFSET;
import static com.samsung.android.sdk.healthdata.HealthDataResolver.Filter;
import static com.samsung.android.sdk.healthdata.HealthDataResolver.ReadRequest;

/**
 * This class provides sleep and hart rate data querying from samsung health
 */
class PhysicalHealthData {
    /**
     * These are temporary variables to store the current sleep data.
     */
    private static int sleepStreak =0;
    /**
     * This is a temp variable to store max sleep steak
     */
    private static int maxSleepStreak =0;

    /**
     * A public Observer for importing samsung health recorded heart rate data in a format of a json array.
     * This is used during setup
     */
    public interface ImportHeartObserver { void onChanged(JSONArray heartDataJsonArray);}

    /**
     * A public Observer for importing samsung health recorded sleep data in a format of a json array, int max sleep streak and int the current sleep streak
     * This is used during setup
     */
    public interface ImportSleepObserver { void onChanged(JSONArray sleepDataJsonArray, int max , int currentStreak);}

    /**
     * A public Observer for getting the sleep data since the last time it was checked since the last time the app opened.
     * When called it will update the user profile
     */
    public interface GetSinceLastSleepObserver { void onChanged(JSONArray sleepDataJsonArray, int max , int currentStreak);}

    /**
     * A public Observer for getting the heart data since the last time it was checked since the last time the app opened.
     * When called it will update the user profile
     */
    public interface GetSinceLastHeartDataObserver { void onChanged(JSONArray heartDataArray);}

    /**
     * This function imports the users hart rate data from samsung health. It puts the data into a json array format that is then processed and added to the user profile
     * @param listener This is the ImportHeartObserver public interface and is used to pass and process data when the query is complete
     */
    static void importHeartRate(HealthDataStore samsungHealthDataStore, ImportHeartObserver listener) {
        ReadRequest request = new ReadRequest.Builder()
                .setDataType(HeartRate.HEALTH_DATA_TYPE)
                .setProperties(new String[]{
                        HeartRate.HEART_BEAT_COUNT,
                        HeartRate.HEART_RATE,
                        HeartRate.MAX,
                        HeartRate.MIN,
                        START_TIME,
                        END_TIME,
                        HeartRate.MIN,
                })
                .build();
        new HealthDataResolver(samsungHealthDataStore, null).read(request).setResultListener(result -> {
            JSONArray hart_rate_graph = new JSONArray();
            for (com.samsung.android.sdk.healthdata.HealthData data : result) {
                try {
                    JSONObject hart_rate = new JSONObject();
                    hart_rate.put("HEART_BEAT_COUNT", data.getInt(HeartRate.HEART_BEAT_COUNT));
                    hart_rate.put("HEART_RATE", data.getFloat(HeartRate.HEART_RATE));
                    hart_rate.put("MAX", data.getLong(HeartRate.MAX));
                    hart_rate.put("MIN", data.getLong(HeartRate.MIN));
                    hart_rate.put("start_time", data.getLong(START_TIME));
                    hart_rate.put("end_time", data.getLong(END_TIME));
                    hart_rate_graph.put(hart_rate);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            listener.onChanged(hart_rate_graph);
            result.close();

        });

    }

    /**
     * This function imports the users sleep data from samsung health. It puts the data into a json array format that is then processed and added to the user profile.
     * The json array has JSON objects which contain sleep_time (The time the user sleeps for), start_time ( The time the user started to sleep), start_time (The time the user woke up)
     * @param listener This is the ImportSleepObserver public interface and is used to pass and process data when the query is complete
     */
    static void importSleep(HealthDataStore samsungHealthDataStore, ImportSleepObserver listener) {
        ReadRequest request = new ReadRequest.Builder()
                .setDataType(HealthConstants.Sleep.HEALTH_DATA_TYPE)
                .build();
        sleepStreak = 0; maxSleepStreak = 0;
        new HealthDataResolver(samsungHealthDataStore, null).read(request).setResultListener(result -> {
            JSONArray sleep_hours = new JSONArray();
            try {
                for (com.samsung.android.sdk.healthdata.HealthData data : result) {
                    JSONObject sleep = new JSONObject();
                    String start = data.get(START_TIME).toString();
                    String end = data.get(END_TIME).toString();
                    long sleep_duration = Long.parseLong(end) - Long.parseLong(start);
                    if (sleep_duration >= 28800000) {
                        sleepStreak++;
                        if (sleepStreak > maxSleepStreak) {
                             maxSleepStreak = sleepStreak;
                        }
                    } else {
                        sleepStreak = 0;
                    }
                    sleep.put("sleep_time",((sleep_duration/1000L)/60)/60);
                    sleep.put("start_time",start);
                    sleep.put("end_Time",end);
                    sleep_hours.put(sleep);
                }
                listener.onChanged(sleep_hours, maxSleepStreak, sleepStreak);
                result.close();
            } catch(Exception e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * This function gets the hart_rate data over the specified time range
     * @param samsungHealthDataStore This is the connection to the samsung health database connected to by the SamsungHealth class
     * @param start_time This is the start time for the data you want to query
     * @param end_time This is the end time for the data you want to query
     */

     static void getHeartRate(HealthDataStore samsungHealthDataStore, long start_time, long end_time, GetSinceLastHeartDataObserver listener) {
        Filter filter = Filter.and(Filter.greaterThanEquals(HeartRate.START_TIME, start_time),
                Filter.lessThanEquals(HeartRate.START_TIME, end_time));

        ReadRequest request = new ReadRequest.Builder()
                .setDataType(HeartRate.HEALTH_DATA_TYPE)
                .setFilter(filter)
                .setProperties(new String[]{
                        HeartRate.HEART_BEAT_COUNT, HeartRate.HEART_RATE, HeartRate.MAX,
                        HeartRate.MIN, START_TIME, END_TIME, HeartRate.MIN,
                })
                .build();
        new HealthDataResolver(samsungHealthDataStore, null).read(request).setResultListener(result -> {
            JSONArray hart_rate_graph = new JSONArray();
            try {
            for (com.samsung.android.sdk.healthdata.HealthData data : result) {
                int heartBeatCount = data.getInt(HeartRate.HEART_BEAT_COUNT);
                long heartRate = data.getLong(HeartRate.HEART_RATE);
                Log.w("APP_TAG", "Heart beat count " + heartBeatCount
                        + ", heart rate " + heartRate + "bpm" + "max " + (data.getInt(HeartRate.MAX)) + "Min " +
                        (data.getInt(HeartRate.MIN)) + "Start " + data.getLong(START_TIME) + "End " + data.getLong(END_TIME));
                JSONObject hart_rate = new JSONObject();
                hart_rate.put("HEART_BEAT_COUNT", data.getInt(HeartRate.HEART_BEAT_COUNT));
                hart_rate.put("HEART_RATE", data.getFloat(HeartRate.HEART_RATE));
                hart_rate.put("MAX", data.getLong(HeartRate.MAX));
                hart_rate.put("MIN", data.getLong(HeartRate.MIN));
                hart_rate.put("start_time", data.getLong(START_TIME));
                hart_rate.put("end_time", data.getLong(END_TIME));
                hart_rate_graph.put(hart_rate);
            }
                listener.onChanged(hart_rate_graph);
                result.close();
            }catch(Exception e) {
                // Print stack trace for debugging.
                e.printStackTrace();
            }
    });

    }

    /**
     * This function gets the sleep data over the specified time range
     * @param samsungHealthDataStore This is the connection to the samsung health database connected to by the SamsungHealth class
     * @param start_time This is the start time for the data you want to query
     * @param end_time This is the end time for the data you want to query
     */
     static void getSleep(HealthDataStore samsungHealthDataStore, long start_time, long end_time, GetSinceLastSleepObserver listener) {
        start_time -= (24 * 60 * 60 * 1000L); // minuses a day to get the last sleep cycle
        ReadRequest request = new ReadRequest.Builder()
                .setDataType(HealthConstants.Sleep.HEALTH_DATA_TYPE)
                .setLocalTimeRange(START_TIME, TIME_OFFSET, start_time, end_time)
                .build();

        new HealthDataResolver(samsungHealthDataStore, null).read(request).setResultListener(result -> {
            JSONArray sleep_hours = new JSONArray();
            try {
                for (com.samsung.android.sdk.healthdata.HealthData data : result) {
                    JSONObject sleep = new JSONObject();
                    String start = data.get(START_TIME).toString();
                    String end = data.get(END_TIME).toString();
                    long sleep_duration = Long.parseLong(end) - Long.parseLong(start);
                    if (sleep_duration >= 28800000) {
                        sleepStreak++;
                        if (sleepStreak > maxSleepStreak) {
                            maxSleepStreak = sleepStreak;
                        }
                    } else {
                        sleepStreak = 0;
                    }
                    sleep.put("sleep_time",((sleep_duration/1000L)/60)/60);
                    sleep.put("start_time",start);
                    sleep.put("end_Time",end);
                    sleep_hours.put(sleep);
                }
                listener.onChanged(sleep_hours, maxSleepStreak, sleepStreak);
                result.close();
            } catch(Exception e) {
                e.printStackTrace();
            }
        });
    }
}
