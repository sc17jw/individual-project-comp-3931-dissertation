package com.example.personalised_assistant.samsung_health;

import android.app.Activity;

import com.example.personalised_assistant.R;
import com.example.personalised_assistant.utils.GlobalVariables;
import com.example.personalised_assistant.utils.MessageAndTextUtils;
import com.example.personalised_assistant.utils.UserProfile;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

/**
 * This classes updates the user profile with the data from the SamsungHealth class.
 */
class UpdateUserProfileSamsung {
    /**
     * THis method updates the user profile steps taken. It also checks if the step count overflows if so adds the presided achievement and resets the counter.
     * Theoretically this would be impossible due to the maximum value of long but better safe than sorry.
     * This also requires extra attention with its own method since the level system is based upon this data and a
     * extra achievement is made if the user overflows the long value
     *
     * @param step_count      This is the step count to add to the user profile.
     * @param globalVariables This is a pointer to the global variables object that allows us to update the user profile.
     * @param activity        This is the activity that allows us to display error messages.
     * @param Today           This is a Calender object that allows us to set the time stamp on the update.
     */
    static void updateProfileStep(long step_count, GlobalVariables globalVariables, Activity activity, Calendar Today) {
        try {
            long steps_from_profile = Long.parseLong(globalVariables.getUserProfile().get("total_steps").toString());
            if (step_count < 0) {
                MessageAndTextUtils.alert(activity, null, activity.getString(R.string.prestidge_text), activity.getString(R.string.SamsungHealth));
                step_count = 0;
                UserProfile.add_achievements("Prestiged", activity);
                globalVariables.insertKeyIntoProfile("total_steps", String.valueOf(step_count), activity);
            }
            globalVariables.insertKeyIntoProfile("total_steps", String.valueOf(steps_from_profile + step_count), activity);
            globalVariables.insertKeyIntoProfile("last_step_time", String.valueOf(Today.getTimeInMillis()), activity);
        } catch (JSONException e) {
            MessageAndTextUtils.error_alert(activity, null,
                    activity.getString(R.string.FailedToUpdate),
                    activity.getString(R.string.SamsungHealth));
        }
    }

    /**
     * This method will update the user profile with the provided JSONArray by getting adding to the end of the existing array from the profile.
     * This value is then added back to the user profile
     *
     * @param key              This is the key that we want to update in the json array for the user profile
     * @param updateTimeString This is the update time for the item you want to update
     * @param object           This is the JSONArray that will be iterate though adding the new keys to the user profile
     * @param globalVariables  This is a pointer to the global variables object that allows us to update the user profile.
     * @param activity         This is the activity that allows us to display error messages.
     * @param Today            This is a Calender object that allows us to set the time stamp on the update.
     */
    static void updateProfileJsonArray(String key, String updateTimeString, JSONArray object, GlobalVariables globalVariables, Activity activity, Calendar Today) {
        try {
            JSONArray from_profile = new JSONArray(globalVariables.getUserProfile().get(key).toString());
            for (int i = 0; i < object.length(); i++) {
                from_profile.put(object.get(i));
            }
            globalVariables.insertKeyIntoProfile(key, String.valueOf(from_profile), activity);
            globalVariables.insertKeyIntoProfile(updateTimeString, String.valueOf(Today.getTimeInMillis()), activity);
        } catch (Exception e) {
            e.printStackTrace();
            MessageAndTextUtils.error_alert(activity, null,
                    activity.getString(R.string.FailedToUpdate) + updateTimeString,
                    activity.getString(R.string.SamsungHealth));
        }
    }

    /**
     * This method will update the user profile with the value provided by getting the data from the profile and adding it to the value.
     * This value is then added back to the user profile
     *
     * @param key              This is the key that we want to update in the json array for the user profile
     * @param updateTimeString This is the update time for the item you want to update
     * @param value            This is the value you want to insert into the profile
     * @param globalVariables  This is a pointer to the global variables object that allows us to update the user profile.
     * @param activity         This is the activity that allows us to display error messages.
     * @param Today            This is a Calender object that allows us to set the time stamp on the update.
     */
    static void updateProfileValue(String key, String updateTimeString, double value, GlobalVariables globalVariables, Activity activity, Calendar Today) {
        try {
            double from_profile = globalVariables.getUserProfile().getDouble(key);
            globalVariables.insertKeyIntoProfile(key, String.valueOf(from_profile + value), activity);
            globalVariables.insertKeyIntoProfile(updateTimeString, String.valueOf(Today.getTimeInMillis()), activity);
        } catch (Exception e) {
            MessageAndTextUtils.error_alert(activity, null,
                    activity.getString(R.string.FailedToUpdate) + updateTimeString,
                    activity.getString(R.string.SamsungHealth));
        }
    }

    /**
     * This method will insert a json object into the profile
     *
     * @param key              This is the key that we want to update in the json array for the user profile
     * @param updateTimeString This is the update time for the item you want to update
     * @param value            This is the JSONObject you want to insert into the profile
     * @param globalVariables  This is a pointer to the global variables object that allows us to update the user profile.
     * @param activity         This is the activity that allows us to display error messages.
     * @param Today            This is a Calender object that allows us to set the time stamp on the update.
     */
    static void insertProfileJsonObject(String key, String updateTimeString,
                                        JSONObject value, GlobalVariables globalVariables,
                                        Activity activity, Calendar Today) {
        try {
            globalVariables.insertKeyIntoProfile(key, value.toString(), activity);
            globalVariables.insertKeyIntoProfile(updateTimeString, String.valueOf(Today.getTimeInMillis()), activity);
        } catch (Exception e) {
            MessageAndTextUtils.error_alert(activity, null,
                    activity.getString(R.string.FailedToUpdate) + updateTimeString,
                    activity.getString(R.string.SamsungHealth));
        }
    }

    /**
     * This method will insert a value into the profile
     *
     * @param key              This is the key that we want to update in the json array for the user profile
     * @param updateTimeString This is the update time for the item you want to update
     * @param value            This is the double you want to insert into the profile
     * @param globalVariables  This is a pointer to the global variables object that allows us to update the user profile.
     * @param activity         This is the activity that allows us to display error messages.
     * @param Today            This is a Calender object that allows us to set the time stamp on the update.
     */
    static void insertProfileValue(String key, String updateTimeString,
                                   String value, GlobalVariables globalVariables,
                                   Activity activity, Calendar Today) {
        try {
            globalVariables.insertKeyIntoProfile(key, value, activity);
            globalVariables.insertKeyIntoProfile(updateTimeString, String.valueOf(Today.getTimeInMillis()), activity);
        } catch (Exception e) {
            MessageAndTextUtils.error_alert(activity, null,
                    activity.getString(R.string.FailedToUpdate) + updateTimeString,
                    activity.getString(R.string.SamsungHealth));
        }
    }

}
