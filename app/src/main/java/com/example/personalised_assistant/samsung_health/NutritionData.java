package com.example.personalised_assistant.samsung_health;

import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.personalised_assistant.R;
import com.example.personalised_assistant.utils.MessageAndTextUtils;
import com.samsung.android.sdk.healthdata.HealthData;
import com.samsung.android.sdk.healthdata.HealthDataResolver;
import com.samsung.android.sdk.healthdata.HealthDataStore;
import com.samsung.android.sdk.healthdata.HealthDeviceManager;
import com.samsung.android.sdk.healthdata.HealthResultHolder;

import org.json.JSONObject;

import java.util.Calendar;
import java.util.TimeZone;

import static com.samsung.android.sdk.healthdata.HealthConstants.Nutrition;

/**
 * This class gets all the nutrition data eg Food intake and adds new food entries. This is class is used by SamsungHealth class.
 */
class NutritionData {
    /**
     * These are nutrition data and is used as temp storage
     */
    static private float CALORIE = 0, CALCIUM = 0, CARBOHYDRATE = 0, CHOLESTEROL = 0, DIETARY_FIBER = 0, IRON = 0,
            SATURATED_FAT = 0, POTASSIUM = 0, PROTEIN = 0, SODIUM = 0, SUGAR = 0, TOTAL_FAT = 0,
            TRANS_FAT = 0, VITAMIN_A = 0, VITAMIN_C = 0, active_burnt_cal = 0,
            resting_burnt_cal = 0, digestion_burnt_cal = 0;
    /**
     * This is the users current meal count and is used as a temp storage
     */
    static private int meal_count = 0;

    /**
     * This method is used for importing total consumed calories from samsung health.
     * It is used during setup
     *
     * @param samsungHealthDataStore This is the HealthStore object used to connect to the samsung health database and retrieve the data.
     * @param listener               This is the listener for processing the data once the data has been retrieved
     */
    static void importCalorieConsumed(HealthDataStore samsungHealthDataStore, ImportCaloriesConsumedObserver listener) {
        try {
            HealthDataResolver.ReadRequest request = new HealthDataResolver.ReadRequest.Builder()
                    .setDataType(Nutrition.HEALTH_DATA_TYPE)
                    .setProperties(new String[]{Nutrition.CALORIE})
                    .build();
            new HealthDataResolver(samsungHealthDataStore, null).read(request).setResultListener(result -> {
                float caloriesConsumed = 0;
                for (HealthData data : result) {
                    caloriesConsumed = caloriesConsumed + data.getFloat(Nutrition.CALORIE);
                }
                // notify the listener of the value
                listener.onChanged(caloriesConsumed);
                // Close's the connection
                result.close();
            });
        } catch (Exception e) {
            Log.e("exercise", "Error happened while reading exercise", e);
        }

    }

    /**
     * This method is used for importing total burned calories from samsung health.
     * It is used during setup
     *
     * @param samsungHealthDataStore This is the HealthStore object used to connect to the samsung health database and retrieve the data.
     * @param listener               This is the listener for processing the data once the data has been retrieved
     */
    static void importBurnedCalories(HealthDataStore samsungHealthDataStore, ImportBurnedCaloriesObserver listener) {
        HealthDataResolver.ReadRequest request = new HealthDataResolver.ReadRequest.Builder()
                .setDataType("com.samsung.shealth.calories_burned")
                .build();
        new HealthDataResolver(samsungHealthDataStore, null).read(request).setResultListener(result -> {
            float totalCaloriesBurned = 0;
            for (HealthData data : result) {
                totalCaloriesBurned = totalCaloriesBurned + data.getFloat("active_calorie");
                totalCaloriesBurned = totalCaloriesBurned + data.getFloat("rest_calorie");
                totalCaloriesBurned = totalCaloriesBurned + data.getFloat("tef_calorie");
            }
            listener.onChanged(totalCaloriesBurned);
            // Close's the connection

            result.close();
        });
    }

    /**
     * This method will get today's burned Calories
     *
     * @param samsungHealthDataStore This is the HealthStore object used to connect to the samsung health database and retrieve the data.
     * @param start_time             This is the start time from the user profile being the last time it was updated
     * @param listener               This is the listener for processing the data once the data has been retrieved
     */
    static void getTodayBurnedCalories(HealthDataStore samsungHealthDataStore, long start_time, GetTodayBurnedCaloriesObserver listener) {
        HealthDataResolver.ReadRequest request = new HealthDataResolver.ReadRequest.Builder()
                .setDataType("com.samsung.shealth.calories_burned")
                .setFilter(HealthDataResolver.Filter.eq("day_time", start_time))
                .build();

        new HealthDataResolver(samsungHealthDataStore, null).read(request).setResultListener(result -> {
            for (HealthData data : result) {
                active_burnt_cal = +data.getFloat("active_calorie");
                resting_burnt_cal = +data.getFloat("rest_calorie");
                digestion_burnt_cal = +data.getFloat("tef_calorie");
            }
            listener.onChanged(new float[]{
                    active_burnt_cal,
                    resting_burnt_cal,
                    digestion_burnt_cal
            });
            result.close();
        });
    }

    /**
     * This method will get the users current consumed food and meals
     *
     * @param samsungHealthDataStore This is the HealthStore object used to connect to the samsung health database and retrieve the data.
     * @param start_time             This is the start time from the user profile being the last time it was updated
     * @param end_time               this is the current time in milliseconds
     * @param listener               This is the listener for processing the data once the data has been retrieved
     */
    static void getFoodNutritionInformation(HealthDataStore samsungHealthDataStore,
                                            long start_time, long end_time,
                                            GetFoodNutritionDataObserver listener) {
        JSONObject nutrition = new JSONObject();
        //sends a request for the data from samsung health
        HealthDataResolver.ReadRequest request = new HealthDataResolver.ReadRequest.Builder()
                .setDataType(Nutrition.HEALTH_DATA_TYPE)
                .setProperties(new String[]{Nutrition.CALORIE, Nutrition.CALCIUM, Nutrition.CARBOHYDRATE,
                        Nutrition.CHOLESTEROL, Nutrition.DIETARY_FIBER, Nutrition.IRON, Nutrition.SATURATED_FAT,
                        Nutrition.POTASSIUM, Nutrition.PROTEIN, Nutrition.SODIUM, Nutrition.SUGAR,
                        Nutrition.TOTAL_FAT, Nutrition.TRANS_FAT, Nutrition.VITAMIN_A, Nutrition.VITAMIN_C,
                        Nutrition.MEAL_TYPE})
                .setLocalTimeRange(Nutrition.CREATE_TIME, Nutrition.TIME_OFFSET, start_time, end_time)
                .build();
        // Gets the nutrition data for the time frame selected
        new HealthDataResolver(samsungHealthDataStore, null).read(request).setResultListener(result -> {
            for (HealthData data : result) {
                CALORIE += data.getFloat(Nutrition.CALORIE);
                CALCIUM += data.getFloat(Nutrition.CALCIUM);
                CARBOHYDRATE += data.getFloat(Nutrition.CARBOHYDRATE);
                CHOLESTEROL += data.getFloat(Nutrition.CHOLESTEROL);
                DIETARY_FIBER += data.getFloat(Nutrition.DIETARY_FIBER);
                IRON += data.getFloat(Nutrition.IRON);
                SATURATED_FAT += data.getFloat(Nutrition.SATURATED_FAT);
                POTASSIUM += data.getFloat(Nutrition.POTASSIUM);
                PROTEIN += data.getFloat(Nutrition.PROTEIN);
                SODIUM += data.getFloat(Nutrition.SODIUM);
                SUGAR += data.getFloat(Nutrition.SUGAR);
                TOTAL_FAT += data.getFloat(Nutrition.TOTAL_FAT);
                TRANS_FAT += data.getFloat(Nutrition.TRANS_FAT);
                VITAMIN_A += data.getFloat(Nutrition.VITAMIN_A);
                VITAMIN_C += data.getFloat(Nutrition.VITAMIN_C);
                int MEAL_TYPE = data.getInt(Nutrition.MEAL_TYPE);
                if (MEAL_TYPE == Nutrition.MEAL_TYPE_BREAKFAST || MEAL_TYPE == Nutrition.MEAL_TYPE_DINNER || MEAL_TYPE == Nutrition.MEAL_TYPE_LUNCH) {
                    meal_count++;
                }
            }
            // Trys to insert the values of the data got in the above itter loop into a json object.
            try {
                nutrition.put("CALORIE", String.valueOf(CALORIE));
                nutrition.put("CALCIUM", String.valueOf(CALCIUM));
                nutrition.put("CARBOHYDRATE", String.valueOf(CARBOHYDRATE));
                nutrition.put("CHOLESTEROL", String.valueOf(CHOLESTEROL));
                nutrition.put("DIETARY_FIBER", String.valueOf(DIETARY_FIBER));
                nutrition.put("IRON", String.valueOf(IRON));
                nutrition.put("SATURATED_FAT", String.valueOf(SATURATED_FAT));
                nutrition.put("POTASSIUM", String.valueOf(POTASSIUM));
                nutrition.put("PROTEIN", String.valueOf(PROTEIN));
                nutrition.put("SODIUM", String.valueOf(SODIUM));
                nutrition.put("SUGAR", String.valueOf(SUGAR));
                nutrition.put("TOTAL_FAT", String.valueOf(TOTAL_FAT));
                nutrition.put("TRANS_FAT", String.valueOf(TRANS_FAT));
                nutrition.put("VITAMIN_A", String.valueOf(VITAMIN_A));
                nutrition.put("VITAMIN_C", String.valueOf(VITAMIN_C));
                nutrition.put("MEAL_COUNT", String.valueOf(meal_count));
            } catch (Exception e) {
                Log.e("exercise", "Error happened while reading exercise", e);
            }
            listener.onChanged(nutrition);
            result.close();
        });
    }

    /**
     * This method will add food to samsung health
     *
     * @param samsungHealthDataStore This is the HealthStore object used to connect to the samsung health database and retrieve the data.
     * @param foodItem               This is a json object of the food item the user wants to add in the format
     * @param activity               This is a pointer to the current activity that allows us to show a error message.
     * @param add_too_samsung_food   This allows us to remove the button when adding food or allow the user to retry adding,
     */
    static void addFood(HealthDataStore samsungHealthDataStore, JSONObject foodItem, Activity activity, Button add_too_samsung_food) {
        try {
            HealthDataResolver resolver = new HealthDataResolver(samsungHealthDataStore, null);
            HealthDataResolver.InsertRequest request = new HealthDataResolver.InsertRequest.Builder().setDataType(Nutrition.HEALTH_DATA_TYPE).build();
            HealthData data = new HealthData();

            data.putFloat(Nutrition.CALORIE, ((float) foodItem.getDouble("calories")));
            data.putInt(Nutrition.MEAL_TYPE, SamsungHealthConstants.getMealType());
            data.putLong(Nutrition.START_TIME, Calendar.getInstance().getTimeInMillis());
            data.putLong(Nutrition.TIME_OFFSET, TimeZone.getDefault().getOffset(Calendar.getInstance().getTimeInMillis()));

            data.putString(Nutrition.TITLE, foodItem.getString("title"));
            data.putFloat(Nutrition.TOTAL_FAT, (Float.parseFloat(foodItem.getString("fat").split("g")[0])));
            data.putFloat(Nutrition.SATURATED_FAT, (Float.parseFloat(foodItem.getString("saturatedFat").split("g")[0])));
            data.putFloat(Nutrition.POLYSATURATED_FAT, 0);
            data.putFloat(Nutrition.MONOSATURATED_FAT, 0);
            data.putFloat(Nutrition.TRANS_FAT, 0);
            data.putFloat(Nutrition.CARBOHYDRATE, (Float.parseFloat(foodItem.getString("carbs").split("g")[0])));
            data.putFloat(Nutrition.DIETARY_FIBER, (Float.parseFloat(foodItem.getString("fiber").split("g")[0])));
            data.putFloat(Nutrition.SUGAR, (Float.parseFloat(foodItem.getString("sugar").split("g")[0])));
            data.putFloat(Nutrition.PROTEIN, (Float.parseFloat(foodItem.getString("protein").split("g")[0])));
            data.putFloat(Nutrition.CHOLESTEROL, 0);
            data.putFloat(Nutrition.SODIUM, (Float.parseFloat(foodItem.getString("sodium").split("m")[0])));
            data.putFloat(Nutrition.POTASSIUM, (Float.parseFloat(foodItem.getString("potassium").split("m")[0])));
            data.putFloat(Nutrition.VITAMIN_A, ((Float.parseFloat(foodItem.getString("vitaminA").split("I")[0]))));
            data.putFloat(Nutrition.VITAMIN_C, (Float.parseFloat(foodItem.getString("vitaminC").split("m")[0])));
            data.putFloat(Nutrition.CALCIUM, (Float.parseFloat(foodItem.getString("calcium").split("m")[0])));
            data.putFloat(Nutrition.IRON, (Float.parseFloat(foodItem.getString("iron").split("m")[0])));

            data.setSourceDevice(new HealthDeviceManager(samsungHealthDataStore).getLocalDevice().getUuid());

            request.addHealthData(data);

            resolver.insert(request).setResultListener(baseResult -> {
                if (baseResult.getStatus() != HealthResultHolder.BaseResult.STATUS_SUCCESSFUL) {
                    MessageAndTextUtils.error_alert(activity, null, "Error While adding food item. \n Error Code : " + baseResult.getStatus(), activity.getString(R.string.SamsungHealth));
                    add_too_samsung_food.setClickable(true);
                } else {
                    MessageAndTextUtils.alert(activity, null, "Added food item to Shealth ", activity.getString(R.string.SamsungHealth));
                    add_too_samsung_food.setVisibility(View.GONE);
                }

            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * This method will gets the calories consumed today
     *
     * @param samsungHealthDataStore This is the HealthStore object used to connect to the samsung health database and retrieve the data.
     * @param startTime             This is the start time from the user profile being the last time it was updated
     * @param endTime             This is the end time meaning the current time in ms
     * @param listener               This is the listener for processing the data once the data has been retrieved
     */
    static void getTodayConsumedCalories(HealthDataStore samsungHealthDataStore, long startTime,long endTime ,GetTodayCaloriesConsumedObserver listener) {
        try {
            HealthDataResolver.ReadRequest request = new HealthDataResolver.ReadRequest.Builder()
                    .setDataType(Nutrition.HEALTH_DATA_TYPE)
                    .setProperties(new String[]{Nutrition.CALORIE})
                    .setLocalTimeRange(Nutrition.CREATE_TIME, Nutrition.TIME_OFFSET, startTime,endTime)
                    .build();
            new HealthDataResolver(samsungHealthDataStore, null).read(request).setResultListener(result -> {
                float caloriesBurned = 0;
                for (HealthData data : result) {
                    caloriesBurned += data.getFloat(Nutrition.CALORIE);
                }
                listener.onChanged(caloriesBurned);
                result.close();
            });
        } catch (Exception e) {
            Log.e("exercise", "Error happened while reading exercise", e);
        }

    }

    /**
     * A public Observer for importing samsung health calories burned in a float data format.
     * This is used during setup
     */
    public interface ImportBurnedCaloriesObserver {
        void onChanged(float count);
    }

    /**
     * A public Observer for importing samsung health calories consumed in a float data format.
     * This is used during setup
     */
    public interface ImportCaloriesConsumedObserver {
        void onChanged(float count);
    }

    /**
     * A public Observer for getting samsung health recorded food information  in a format of a JSONObject.
     * When called it will update the user profile
     */
    public interface GetFoodNutritionDataObserver {
        void onChanged(JSONObject foodInformationJsonObject);
    }

    /**
     * A public Observer for getting samsung health recorded today's calories burned in a format of a float since the last time the app opened.
     * When called it will update the user profile
     */
    public interface GetTodayBurnedCaloriesObserver {
        void onChanged(float[] count);
    }

    /**
     * A public Observer for getting samsung health recorded today's calories consumed in a format of a float since the last time the app opened.
     * When called it will update the user profile
     */
    public interface GetTodayCaloriesConsumedObserver {
        void onChanged(float count);
    }
}
