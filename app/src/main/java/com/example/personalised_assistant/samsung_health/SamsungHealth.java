package com.example.personalised_assistant.samsung_health;

import android.app.Activity;
import android.util.Log;
import android.widget.Button;

import androidx.appcompat.app.AlertDialog;

import com.example.personalised_assistant.R;
import com.example.personalised_assistant.utils.GlobalVariables;
import com.example.personalised_assistant.utils.MessageAndTextUtils;
import com.samsung.android.sdk.healthdata.HealthConnectionErrorResult;
import com.samsung.android.sdk.healthdata.HealthConstants;
import com.samsung.android.sdk.healthdata.HealthConstants.HeartRate;
import com.samsung.android.sdk.healthdata.HealthDataStore;
import com.samsung.android.sdk.healthdata.HealthPermissionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;
import java.util.TimeZone;

import static com.example.personalised_assistant.samsung_health.UpdateUserProfileSamsung.insertProfileJsonObject;
import static com.example.personalised_assistant.samsung_health.UpdateUserProfileSamsung.insertProfileValue;
import static com.example.personalised_assistant.samsung_health.UpdateUserProfileSamsung.updateProfileJsonArray;
import static com.example.personalised_assistant.samsung_health.UpdateUserProfileSamsung.updateProfileStep;
import static com.example.personalised_assistant.samsung_health.UpdateUserProfileSamsung.updateProfileValue;

/**
 * This class is the main class for getting SHealth data.
 */
public class SamsungHealth {
    /**
     * This is the Samsung Health Store object that allows for connection to the SHealth database.
     */
    public HealthDataStore samsungHealthDataStore;
    /**
     * This holds the current time and date. This is used to set the time for all search quires
     */
    private Calendar Today;
    /**
     * This holds the start time for each query
     */
    private long startTime = 0;
    /**
     * This is the time offset for the users time zone that they are in.
     */
    private final long timeOffset;
    /**
     * This holds the end time for each query
     */
    private long endTime = 0;
    /**
     * This is the total step count used in importing step count during setup.
     */
    private long totalStepCount = 0;
    /**
     * These are the Sleep data count for a 8 hour sleep period. Meaning it keeps a max count for a streak of 8 hours per night of sleep and a current streak
     */
    private int sleepStreak = 0, maxSleepStreak = 0;
    /**
     * This is the current calories burned values being active, resting and digestive
     */
    private float[] caloriesBurned;

    /**
     * This is the total calories burned by the user and is used during setup.
     */
    private float totalCaloriesBurned = 0;
    /**
     * This is the total calories consumed by the user as recorded in Shealth and is used during setup.
     */
    private float totalCaloriesConsumed = 0;
    /**
     * This is a json array of the activities recoded by shealth in the format
     * "exercise":"","calorie":,"duration":,"start_time":,"end_time".
     */
    private JSONArray exercisesDataJSONArray = new JSONArray();
    /**
     * This is a json array of the sleep data recoded by shealth in the format
     * "sleep_data":[{"sleep_time":,"start_time":,"end_Time":}]
     */
    private JSONArray sleepDataJSONArray = new JSONArray();
    /**
     * This is a json array of the hart rate data recoded by shealth in the format
     * "hart_rate":"[{"HEART_BEAT_COUNT":,"HEART_RATE":,"MAX":,"MIN":,"start_time":,"end_time":}]
     */
    private JSONArray heartDataJSONArray = new JSONArray();
    /**
     * This is a json object of the nutrition data recorded by shealth in the fromat
     * "nutrition":{"CALORIE":"","CALCIUM":"","CARBOHYDRATE":"","CHOLESTEROL":"",
     * "DIETARY_FIBER":"","IRON":"","SATURATED_FAT":"","POTASSIUM":"","PROTEIN":"",
     * "SODIUM":"","SUGAR":"","TOTAL_FAT\":"","TRANS_FAT":"","VITAMIN_A":"","VITAMIN_C":"",
     * "MEAL_COUNT":""}
     */
    private JSONObject foodNutritionInformation = new JSONObject();
    /**
     * This is a pointer to the activity which allows us to display pop up error messages
     */
    private Activity activity;
    /**
     * This allows us to access values in the global variables as well as set them.
     */
    private GlobalVariables globalVariables;
    /**
     * This is a Hash-set of permission required by the application as defined in the constructor
     */
    private final Set<HealthPermissionManager.PermissionKey> hashSetHealthDataPermissions = new HashSet<>();
    /**
     * This is a listener for when the the nutrition values have been updated eg calories , nutrients ..etc. It will update the user profile of calories consumed today and total
     */
    private final NutritionData.GetFoodNutritionDataObserver getFoodNutritionDataListener = nutritionValueJson -> {
        foodNutritionInformation = nutritionValueJson;
        if (!globalVariables.getUserProfileIsError()) {
            try {
                //gets time since last
                getSinceLast("last_cal_intake_time");
                // Updates the total cals consumed value if greater than a day
                if ((startTime - endTime) == (1000L * 60 * 60 * 24)) {
                    updateProfileValue("total_cals_consumed", "last_cal_intake_time", foodNutritionInformation.getDouble("CALORIE"), globalVariables, activity, Today);
                }
                // inserts the cals consumed today value  in the user profile
                updateProfileValue("consumed_today", "0",
                        foodNutritionInformation.getDouble("CALORIE"),
                        globalVariables, activity, Today);
                // inserts the nutrition json object to the user profile
                insertProfileJsonObject("nutrition", "",
                        foodNutritionInformation,
                        globalVariables, activity, Today);

            } catch (JSONException e) {
                // notifies the user of the error
                MessageAndTextUtils.error_alert(activity, null,
                        activity.getString(R.string.failedToGetLastCalsBurned),
                        activity.getString(R.string.SamsungHealth));
            }
        }
    };
    /**
     * This is a listener for when today's calories burned is called. It will update the user profile when its pasted  a day
     */
    private final NutritionData.GetTodayCaloriesConsumedObserver getTodayCaloriesConsumedListener = calsConsumed -> {
        //gets time since last
        getSinceLast("last_cal_consumed_time");
        // Updates the total cals consumed value if greater than a day
        if ((startTime - endTime) == (1000L * 60 * 60 * 24)) {
            updateProfileValue("total_cals_consumed", "last_cal_consumed_time",
                    caloriesBurned[0] + caloriesBurned[1] + caloriesBurned[3],
                    globalVariables, activity, Today);
        }
        try {
            // inserts the cals consumed today value  in the user profile
            insertProfileValue("consumed_today",
                    "",
                    String.valueOf(calsConsumed),
                    globalVariables, activity, Today);

        } catch (Exception e) {
            // notifies the user of the error
            MessageAndTextUtils.error_alert(activity, null,
                    activity.getString(R.string.calsConsumedError),
                    activity.getString(R.string.SamsungHealth));
        }
    };
    /**
     * This is a listener for when today's calories burned is called. It will update the user profile when its pasted  a day
     */
    private final NutritionData.GetTodayBurnedCaloriesObserver getTodayBurnedCaloriesListener = burnedCals -> {
        caloriesBurned = burnedCals;
        getSinceLast("last_cal_burned_time");
        if ((startTime - endTime) == (1000L * 60 * 60 * 24)) {
            // updates the total cals burned
            updateProfileValue("total_cals_burned", "last_cal_burned_time",
                    caloriesBurned[0] + caloriesBurned[1] + caloriesBurned[3],
                    globalVariables, activity, Today);
        }
        try {
            // inserts the burned caleries today
            insertProfileValue("burned_today",
                    "",
                    String.valueOf(caloriesBurned[0] + caloriesBurned[1] + caloriesBurned[3]),
                    globalVariables, activity, Today);

        } catch (Exception e) {
            // notifies the user of the error
            MessageAndTextUtils.error_alert(activity, null,
                    activity.getString(R.string.failedToGetLastCalsBurnedError),
                    activity.getString(R.string.SamsungHealth));
        }
    };
    /**
     * This is a listener for updating step count since last time and updating the user profile
     */
    private final ExerciseData.GetSinceLastStepCountObserver getSinceLastStepCountListener = step_count -> updateProfileStep(step_count, globalVariables, activity, Today);
    /**
     * This is a listener for updating the sleep data since last time app was opend and updates the user profile
     */
    private final PhysicalHealthData.GetSinceLastSleepObserver getSinceLastSleepCountListener = (sleepDataJsonArray, max, currentStreak) -> {
        SamsungHealth.this.setCurrentTime();
        try {
            if (max > globalVariables.getUserProfile().getDouble("max_sleep_streak")) {
                insertProfileValue("max_sleep_streak", "last_sleep_time",
                        String.valueOf(currentStreak), globalVariables,
                        activity, Today);
            }

        } catch (JSONException e) {
            // notifies the user of a error if we cant get the max sleep streak
            MessageAndTextUtils.error_alert(activity, null,
                    activity.getString(R.string.error_max_sleep_streakUpdating),
                    activity.getString(R.string.SamsungHealth));
        }
        updateProfileJsonArray("sleep_data",
                "",
                sleepDataJsonArray, globalVariables,
                activity, Today);

        insertProfileValue("sleep_streak",
                "last_sleep_time",
                String.valueOf(currentStreak), globalVariables,
                activity, Today);
    };


    /**
     * This is a listener for getting the latest activities and updating the user profile
     */
    private final ExerciseData.GetSinceLastActivityDataObserver getSinceLastActivityDataListener = exercises -> {
        this.exercisesDataJSONArray = exercises;
        updateProfileJsonArray("exercises",
                "last_exercises_time",
                exercises, globalVariables,
                activity, Today);
    };
    /**
     * This is a listener for getting the latest heart data and updating the user profile
     */
    private final PhysicalHealthData.GetSinceLastHeartDataObserver getSinceLastHeartDataListener = heartJson -> {
        heartDataJSONArray = heartJson;
        updateProfileJsonArray("hart_rate",
                "last_hartrate_time",
                heartJson, globalVariables,
                activity, Today);
    };
    /**
     * This is a listener for importing the burned cals and updating the user profile
     */
    private final NutritionData.ImportBurnedCaloriesObserver importBurnedCaloriesListener = burnedCalories -> totalCaloriesBurned = burnedCalories;
    /**
     * This is a listener for importing the consumed cals and updating the user profile
     */
    private final NutritionData.ImportCaloriesConsumedObserver importCaloriesConsumedListener = consumedCalories -> totalCaloriesConsumed = consumedCalories;
    /**
     * This is a listener for importing the step count and updating the user profile
     */
    private final ExerciseData.ImportStepCountObserver importStepCountListener = step_count -> this.totalStepCount = step_count;
    /**
     * This is a listener for importing the activity data and updating the user profile
     */
    private final ExerciseData.ImportActivityObserver importActivityListener = exercises_json -> exercisesDataJSONArray = exercises_json;
    /**
     * This is a listener for importing the sleeping data and updating the user profile
     */
    private final PhysicalHealthData.ImportSleepObserver importSleepListener = (jsonArraySleepData, maxStreak, currentStreak) -> {
        sleepDataJSONArray = jsonArraySleepData;
        sleepStreak = currentStreak;
        maxSleepStreak = maxStreak;
    };
    /**
     * This is a listener for importing the heart data and updating the user profile
     */
    private final PhysicalHealthData.ImportHeartObserver importHeartRateListener = heartJson -> heartDataJSONArray = heartJson;


    /**
     * This is the samsung health connection listener. It will update the values of the collected data in the user profile.
     */
    public final HealthDataStore.ConnectionListener samsungHealthConnection = new HealthDataStore.ConnectionListener() {
        /**
         * This method will get and set all the required health data.
         */
        @Override
        public void onConnected() {
            // Checks if the user permissions is set if not prompt the user to get allow the permissions
            if (new HealthPermissionManager(samsungHealthDataStore)
                    .isPermissionAcquired(hashSetHealthDataPermissions).
                            containsValue(false)) {
                // Requests the required permissions set in the constructor.
                requestPermission();
            } else {
                updateNutritionData();
                if (globalVariables.getSetUpFlag()) {
                    ExerciseData.importStepCount(samsungHealthDataStore, importStepCountListener);
                    ExerciseData.importActivity(samsungHealthDataStore, importActivityListener);
                    PhysicalHealthData.importSleep(samsungHealthDataStore, importSleepListener);
                    PhysicalHealthData.importHeartRate(samsungHealthDataStore, importHeartRateListener);
                    NutritionData.importBurnedCalories(samsungHealthDataStore, importBurnedCaloriesListener);
                    NutritionData.importCalorieConsumed(samsungHealthDataStore, importCaloriesConsumedListener);
                } else {
                    if (!globalVariables.getUserProfileIsError()) {
                        updateSleep();
                        updateCaloriesBurned();
                        updateStepCount();
                        updateExerciseData();
                        updateHeartData();
                        updateCaloriesConsumed();
                    }
                }
            }
        }


        /**
         * This method displays the error dialog when their is a error child connecting to shealth
         * @param error This is the error that has been returned by shealth
         */
        @Override
        public void onConnectionFailed(HealthConnectionErrorResult error) {
            showConnectionFailureDialog(error);
        }

        /**
         * This method will disconnect the connection to shealth
         */
        @Override
        public void onDisconnected() {
            if (!activity.isFinishing()) {
                samsungHealthDataStore.connectService();
            }
        }
    };

    /**
     * This is the constructor and is used to check permissions and connect to the health stone.
     *
     * @param activity This is the current activity used to connect to the health store .
     */
    public SamsungHealth(Activity activity) {
        // Adds the step count read permission to the permission set
        hashSetHealthDataPermissions.add(new HealthPermissionManager.PermissionKey(
                HealthConstants.StepCount.HEALTH_DATA_TYPE,
                HealthPermissionManager.PermissionType.READ));
        // Adds the sleep data read permission to the permission set
        hashSetHealthDataPermissions.add(new HealthPermissionManager.PermissionKey(
                HealthConstants.Sleep.HEALTH_DATA_TYPE,
                HealthPermissionManager.PermissionType.READ));
        // Adds the heart rate read permission to the permission set
        hashSetHealthDataPermissions.add(new HealthPermissionManager.PermissionKey(
                HeartRate.HEALTH_DATA_TYPE,
                HealthPermissionManager.PermissionType.READ));
        // Adds the exercise data read permission to the permission set
        hashSetHealthDataPermissions.add(new HealthPermissionManager.PermissionKey(
                HealthConstants.Exercise.HEALTH_DATA_TYPE,
                HealthPermissionManager.PermissionType.READ));
        // Adds the calories burned read permission to the permission set
        hashSetHealthDataPermissions.add(new HealthPermissionManager.PermissionKey(
                "com.samsung.shealth.calories_burned",
                HealthPermissionManager.PermissionType.READ));
        // Adds the nutrition read permission to the permission set
        hashSetHealthDataPermissions.add(new HealthPermissionManager.PermissionKey(
                HealthConstants.Nutrition.HEALTH_DATA_TYPE,
                HealthPermissionManager.PermissionType.READ));
        // Adds the nutrition write permission to the permission set
        hashSetHealthDataPermissions.add(new HealthPermissionManager.PermissionKey(
                HealthConstants.Nutrition.HEALTH_DATA_TYPE,
                HealthPermissionManager.PermissionType.WRITE));
        // Allows us to access and write to the global variables
        globalVariables = (GlobalVariables) activity.getApplicationContext();
        // Sets the pointer to the current activity.
        this.activity = activity;
        // Sets the time info. This is used to get the data from  shealth in the desired time frame.
        this.Today = Calendar.getInstance(TimeZone.getDefault());
        Today.setTimeInMillis(Today.getTimeInMillis() + TimeZone.getDefault().getOffset(Calendar.getInstance().getTimeInMillis()));
        this.timeOffset = TimeZone.getDefault().getOffset(Today.getTimeInMillis());
        // This connects us to the samsung health database.
        samsungHealthDataStore = new HealthDataStore(activity.getApplicationContext(), samsungHealthConnection);
        samsungHealthDataStore.connectService();
    }

    /**
     * This method will request shealth permissions and display a error message if not allowed
     */
    private void requestPermission() {
        try {
            new HealthPermissionManager(samsungHealthDataStore).
                    requestPermissions(hashSetHealthDataPermissions, activity).
                    setResultListener(permissionResult -> {
                        if (permissionResult.getResultMap().containsValue(false)) {
                            MessageAndTextUtils.error_message(activity, (dialog, which) -> {
                                        if (new HealthPermissionManager(samsungHealthDataStore)
                                                .isPermissionAcquired(hashSetHealthDataPermissions)
                                                .containsValue(false)) {
                                            // request permissions not acquired
                                            requestPermission();
                                        }
                                    },
                                    (dialog, which) -> System.exit(0),
                                    activity.getString(R.string.applicationRequiresSHealth),
                                    activity.getString(R.string.SamsungHealth));
                        }
                    });
        } catch (Exception e) {
            MessageAndTextUtils.alert(activity, null,
                    activity.getString(R.string.SamsungHealth) + activity.getString(R.string.permission_setting_failed) + e.toString(),
                    activity.getString(R.string.samsunghealth));
        }
    }

    /**
     * This method will display a appropriate error based for the connection issue with samsung health. It will also try allow for a resolution for this error.
     *
     * @param error This is the error code that shealth has thrown in a HealthConnectionErrorResult format which is a wrapper for a int value
     */
    private void showConnectionFailureDialog(final HealthConnectionErrorResult error) {
        if (activity.isFinishing()) {
            return;
        }
        String Message;
        if (error.hasResolution()) {
            switch (error.getErrorCode()) {
                case HealthConnectionErrorResult.PLATFORM_NOT_INSTALLED:
                    Message = activity.getString(R.string.InstallSamsungHealth);
                    break;
                case HealthConnectionErrorResult.CONNECTION_FAILURE:
                    Message = activity.getString(R.string.ConnectionFailed);
                    break;
                case HealthConnectionErrorResult.OLD_VERSION_SDK:
                    Message = activity.getString(R.string.UnsupportedPlatformSamsungHealth);
                    break;
                case HealthConnectionErrorResult.PLATFORM_INITIALIZING:
                    Message = activity.getString(R.string.InitingPlatform);
                    break;
                case HealthConnectionErrorResult.TIMEOUT:
                    Message = activity.getString(R.string.TimedOutSamungHealth);
                    break;
                case HealthConnectionErrorResult.PLATFORM_SIGNATURE_FAILURE:
                    Message = activity.getString(R.string.PlatformSigFailed);
                    break;
                case HealthConnectionErrorResult.USER_PASSWORD_NEEDED:
                    Message = activity.getString(R.string.UserPasswordNeeded);
                    break;
                case HealthConnectionErrorResult.USER_PASSWORD_POPUP:
                    Message = activity.getString(R.string.UserPasswordNeededToConnectToShealth);
                    break;
                case HealthConnectionErrorResult.OLD_VERSION_PLATFORM:
                    Message = activity.getString(R.string.PleaseUpgradeSamsungHealth);
                    break;
                case HealthConnectionErrorResult.PLATFORM_DISABLED:
                    Message = activity.getString(R.string.PleaseEnableSamsungHealth);
                    break;
                case HealthConnectionErrorResult.USER_AGREEMENT_NEEDED:
                    Message = activity.getString(R.string.PleaseAgreeToSamsungHealthPolicy);
                    break;
                default:
                    Message = activity.getString(R.string.PleaseMakeSamsungHealthAvailable);
                    break;
            }
        } else {
            Message = activity.getString(R.string.ConnectionWithSamsungHealthIsNotAvailable);
        }

        AlertDialog.OnClickListener ok = (dialog, which) -> {
            if (error.hasResolution()) {
                error.resolve(activity);
            }
        };
        MessageAndTextUtils.error_alert(activity,
                ok, Message, 
                activity.getString(R.string.samsunghealth));
    }

    /**
     * This method will add food to samsung health
     *
     * @param food_object          This is the food item in a json object containing all its nutrients
     * @param activity             This is a pointer to the current activity. This allows us to creates error message popups.
     * @param add_too_samsung_food This is the button which adds allows adding food. This is used so if the food is added it is removed to stop duplicates and to allow a retry to add food if not successful in adding.
     */
    public void set_food(JSONObject food_object, Activity activity, Button add_too_samsung_food) {
        NutritionData.addFood(samsungHealthDataStore, food_object, activity, add_too_samsung_food);
    }

    /**
     * This method will update all values from samsung health
     */
    public void updateAllValues() {
        updateNutritionData();
        updateCaloriesBurned();
        updateStepCount();
        updateExerciseData();
        updateHeartData();
        updateSleep();
        updateCaloriesConsumed();
    }

    /**
     * Method to get time since last of a item
     *
     * @param item This is the string of the item you want to get the time since last of.
     */
    private void getSinceLast(String item) {
        setCurrentTime();
        try {
            startTime = (Long.parseUnsignedLong(globalVariables.getUserProfile().get(item).toString()));
        } catch (Exception e) {
            MessageAndTextUtils.error_alert(activity, null, "Failed To get Step Count", activity.getString(R.string.SamsungHealth));
        }
        if (startTime == 0) {
            startTime = Today.getTimeInMillis() - (24 * 60 * 60 * 1000L);
        }
        Log.d("set_last_step_time", "start time: " + startTime + "end_time " + endTime);
    }

    /**
     * Method to set the current time
     */
    private void setCurrentTime() {
        Today = Calendar.getInstance(TimeZone.getDefault());
        Today.setTimeInMillis(Today.getTimeInMillis() + timeOffset);
        endTime = Today.getTimeInMillis();
    }

    /**
     * Method to set the time to the start of the day
     */
    private void setStartTimeDay() {
        //Offset to stop midnight day forwarding
        int offset = 0;
        if (Today.get(Calendar.HOUR_OF_DAY) == 0) {
            offset = -86400000;
        }
        Today.set(Calendar.HOUR_OF_DAY, 0);
        Today.clear(Calendar.MINUTE);
        Today.clear(Calendar.SECOND);
        Today.clear(Calendar.MILLISECOND);
        startTime = (Today.getTimeInMillis() + timeOffset + offset);
        setCurrentTime();
    }

    /**
     * This getter method will return the users sleep data
     */
    public JSONArray getSleepDataJSONArray() {
        return sleepDataJSONArray;
    }

    /**
     * This getter method will return the users heart rate data
     */
    public JSONArray getHeartDataJSONArray() {
        return heartDataJSONArray;
    }

    /**
     * This getter method will return the users current sleep streak of longer than 8 hours
     */
    public int getSleepStreak() {
        return sleepStreak;
    }

    /**
     * This getter method will return the users maximum sleep streak of longer than 8 hours
     */
    public int getMaxSleepStreak() {
        return maxSleepStreak;
    }

    /**
     * This getter method will return the users nurtrition data for the current day
     */
    public JSONObject getFoodNutritionInformation() {
        return foodNutritionInformation;
    }

    /**
     * This getter method will return the total number of calories burned
     */
    public float getTotalCaloriesBurned() {
        return totalCaloriesBurned;
    }

    /**
     * This getter method will return the number of calories burned in a float array.
     */
    public float[] getCaloriesBurned() {
        return caloriesBurned;
    }

    /**
     * This is a getter method that will return the total number of calories consumed
     */
    public float getTotalCaloriesConsumed() {
        return totalCaloriesConsumed;
    }

    /**
     * This getter method will return the total step count
     */
    public long getTotalStepCount() {
        return totalStepCount;
    }

    /**
     * This getter method will return a JSON array of exercise data
     */
    public JSONArray getExercisesDataJSONArray() {
        return exercisesDataJSONArray;
    }

    /**
     * This method will update the nutrition data
     */
    public void updateNutritionData() {
        setStartTimeDay();
        NutritionData.getFoodNutritionInformation(samsungHealthDataStore, startTime, endTime, getFoodNutritionDataListener);
    }

    /**
     * This method will update the heart rate data
     */
    public void updateHeartData() {
        setCurrentTime();
        getSinceLast("last_hartrate_time");
        if (startTime != endTime) {
            PhysicalHealthData.getHeartRate(samsungHealthDataStore, startTime, endTime, getSinceLastHeartDataListener);
        }
    }

    /**
     * This method will update the exercise data.
     */
    public void updateExerciseData() {
        getSinceLast("last_exercises_time");
        if (startTime != endTime) {
            ExerciseData.getSinceLastActivity(samsungHealthDataStore, startTime,
                    endTime, getSinceLastActivityDataListener);
        }
    }

    /**
     * This method will update the current step count
     */
    public void updateStepCount() {
        getSinceLast("last_step_time");
        if (startTime != endTime) {
            ExerciseData.getSinceLastStepCount(samsungHealthDataStore, startTime,
                    endTime, getSinceLastStepCountListener);
        }
    }

    /**
     * This method will update Calories burned when called
     */
    public void updateCaloriesBurned() {
        NutritionData.getTodayBurnedCalories(samsungHealthDataStore, startTime,
                getTodayBurnedCaloriesListener);
    }

    /**
     * This method will update the sleep data
     */
    public void updateSleep() {
        getSinceLast("last_step_time");

        PhysicalHealthData.getSleep(samsungHealthDataStore, startTime,
                endTime, getSinceLastSleepCountListener);
    }

    /**
     * This method will update the cals consumed
     */
    public void updateCaloriesConsumed() {
        getSinceLast("last_step_time");
        NutritionData.getTodayConsumedCalories(samsungHealthDataStore,
                startTime, endTime, getTodayCaloriesConsumedListener);
    }

}
