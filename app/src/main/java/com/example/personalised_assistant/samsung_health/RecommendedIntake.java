package com.example.personalised_assistant.samsung_health;

/**
 * This class will return the recommended nutrients, minerals and vitamins based upon the users profile.
 */
public class RecommendedIntake {
    /**
     * This is the users gender and is used to adapt to the users  based upon government guide lines
     */
    private final String gender;
    /**
     * This is the users current age and is used to adapt to the users age based upon government guide lines
     */
    private final int age;
    /**
     * List all the keys for the recommended amount of nutrients, minerals and vitamins.
     */
    public static final String[] key={
            "CALORIE","CALCIUM","CARBOHYDRATE","CHOLESTEROL","DIETARY_FIBER","IRON"
            ,"SATURATED_FAT","POTASSIUM","PROTEIN","SODIUM","SUGAR","TOTAL_FAT","TRANS_FAT","VITAMIN_A","VITAMIN_C"
    };



    public RecommendedIntake(int age , String gender){
        this.age = age;
        this.gender = gender;
    }

    public Float[] getRecommendFoodIntake() {
        //constance across all age ranges apart from gender
        float CHOLESTEROL = 0, DIETARY_FIBER = 30, POTASSIUM = 3500,
                SODIUM = 2400f,TRANS_FAT = 0,VITAMIN_C = 40;

        // Sets SamsungHealthConstants and duplicates and init values
        float VITAMIN_A, CALORIE  = 2000f,CALCIUM = 700, CARBOHYDRATE = 267,IRON = 8.7f,
                SATURATED_FAT = 24,PROTEIN = 45,SUGAR = 27,TOTAL_FAT = 78;

        if (gender.equalsIgnoreCase("female")){
            VITAMIN_A = 600;
        }else{
            VITAMIN_A = 700;
        }

        if (age<=18){
            if (gender.equalsIgnoreCase("female")){
                CALCIUM = 800;
                IRON = 14.8f;
            }else{
                CALORIE  = 2500;
                PROTEIN = 55.2f;
                TOTAL_FAT = 97;
                SATURATED_FAT = 31;
                CARBOHYDRATE = 333;
                SUGAR = 33;
                CALCIUM = 1000;
                IRON = 11.3f;
            }
        }else if(age<=64 ){
            if (gender.equalsIgnoreCase("female")){
                IRON = 14.8f;
            }else{
                CALORIE  = 2500;
                PROTEIN = 55.2f;
                TOTAL_FAT = 97;
                SATURATED_FAT = 31;
                CARBOHYDRATE = 333;
                SUGAR = 33;
            }
        }else if(age <= 74){
            if (gender.equalsIgnoreCase("female")){
                CALORIE  = 1912;
                CARBOHYDRATE = 255;
                TOTAL_FAT = 74;
                SATURATED_FAT = 23;
                PROTEIN = 46.5f;
                SUGAR = 26;
            }else{
                CALORIE  = 2342;
                CARBOHYDRATE = 312;
                TOTAL_FAT = 91;
                SATURATED_FAT = 29;
                PROTEIN = 53.3f;
                SUGAR = 31;
            }
        }else{
            if (gender.equalsIgnoreCase("female")){
                CALORIE  = 1840;
                CARBOHYDRATE = 255;
                TOTAL_FAT = 72;
                SATURATED_FAT = 23;
                PROTEIN = 46.5f;
                SUGAR = 25;
            }else{
                CALORIE  = 2294;
                CARBOHYDRATE = 312;
                TOTAL_FAT = 89;
                SATURATED_FAT = 28;
                PROTEIN = 53.3f;
                SUGAR = 31;
            }
        }

        return new Float []{
                CALORIE,
                CALCIUM,
                CARBOHYDRATE,
                CHOLESTEROL,
                DIETARY_FIBER,
                IRON,
                SATURATED_FAT,
                POTASSIUM,
                PROTEIN,
                SODIUM,
                SUGAR,
                TOTAL_FAT,
                TRANS_FAT,
                VITAMIN_A,
                VITAMIN_C};
    }
}
