package com.example.personalised_assistant.samsung_health;

import com.samsung.android.sdk.healthdata.HealthConstants;

import java.util.Calendar;
import java.util.TimeZone;

/**
 * This is a constancts class and hold meal types and Activity strings from activity codes
 */
class SamsungHealthConstants {
    /**
     * This method will return the appropriate meal time based upon the current item in the day
     *
     * @return This will return a int value that samsungs health uses as a constant for a meal type depending on the current time of day
     */
    static int getMealType() {
        Calendar Today = Calendar.getInstance(TimeZone.getDefault());
        Today.setTimeInMillis(Today.getTimeInMillis() + TimeZone.getDefault().getOffset(Calendar.getInstance().getTimeInMillis()));
        int Hour = Today.get(Calendar.HOUR_OF_DAY);
        if (Hour > 0 && Hour < 10) {
            return HealthConstants.Nutrition.MEAL_TYPE_BREAKFAST;
        } else if (Hour > 10 && Hour < 13) {
            return HealthConstants.Nutrition.MEAL_TYPE_MORNING_SNACK;
        } else if (Hour > 13 && Hour < 17) {
            return HealthConstants.Nutrition.MEAL_TYPE_LUNCH;
        } else if (Hour > 17 && Hour < 20) {
            return HealthConstants.Nutrition.MEAL_TYPE_DINNER;
        } else {
            return HealthConstants.Nutrition.MEAL_TYPE_EVENING_SNACK;
        }
    }

    /**
     * This method will return a string that is the corresponding activity to the activity code given
     *
     * @param type This is the int value for the activity that you want to get the string for.
     * @return This is the string name of the activity id.
     */
    static String getActivityType(int type) {
        if (type == 1001) {
            return "Walking";
        } else if (type == 1002) {
            return "Running";
        } else if (type == 2001) {
            return "Baseball, general";
        } else if (type == 2002) {
            return "Softball, general";
        } else if (type == 2003) {
            return "Cricket";
        } else if (type == 3001) {
            return "Golf, general";
        } else if (type == 3002) {
            return "Billiards";
        } else if (type == 3003) {
            return "Bowling, alley";
        } else if (type == 4001) {
            return "Hockey";
        } else if (type == 4002) {
            return "Rugby, touch, non-competitive";
        } else if (type == 4003) {
            return "Basketball, general";
        } else if (type == 4004) {
            return "Football, general";
        } else if (type == 4005) {
            return "Handball, general";
        } else if (type == 4006) {
            return "Soccer, general, touch";
        } else if (type == 5001) {
            return "Volleyball, general, 6~9 member team, non-competitive";
        } else if (type == 5002) {
            return "Beach volleyball";
        } else if (type == 6001) {
            return "Squash, general";
        } else if (type == 6002) {
            return "Tennis, general";
        } else if (type == 6003) {
            return "Badminton, competitive";
        } else if (type == 6004) {
            return "Table tennis";
        } else if (type == 6005) {
            return "Racquetball, general";
        } else if (type == 7001) {
            return "T'ai chi, general";
        } else if (type == 7002) {
            return "Boxing, in ring";
        } else if (type == 7003) {
            return "Martial arts, moderate pace (Judo, Jujitsu, Karate, Taekwondo)";
        } else if (type == 8001) {
            return "Ballet, general, rehearsal or class";
        } else if (type == 8002) {
            return "Dancing, general (Fork, Irish step, Polka)";
        } else if (type == 8003) {
            return "Ballroom dancing, fast";
        } else if (type == 9001) {
            return "Pilates";
        } else if (type == 9002) {
            return "Yoga";
        } else if (type == 10001) {
            return "Stretching";
        } else if (type == 10002) {
            return "Jump rope, moderate pace (100~120 skips/min), 2 foot skip";
        } else if (type == 10003) {
            return "Hula-hooping";
        } else if (type == 10004) {
            return "Push-ups (Press-ups)";
        } else if (type == 10005) {
            return "Pull-ups (Chin-up)";
        } else if (type == 10006) {
            return "Sit-ups";
        } else if (type == 10007) {
            return "Circuit training, moderate effort";
        } else if (type == 10008) {
            return "Mountain climbers";
        } else if (type == 10009) {
            return "Jumping Jacks";
        } else if (type == 10010) {
            return "Burpee";
        } else if (type == 10011) {
            return "Bench press";
        } else if (type == 10012) {
            return "Squats";
        } else if (type == 10013) {
            return "Lunges";
        } else if (type == 10014) {
            return "Leg presses";
        } else if (type == 10015) {
            return "Leg extensions";
        } else if (type == 10016) {
            return "Leg curls";
        } else if (type == 10017) {
            return "Back extensions";
        } else if (type == 10018) {
            return "Lat pull-downs";
        } else if (type == 10019) {
            return "Deadlifts";
        } else if (type == 10020) {
            return "Shoulder presses";
        } else if (type == 10021) {
            return "Front raises";
        } else if (type == 10022) {
            return "Lateral raises";
        } else if (type == 10023) {
            return "Crunches";
        } else if (type == 10024) {
            return "Leg raises";
        } else if (type == 10025) {
            return "Plank";
        } else if (type == 10026) {
            return "Arm curls";
        } else if (type == 10027) {
            return "Arm extensions";
        } else if (type == 11001) {
            return "Inline skating, moderate pace";
        } else if (type == 11002) {
            return "Hang gliding";
        } else if (type == 11003) {
            return "Pistol shooting";
        } else if (type == 11004) { // Dont know why but in api docs for exercise list
            return "Archery, non-hunting";
        } else if (type == 11005) {
            return "Horseback riding, general";
        } else if (type == 11007) {
            return "Cycling";
        } else if (type == 11008) {
            return "Flying disc, general, playing";
        } else if (type == 11009) {
            return "Roller skating";
        } else if (type == 12001) {
            return "Aerobics, general";
        } else if (type == 13001) {
            return "Hiking";
        } else if (type == 13002) {
            return "Rock climbing, low to moderate difficulty";
        } else if (type == 13003) {
            return "Backpacking";
        } else if (type == 13004) {
            return "Mountain biking, general";
        } else if (type == 13005) {
            return "Orienteering";
        } else if (type == 14001) {
            return "Swimming, general, leisurely, not lap swimming";
        } else if (type == 14002) {
            return "Aquarobics";
        } else if (type == 14003) {
            return "Canoeing, general, for pleasure";
        } else if (type == 14004) {
            return "Sailing, leisure, ocean sailing";
        } else if (type == 14005) {
            return "Scuba diving, general";
        } else if (type == 14006) {
            return "Snorkeling";
        } else if (type == 14007) {
            return "Kayaking, moderate effort";
        } else if (type == 14008) {
            return "Kitesurfing";
        } else if (type == 14009) {
            return "Rafting";
        } else if (type == 14010) {
            return "Rowing machine, general, for pleasure";
        } else if (type == 14011) {
            return "Windsurfing, general";
        } else if (type == 14012) {
            return "Yachting, leisure";
        } else if (type == 14013) {
            return "Water skiing";
        } else if (type == 15001) {
            return "Step machine";
        } else if (type == 15002) {
            return "Weight machine";
        } else if (type == 15003) {
            return "Exercise bike, Moderate to vigorous effort";
        } else if (type == 15004) {
            return "Rowing machine";
        } else if (type == 15005) {
            return "Treadmill, combination of jogging and walking";
        } else if (type == 15006) {
            return "Elliptical trainer, moderate effort";
        } else if (type == 16001) {
            return "Cross-country skiing, general, moderate speed (4.0~4.9 mph)";
        } else if (type == 16002) {
            return "Skiing, general, downhill, moderate effort";
        } else if (type == 16003) {
            return "Ice dancing";
        } else if (type == 16004) {
            return "Ice skating, general";
        } else if (type == 16006) {
            return "Ice hockey, general";
        } else if (type == 16007) {
            return "Snowboarding, general, moderate effort";
        } else if (type == 16008) {
            return "Alpine skiing, general, moderate effort";
        } else if (type == 16009) {
            return "Snowshoeing, moderate effort";
        } else {
            return "Custom";
        }
    }
}
