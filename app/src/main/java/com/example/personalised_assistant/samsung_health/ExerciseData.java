package com.example.personalised_assistant.samsung_health;

import android.util.Log;

import com.samsung.android.sdk.healthdata.HealthConstants;
import com.samsung.android.sdk.healthdata.HealthData;
import com.samsung.android.sdk.healthdata.HealthDataResolver;
import com.samsung.android.sdk.healthdata.HealthDataStore;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

import static com.samsung.android.sdk.healthdata.HealthConstants.Exercise.CALORIE;
import static com.samsung.android.sdk.healthdata.HealthConstants.Exercise.DURATION;
import static com.samsung.android.sdk.healthdata.HealthConstants.Exercise.EXERCISE_TYPE;
import static com.samsung.android.sdk.healthdata.HealthConstants.Exercise.HEALTH_DATA_TYPE;
import static com.samsung.android.sdk.healthdata.HealthConstants.SessionMeasurement.END_TIME;
import static com.samsung.android.sdk.healthdata.HealthConstants.SessionMeasurement.START_TIME;
import static com.samsung.android.sdk.healthdata.HealthConstants.SessionMeasurement.TIME_OFFSET;
import static com.samsung.android.sdk.healthdata.HealthConstants.StepCount.COUNT;

/**
 * This class gets all the exercise data eg number of steps and exercises type duration and calorie count.
 * This is class is used by SamsungHealth class.
 */
class ExerciseData {
    /**
     * Creates temp storage for step count
     */
    private static long importStepCount = 0, stepCount = 0;

    /**
     * This method is used for importing user actives from samsung health.
     * It is used during setup
     *
     * @param samsungHealthDataStore This is the HealthStore object used to connect to the samsung health database and retrieve the data.
     * @param listener               This is the listener for processing the data once the data has been retrieved
     */
    static void importActivity(HealthDataStore samsungHealthDataStore, ImportActivityObserver listener) {
        // Creates a temp array for adding shealth data to be returned to the listener.
        JSONArray exercises = new JSONArray();
        try {
            // Gets the data from shealth database from the maximum time period to present
            HealthDataResolver.ReadRequest request = new HealthDataResolver.ReadRequest.Builder()
                    .setDataType(HEALTH_DATA_TYPE)
                    .setProperties(new String[]{CALORIE, EXERCISE_TYPE, DURATION, START_TIME, END_TIME})
                    .build();
            //Queries the database for the data and returns the result
            new HealthDataResolver(samsungHealthDataStore, null).read(request).setResultListener(result -> {
                // Put the exercise data from shealth into a json array
                for (HealthData data : result) {
                    JSONObject exercise = new JSONObject();
                    try {
                        exercise.put("exercise", SamsungHealthConstants
                                .getActivityType(Integer.parseInt(data.get(EXERCISE_TYPE).toString())));// gets the activity name from the constants list
                        // gets the cal info and puts it into the object
                        exercise.put("calorie", data.getFloat(CALORIE));
                        // gets how long the activity last puts it into the object
                        exercise.put("duration", data.getLong(DURATION));
                        // gets the start time and puts it into the object
                        exercise.put("start_time", data.getLong(START_TIME));
                        // gets the end time and puts it into the object
                        exercise.put("end_time", data.getLong(END_TIME));
                        // puts the object into the json array
                        exercises.put(exercise);
                    } catch (JSONException e) {
                        // Print out stack trace for debugging
                        e.printStackTrace();
                    }
                }
                // calls the listener and updates the user profile.
                listener.onChanged(exercises);
                //Closes the connection
                result.close();
            });
        } catch (Exception e) {
            // Print out stack trace for debugging
            Log.e("exercise", "Error happened while reading exercise", e);
        }
    }

    /**
     * This method is used for importing the total step count from samsung health.
     * It is used during setup
     *
     * @param samsungHealthDataStore This is the HealthStore object used to connect to the samsung health database and retrieve the data.
     * @param listener               This is the listener for processing the data once the data has been retrieved
     */
    static void importStepCount(HealthDataStore samsungHealthDataStore, ImportStepCountObserver listener) {
        //sets step count to 0
        importStepCount = 0;
        // Gets the data from shealth database from the maximum time period to present
        HealthDataResolver.ReadRequest request = new HealthDataResolver.ReadRequest.Builder()
                .setDataType(HealthConstants.StepCount.HEALTH_DATA_TYPE)
                .setProperties(new String[]{COUNT, HealthConstants.StepCount.CALORIE})
                .build();
        //Queries the database for the data and returns the result
        new HealthDataResolver(samsungHealthDataStore, null).read(request).setResultListener(result -> {
            for (HealthData data : result) {
                //Adds to the total of the step count
                importStepCount += data.getInt(COUNT);
            }
            // calls the listener and updates the user profile.
            listener.onChanged(importStepCount);
            //Closes the connection
            result.close();
        });
    }

    /**
     * This method will get the actives the user has engaged in since the last time it was updated
     *
     * @param samsungHealthDataStore This is the HealthStore object used to connect to the samsung health database and retrieve the data.
     * @param start_time             This is the start time from the user profile being the last time it was updated
     * @param end_time               this is the current time in milliseconds
     * @param listener               This is the listener for processing the data once the data has been retrieved
     */
    static void getSinceLastActivity(HealthDataStore samsungHealthDataStore, long start_time, long end_time, GetSinceLastActivityDataObserver listener) {
        // Creates a temp array for adding shealth data to be returned to the listener.
        JSONArray exercises = new JSONArray();
        try {
            // Gets the data from shealth database from a given time period start_time to end_time
            HealthDataResolver.ReadRequest request = new HealthDataResolver.ReadRequest.Builder()
                    .setDataType(HEALTH_DATA_TYPE)
                    .setLocalTimeRange(START_TIME, TIME_OFFSET, start_time, end_time)
                    .setProperties(new String[]{CALORIE, EXERCISE_TYPE, DURATION, START_TIME, END_TIME})
                    .build();
            //Queries the database for the data and returns the result
            new HealthDataResolver(samsungHealthDataStore, null)
                    .read(request)
                    .setResultListener(result -> {
                        // Put the exercise data from shealth into a json array
                        for (HealthData data : result) {
                            JSONObject exercise = new JSONObject();
                            try {
                                exercise.put("exercise", SamsungHealthConstants
                                        .getActivityType(Integer.parseInt(data.get(EXERCISE_TYPE).toString())));// gets the activity name from the constants list
                                // gets the cal info and puts it into the object
                                exercise.put("calorie", data.getFloat(CALORIE));
                                // gets how long the activity last puts it into the object
                                exercise.put("duration", data.getLong(DURATION));
                                // gets the start time and puts it into the object
                                exercise.put("start_time", data.getLong(START_TIME));
                                // gets the end time and puts it into the object
                                exercise.put("end_time", data.getLong(END_TIME));
                                // puts the object into the json array
                                exercises.put(exercise);
                            } catch (JSONException e) {
                                // Print out stack trace for debugging
                                e.printStackTrace();
                            }
                        }
                        // calls the listener and updates the user profile.
                        listener.onChanged(exercises);
                        //Closes the connection
                        result.close();
                    });
        } catch (Exception e) {
            Log.e("exercise", "Error happened while reading exercise", e);
        }
    }

    /**
     * This method will get the total number of steps since the last time it was updated
     *
     * @param samsungHealthDataStore This is the HealthStore object used to connect to the samsung health database and retrieve the data.
     * @param start_time             This is the start time from the user profile being the last time it was updated
     * @param end_time               this is the current time in milliseconds
     * @param listener               This is the listener for processing the data once the data has been retrieved
     */
    static void getSinceLastStepCount(HealthDataStore samsungHealthDataStore, long start_time, long end_time, GetSinceLastStepCountObserver listener) {
        stepCount = 0;
        HealthDataResolver.AggregateRequest request = new HealthDataResolver.AggregateRequest.Builder()
                .setDataType(HealthConstants.StepCount.HEALTH_DATA_TYPE)
                .setSort("count", HealthDataResolver.SortOrder.DESC)
                .addFunction(HealthDataResolver.AggregateRequest.AggregateFunction.SUM, COUNT, "count")
                .setLocalTimeRange(START_TIME, TIME_OFFSET, start_time, end_time)
                .build();
        //Queries the database for the data and returns the result
        new HealthDataResolver(samsungHealthDataStore, null)
                .aggregate(request)
                .setResultListener(result -> {
                    Iterator<HealthData> iterator = result.iterator();
                    if (iterator.hasNext()) {
                        HealthData data = iterator.next();
                        stepCount = data.getInt("count");
                    }
                    listener.onChanged(stepCount);
                    result.close();
                });
    }

    /**
     * A public Observer for importing samsung health recorded actives in a format of a json array.
     * This is used during setup
     */
    public interface ImportActivityObserver {
        void onChanged(JSONArray activitiesJSONArray);
    }

    /**
     * A public Observer for importing samsung health records of steps in a long data format.
     * This is used during setup
     */
    public interface ImportStepCountObserver {
        void onChanged(long import_step_count);
    }

    /**
     * A public Observer for getting the step count since the last time it was checked since the last time the app opened.
     * When called it will update the user profile
     */
    public interface GetSinceLastStepCountObserver {
        void onChanged(long stepCount);
    }

    /**
     * A public Observer for getting samsung health recorded actives in a format of a json array since the last time the app opened.
     * When called it will update the user profile
     */
    public interface GetSinceLastActivityDataObserver {
        void onChanged(JSONArray activatesJSONArray);
    }

}
