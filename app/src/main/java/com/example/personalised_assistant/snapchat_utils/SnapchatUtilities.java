package com.example.personalised_assistant.snapchat_utils;

import android.app.Activity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.Nullable;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RetryPolicy;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.personalised_assistant.R;
import com.example.personalised_assistant.utils.GlobalVariables;
import com.example.personalised_assistant.utils.MessageAndTextUtils;
import com.example.personalised_assistant.utils.FileUtilities;
import com.snapchat.kit.sdk.SnapLogin;
import com.snapchat.kit.sdk.core.controller.LoginStateController;
import com.snapchat.kit.sdk.login.models.MeData;
import com.snapchat.kit.sdk.login.models.UserDataResponse;
import com.snapchat.kit.sdk.login.networking.FetchUserDataCallback;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;
import java.util.TimeZone;

/**
 * This class is used to login,logout and interact with snapchat and its features. It allows for searching of snapchat bitemojie strips.
 */
public class SnapchatUtilities {
    /**
     * This holds the users snapchat information and is from the snapchat api
     */
    private MeData snapChatsUserData;
    /**
     * This boolean is gettable and is set in the constructed if the user is logged into snapchat
     */
    private Boolean isLoggedIn = false;
    /**
     * This holds a pointer to the activity used to display error messages and to get the global_vars object
     */
    private final Activity activity;
    /**
     * THis holds the users version of the snapchat database for the adaptive feature.
     */
    private JSONArray bitEmojiDataBase = null;
    /**
     * This is a pointer to the global vars object that stores the loaded user profile and other variables used across activates. These values only get cleared if the user closes the application but are saved before hand.
     */
    private final GlobalVariables globalVariables;
    /**
     * This is the retry policy for getting the snapchat database
     */
    private final RetryPolicy retryPolicy = new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

    /**
     * This setsup the object by setting the activity and the getting the global_vars object. It also checks if the user is logged in or not.
     * @param _activity this is a pointer to the activity object
     */
    public SnapchatUtilities(Activity _activity){
        activity = _activity;
        globalVariables = (GlobalVariables)activity.getApplicationContext();

        if (SnapLogin.isUserLoggedIn(activity)) {
            isLoggedIn = true;
        }
    }

    /**
     * This methord will return if the user is logged in or not
     * @return  This will return a boolean value true if user is logged into snapchat or false if not.
     */
    public boolean isUserLoggedIn(){
        return isLoggedIn;
    }

    /**
     * This method will log the user into snapchat and get the user data and display a success message
     */
    public void login(){
        if (!isLoggedIn) {
        SnapLogin.getAuthTokenManager(activity).startTokenGrant();
        SnapLogin.getLoginStateController(activity).addOnLoginStateChangedListener(snapChatLoginListener);
        }
    }

    /**
     * This function when called will blacklist the users user token for snapchat and log them out.
     */
    public void logout(){
        if (isLoggedIn){
            SnapLogin.getAuthTokenManager(activity).clearToken();
        }
    }

    /**
     * This function gets the users snapchat information and updates the bit emoji database if older than 7 days
     */
    public void getSnapChatUserData() {
        // This will attempt to get the users data from snapchat
        SnapLogin.fetchUserData(activity, "{me{bitmoji{avatar,id},displayName,externalId}}", null, new FetchUserDataCallback() {
            /**
             * This method is called when getting the users data is successful. These comments will not be in javadoc but should give a bit more explation to whats going on.
             * @param userDataResponse This is the resulting user data from the above query
             */
            @Override
            public void onSuccess(@Nullable UserDataResponse userDataResponse) {
                assert userDataResponse != null;
                // sets the snapchat data object
                snapChatsUserData = userDataResponse.getData().getMe();
                try {
                    // Checks to see if the bit emoji database exists if not it will make a new one
                    File data_base_test = new File(activity.getFilesDir() + "/bitemoji_cartoon.json");
                    if (data_base_test.exists()) {
                        // Reads the bitemoji database fom file
                        bitEmojiDataBase = FileUtilities.read_bitemoji_data(activity.getFilesDir() + "/bitemoji_cartoon.json");
                        assert bitEmojiDataBase != null;
                        // gets today time and date
                        Calendar today = Calendar.getInstance(TimeZone.getDefault());
                        // gets today time with time zone offsets
                        long todayInMilli = today.getTimeInMillis() + TimeZone.getDefault().getOffset(today.getTimeInMillis());
                        // 604800000 = 7 * 24 * 60 * 60 * 1000L which is 7 days in epoch time
                        if (bitEmojiDataBase.getLong(0) + 604800000  < todayInMilli && !globalVariables.getSetUpFlag()) {
                            getComicId();
                        }
                    }else{
                        // creates the bitemoji database
                        getComicId();
                    }
                    // gets the bitemoji image
                    getBitEmoji();
                } catch (Exception e) {
                    MessageAndTextUtils.error_alert(activity,null, 
                            activity.getString(R.string.SnapchatBitemojiDatabaseUpdateErrorMessage)+ e,
                            activity.getString(R.string.SnapchatBitemojiDatabaseUpdateErrorTitle));
                }
            }

            /**
             * This method is called when getting the snapchat users data fails it will display a error to the user
             * @param fail This is a boolean value set to true if the it failed to get the data.
             * @param code This is the error code of the failure. This will be displayed to the user.
             */
            @Override
            public void onFailure(boolean fail, int code) {
                if (code == 401){
                    MessageAndTextUtils.error_alert(activity,null, 
                            activity.getString(R.string.reauth_snapchat_is_required),
                            activity.getString(R.string.error_getting_snapchat_user_data));
                    login();
                }else{
                    MessageAndTextUtils.error_alert(activity,null, 
                            activity.getString(R.string.error_while_getting_snapchat_user_data)+ code,
                            activity.getString(R.string.error_getting_snapchat_user_data));
                }
            }
        });
    }

    /**
     * This methord is a private method that gets the bit emoji database and appends the users id to create a database in a json array format of the users bitemoji for the purpose of creating adaptive bitemoji carton strips to encouge positive action
     */
    private void getComicId(){
        StringRequest getBitEmojiJson = new StringRequest(Request.Method.GET, "https://api.bitmoji.com/content/templates",
                response -> {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONArray emojiData = jsonObject.getJSONArray("imoji");
                        JSONArray new_emoji_data = new JSONArray();
                        new_emoji_data.put(0,String.valueOf(Calendar.getInstance(TimeZone.getTimeZone("UTC")).getTimeInMillis()));
                        for (int i =1; i<emojiData.length()+1;i++){

                            JSONObject currentObject = emojiData.getJSONObject(i-1);

                            JSONObject tempIdObject = new JSONObject();

                            tempIdObject.put("id","https://sdk.bitmoji.com/render/panel/"+
                                    currentObject.getString("comic_id")+ "-"
                                    + snapChatsUserData.getBitmojiData().getId()+
                                    "-v1.png?transparent=1&palette=1");

                            tempIdObject.put("tags",currentObject.getJSONArray("tags"));
                            new_emoji_data.put(i, tempIdObject);
                        }

                        bitEmojiDataBase = new_emoji_data;

                        FileUtilities.write_bitemoji_data(bitEmojiDataBase,activity.getFilesDir()+"/bitemoji_cartoon.json");
                    } catch (Exception e) {
                        MessageAndTextUtils.alert(activity, null, 
                                activity.getString(R.string.SnapchatBitemojiDatabaseCreationErrorMessage) + e, 
                                activity.getString(R.string.SnapchatBitemojiDatabaseErrorTitle));
                    }

                },
                error -> MessageAndTextUtils.alert(activity,null, 
                        activity.getString(R.string.BitemojiDatabaseBug)+error.getMessage(),
                        activity.getString(R.string.BitEmojiDataBaseErrorTitle))
        );
        getBitEmojiJson.setRetryPolicy(retryPolicy);
        Volley.newRequestQueue(activity).add(getBitEmojiJson);
    }

    /**
     *  This method gets the users bit emoji and saves it to the file system
     */
    private void getBitEmoji() {
        if (snapChatsUserData == null){
            MessageAndTextUtils.alert(activity,null, "Error While getting bitemoji avatar image","Snap Chat bitemoji image error");
        }
                if (snapChatsUserData.getBitmojiData() != null) {
                        ImageRequest imageRequest = new ImageRequest(snapChatsUserData.getBitmojiData().getAvatar(), response -> {
                            FileUtilities.write_image(response,activity.getFilesDir()+"/avatar.png");
                            ImageView setup = activity.findViewById(R.id.bit_emoji_connected);
                            Button loginConnect = activity.findViewById(R.id.connect_bit_emoji__loginbtn);
                            if (setup != null){
                                setup.setImageBitmap(response);
                            }
                            if (loginConnect != null){
                                loginConnect.setVisibility(View.GONE);
                            }
                        }, 0, 0, ImageView.ScaleType.CENTER, null,
                                error -> MessageAndTextUtils.error_alert(activity,null, 
                                        activity.getString(R.string.SnapchatBitemojiImageErrorMessage)+ error.getMessage(),
                                        activity.getString(R.string.SnapchatBitemojiImageErrorTitle)));
                    imageRequest.setRetryPolicy(retryPolicy);
                    Volley.newRequestQueue(activity).add(imageRequest);
                }
    }


    /**
     * This method gets an images from the bitemoji database that match the search string search and exclude the value of exclude
     * @param search This is the tag that you want to find.
     * @param exclude This is a string array of tags you want to exclude from the search
     * @param listener This is the listener you want to be called if the result is found
     */
    public void getBitEmojiImage(String search, String[] exclude, SnapChatSearchImageObserver listener){
        String url = null;
        // Checks if the database is loaded into ram
        if (bitEmojiDataBase == null) {
            try {
                // Loads database into ram
                bitEmojiDataBase = FileUtilities.read_bitemoji_data(activity.getFilesDir() + "/bitemoji_cartoon.json");
            }catch (Exception e){
                // Alerts the user that their was a error
                MessageAndTextUtils.alert(activity,null, 
                        activity.getString(R.string.SnapchatBitemojiDatabaseErrorMessage)+e,
                        activity.getString(R.string.SnapchatBitemojiDatabaseErrorTitle));
                listener.onChanged("");
            }
        }
        try {
            List<String> found = new ArrayList<>();
            for (int i = 1; i< bitEmojiDataBase.length(); i++){
                int is_excluded = 0;
                String search_item =((JSONObject) bitEmojiDataBase.get(i)).getJSONArray("tags").toString();
                if (!(exclude[0].equals(""))) {
                    for (String s : exclude) {
                        if (search_item.contains(s)) {
                            is_excluded++;
                        }
                    }
                }
                if (search_item.contains(search) && is_excluded == 0) {
                    found.add(((JSONObject) bitEmojiDataBase.get(i)).getString("id"));
                }
            }
            if (found.isEmpty()){
                listener.onChanged("");
            }
            url = found.get( new Random().nextInt(found.size()));
        }catch(Exception e){
            MessageAndTextUtils.alert(activity,null, 
                    activity.getString(R.string.SnapChatBitEmojiImageErrorMessage)+e,
                    activity.getString(R.string.SnapChatBitEmojiImageErrorTitle));
        }
        listener.onChanged(url);
    }

    /**
     * This method searches the bitemojie database of the user and for images that try to change the users mood for example anger to funny item. It works by switch and case statments that search for 5 diffrent items based upon selected tags for invoking the correct action to invoke a diffrent mood. The mood is derived from the users notes although this could theoretically be gathered by combing a multitude of datapoints from hart rate to sleep status etc to form a better estimate of mood estimation. We could also use Voice and pitch in voice notes format to gather greater infomation.
     * @param profile This is the users profile object. It is used to get the value of the current mood.
     * @param listener This is the listener that returns the url for the bitemoji when ready for you to use
     */
    public void get_adaptive_pic(JSONObject profile, SnapChatUrlSearchObserver listener){
        int random_tag = new Random().nextInt(5); // gets a random int between 1 to 5 to chose a random tag from their current motions
        // This will chose a tag and picture to try change the users mood if a negative mood is their current mood
        try {
            String mood = profile.getString("current_mood");
            if (mood.compareTo("anger")== 0){
                switch (random_tag) {
                    case 1:
                        getBitEmojiImage("are you mad at me", new String[]{""}, listener::onChanged); // A funny bit emoji to make the user laugh and maybe subconsciously feel better due to grater empathy towards customisable avatars as found in a study in the dissertation document
                        break;
                    case 2:
                        getBitEmojiImage("calm", new String[]{""}, listener::onChanged);
                        break;
                    case 3:
                        getBitEmojiImage("funny", new String[]{""}, listener::onChanged);
                        break;
                    case 4:
                        getBitEmojiImage("meditation", new String[]{"current mood"}, listener::onChanged);
                        break;
                    case 5:
                        getBitEmojiImage("dog", new String[]{""}, listener::onChanged);
                        break;
                    default:
                        getBitEmojiImage("calm down", new String[]{""}, listener::onChanged);
                }
            }else if(mood.compareTo("disgust")== 0) {
                switch (random_tag) {
                    case 1:
                        getBitEmojiImage("motivate", new String[]{""}, listener::onChanged);
                        break;
                    case 2:
                        getBitEmojiImage("cat", new String[]{""}, listener::onChanged);
                        break;
                    case 3:
                        getBitEmojiImage("funny", new String[]{""}, listener::onChanged);
                        break;
                    case 4:
                        getBitEmojiImage("chill", new String[]{""}, listener::onChanged);
                        break;
                    case 5:
                        getBitEmojiImage("dog", new String[]{""}, listener::onChanged);
                        break;
                    default:
                        getBitEmojiImage("cute", new String[]{""}, listener::onChanged);
                }
            }else if(mood.compareTo("fear")== 0) {
                switch (random_tag) {// Similar tags to disgust since a similar region in the brain activating in both responses even some models conclude a disgust comes from fear which could explains ocd and can be counteracted with similar things
                    case 1:
                        getBitEmojiImage("motivate", new String[]{""}, listener::onChanged);
                        break;
                    case 2:
                        getBitEmojiImage("cute", new String[]{""}, listener::onChanged);
                        break;
                    case 3:
                        getBitEmojiImage("funny", new String[]{""}, listener::onChanged);
                        break;
                    case 4:
                        getBitEmojiImage("chill", new String[]{""}, listener::onChanged);
                        break;
                    case 5:
                        getBitEmojiImage("dog", new String[]{""}, listener::onChanged);
                        break;
                    default:
                        getBitEmojiImage("calm", new String[]{""}, listener::onChanged);
                }
            }else if(mood.compareTo("joy")== 0) {
                switch (random_tag) {
                    case 1:
                        getBitEmojiImage("yay", new String[]{""}, listener::onChanged);
                        break;
                    case 2:
                        getBitEmojiImage("yes", new String[]{"dear","accurate","sir","maam","eyes"}, listener::onChanged);
                        break;
                    case 3:
                        getBitEmojiImage("good", new String[]{"night","light bulb","you're not wrong",
                                "hangover","yoooo","encouragement","smells","don't feel",
                                "no","not","poop","goodbye","point","sounds","night"}, listener::onChanged);
                        break;
                    case 4:
                        getBitEmojiImage("happy", new String[]{"day","new year","year","holiday","thanksgiving",
                                "christmas","easter","unhappy","diwali","hanukkah","holi","fool","halloween","passover",
                                "anniversary","for you","cinco de mayo"}, listener::onChanged);
                        break;
                    case 5:
                        getBitEmojiImage("swag", new String[]{""}, listener::onChanged);
                        break;
                    default:
                        getBitEmojiImage("fire", new String[]{"dumpster","year","fired","fireworks","mad","true story","night"}, listener::onChanged);
                }
            }else if(mood.compareTo("sadness") == 0) {
                switch (random_tag) {
                    case 1:
                        getBitEmojiImage("compliments", new String[]{""}, listener::onChanged);
                        break;
                    case 2:
                        getBitEmojiImage("encouragement", new String[]{""}, listener::onChanged);
                        break;
                    case 3:
                        getBitEmojiImage("you can do it", new String[]{""}, listener::onChanged);
                        break;
                    case 4:
                        getBitEmojiImage("funny", new String[]{""}, listener::onChanged);
                        break;
                    case 5:
                        getBitEmojiImage("dog", new String[]{""}, listener::onChanged);
                        break;
                    default:
                        getBitEmojiImage("cheer up", new String[]{"cheers","drink","day"}, listener::onChanged);
                }
            }else{
                getBitEmojiImage("funny",new String[]{""}, listener::onChanged);
            }
        }catch (Exception e){
            MessageAndTextUtils.alert(activity,null, 
                    activity.getString(R.string.ErrorAdaptiveImage)+e,
                    activity.getString(R.string.BitEmojiImageError));
        }

    }

    /**
     * This is a login listener that listens for a response from snapchat. This handel's weather the user has successfully logged in or not giving the user a message
     * with the appropriate response.
     */
    private final LoginStateController.OnLoginStateChangedListener snapChatLoginListener =
            new LoginStateController.OnLoginStateChangedListener() {
                @Override
                public void onLoginSucceeded() {
                    getSnapChatUserData();
                    isLoggedIn = true;
                    MessageAndTextUtils.alert(activity,null,
                            activity.getString(R.string.SnapChatLoginMessage),
                            activity.getString(R.string.SnapChatLoginTitle));
                }

                @Override
                public void onLoginFailed() {
                    MessageAndTextUtils.error_alert(activity,null,
                            activity.getString(R.string.SnapchatLoginErrorMessage),
                            activity.getString(R.string.SnapchatLoginErrorTitle));
                }

                @Override
                public void onLogout() {
                    isLoggedIn = false;
                    MessageAndTextUtils.alert(activity,null,
                            activity.getString(R.string.SnapChatLogoutMessage),
                            activity.getString(R.string.SnapChatLogoutTitle));
                }
            };

    /**
     * A public interface that sends the url found in the search back to the user and displays it.
     */
    public interface SnapChatUrlSearchObserver {
        void onChanged(String url);
    }

    /**
     * A interface that sends the url if the searched image is found
     */
    public interface SnapChatSearchImageObserver {
        void onChanged(String url);
    }
}
