package com.example.personalised_assistant.ui_activities;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.example.personalised_assistant.R;
import com.example.personalised_assistant.snapchat_utils.SnapchatUtilities;
import com.example.personalised_assistant.utils.CryptographyFunctions;
import com.example.personalised_assistant.utils.FileUtilities;
import com.example.personalised_assistant.utils.GlobalVariables;
import com.example.personalised_assistant.utils.Login;
import com.example.personalised_assistant.utils.MessageAndTextUtils;
import com.example.personalised_assistant.utils.SecurityVerification;
import com.example.personalised_assistant.utils.SignUp;
import com.example.personalised_assistant.utils.UserProfile;
import com.example.personalised_assistant.watch_service.WatchService;
import com.samsung.android.sdk.healthdata.HealthDataStore;
import com.snapchat.kit.sdk.SnapLogin;

import java.io.IOException;
import java.security.GeneralSecurityException;

import static android.view.WindowManager.LayoutParams.FLAG_SECURE;

/**
 * This is the main class called when the application has started.
 */
public class MainActivity extends AppCompatActivity {

    /**
     * This is the snapchat object used to login and get images if the user is loged into snapchat or if the user has it in demo mode.
     */
    private SnapchatUtilities snapChatUtilitiesObject;
    /**
     * This is a pointer to the globalVariable which stores data needed across classes and has persistence until the application is closed.
     */
    private GlobalVariables globalVariable;

    /**
     * This method overrides the Activity method onCreate and will preform all the setup needed for the application calling setup or login depending if the user has already signed up or not.
     *
     * @param savedInstanceState This is the save instance from the last time the application or the activity has opened. This is not used in the application but is required parameter
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(FLAG_SECURE, FLAG_SECURE); // flag to block all screenshots and recordings. In applications dose not work in emulators with non native screen capture
        globalVariable = (GlobalVariables) getApplicationContext();
        UserProfile.setGlobalVars(globalVariable);
        globalVariable.setUserProfileIsError(true);
        try {
            // Gets from the preference file the users theme settings.
            boolean light = Boolean.parseBoolean(
                    CryptographyFunctions.getFromEncryptedSharedPreferences(
                            this, false,
                            "light_theme", "",
                            "false"));
            // sets the theme from the preference file
            if (light) {
                getTheme().applyStyle(R.style.light_theme, true);
            } else {
                getTheme().applyStyle(R.style.AppTheme, true);
            }
            // Gets from the preference file the users theme settings.
            boolean setup = Boolean.parseBoolean(
                    CryptographyFunctions.getFromEncryptedSharedPreferences(
                            this, false,
                            "setup", "",
                            "true"));
            // Sets the setup flag
            if (setup) {
                globalVariable.setSetUpFlag(true);
            }
        } catch (GeneralSecurityException | IOException e) {
            // On error notifies the user.
            MessageAndTextUtils.alert(this, null,
                    getString(R.string.ErrorPrefrenceMessage),
                    getString(R.string.themePrefreceError));
        }

        // Checks if the device is attached to a debugged here we would quit normally. This could be N0Ped out but makes it slightly harder
        if (SecurityVerification.isDebug()) {
            MessageAndTextUtils.alert(this, null,
                    getString(R.string.debuggerAttached),
                    getString(R.string.debuggerAttachedTItle));
        }
        // Checks if the device is a emulator
        boolean isEmulator = SecurityVerification.emulator_check();

        // Sets the is a emulator flag altering code execution to demo mode.
        globalVariable.setIsEmulator(isEmulator);
        if (isEmulator) {
            // Notified the user that they have triggered demo mode by the basic emulator detection
            MessageAndTextUtils.error_alert(this, null,
                    getString(R.string.runningInEmulator) +
                    getString(R.string.DemoModeLimmited)
                    , getString(R.string.DemoModeDetected));
            // trys to make demo files
            int result = FileUtilities.makeDemoFiles(this);
            // checks if it made demo mode files
            if (result != 1289905) {
                MessageAndTextUtils.error_alert(this,
                        (dialog, which) -> System.exit(0), // quits
                        getString(R.string.failedToMakeSetupFiles),
                        getString(R.string.fatalError));
            }
        } else {
            globalVariable.setWatchService();
        }

        // Checks if the user is rooted
        SecurityVerification.rootCheck(this);

        // Spawns a new thread to check if the device is safety net verified
        try {
            Thread safetyNetThread = new Thread(() -> SecurityVerification.googleAPI(this));
            safetyNetThread.start();
            safetyNetThread.join();
        } catch (InterruptedException e) {
            MessageAndTextUtils.error_alert(this, null,
                    getString(R.string.integrity_check_failed),
                    getString(R.string.safteynet_verification));
        }

        // Creates the snapchat object for bit emojis
        snapChatUtilitiesObject = new SnapchatUtilities(this);
        globalVariable.setSnapChatObject(snapChatUtilitiesObject);

        // Checks if the user is logged in to snapchat and gets user data
        if (snapChatUtilitiesObject.isUserLoggedIn()) {
            snapChatUtilitiesObject.getSnapChatUserData();
        }
        WatchService watchService = globalVariable.getWatchService();
        // Connects to the watch companion app
        if (watchService != null) {
            // Checks to see if their is any peers
            watchService.findPeers();
            boolean successful = watchService.sendData("Connected".getBytes(), this);
            if (!successful) {
                MessageAndTextUtils.alert(this, null,
                        getString(R.string.FailedToConnetWatch),
                        getString(R.string.FailedToConnetWatchTitle));
            }
        }


        // Checks if it is a emulator and disables samsung health integration
        if (!isEmulator) {
            globalVariable.setSamsungHealth(this);
        }
        // Checks if the user has opened the app for the first time
        if (globalVariable.getSetUpFlag()) {
            SignUp.start_setup(this);
        } else {
            setContentView(R.layout.activity_main);
            Login.login(this);
        }
    }

    /**
     * This method overrides the onResume method of activity and reconnects samsung health and the watch service
     */
    @Override
    protected void onResume() {
        super.onResume();
        if (!SecurityVerification.emulator_check()&&!globalVariable.getSetUpFlag()) {
            globalVariable.getSamsungHealth().samsungHealthDataStore.disconnectService();
            globalVariable.getSamsungHealth().samsungHealthDataStore = new HealthDataStore(this, globalVariable.getSamsungHealth().samsungHealthConnection);
            globalVariable.getSamsungHealth().samsungHealthDataStore.connectService();
            // Connects to the watch service.
            globalVariable.setWatchService();
        }
    }

    /**
     * This overrides the activity on destroy disconnecting the health service  and the watch service
     */
    @Override
    public void onDestroy() {
        if (!SecurityVerification.emulator_check()) {
            globalVariable.getSamsungHealth().samsungHealthDataStore.disconnectService();
            globalVariable.getWatchService().stopSelf();
        }
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (globalVariable.getSetUpFlag()) {
            SignUp.start_setup(this);
        }
    }

    /**
     * This method is called when the login button is clicked for snapchat
     *
     * @param v This is the view for the button
     */
    public void Bit_emoji_setup(View v) {
        snapChatUtilitiesObject = new SnapchatUtilities(this);
        snapChatUtilitiesObject.login();

        globalVariable.setSnapChatObject(snapChatUtilitiesObject);
        if (globalVariable.getSetUpFlag()) {
            findViewById(R.id.bit_emoji_connected).setVisibility(View.VISIBLE);
            findViewById(R.id.connect_bit_emoji_btn).setVisibility(View.GONE);
        }
    }
}
