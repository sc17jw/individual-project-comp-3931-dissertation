package com.example.personalised_assistant.ui_activities;

import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.personalised_assistant.R;
import com.example.personalised_assistant.snapchat_utils.SnapchatUtilities;
import com.example.personalised_assistant.utils.CryptographyFunctions;
import com.example.personalised_assistant.utils.GlobalVariables;
import com.google.android.material.navigation.NavigationView;
import com.samsung.android.sdk.healthdata.HealthDataStore;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.IOException;
import java.security.GeneralSecurityException;

import static android.view.Display.FLAG_SECURE;
import static androidx.drawerlayout.widget.DrawerLayout.DrawerListener;
/**
 * This class is the main activity of the application and sets up the navigation
 */
public class Home extends AppCompatActivity {
    /**
     * This is the application bar config.
     */
    private AppBarConfiguration appBarConfig;
    /**
     * This is a pointer to the globalVariable which stores data needed across classes and has persistence until the application is closed.
     */
    private GlobalVariables globalVariable;

    /**
     * Default constructor as specified in android docs empty
     */
    public Home(){}

    /**
     * This method setup the navigation controller and tool bar. It also sets the theme
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            boolean light = Boolean.parseBoolean(CryptographyFunctions.getFromEncryptedSharedPreferences(
                    this, false,
                    "light_theme", "",
                    "false"));

            if (light) {
                getTheme().applyStyle(R.style.light_theme_noaction, true);
            }

        } catch (GeneralSecurityException | IOException e) {
            e.printStackTrace();
        }


        getWindow().setFlags(FLAG_SECURE, FLAG_SECURE); // flag to block all screenshots and recordings.
        setContentView(R.layout.activity_home);

        globalVariable = (GlobalVariables) getApplicationContext();
        if (!globalVariable.isEmulator()) {
            globalVariable.getSamsungHealth().updateAllValues();
            globalVariable.writeUserProfileToDisk(this);
        }

        Toolbar toolbar = findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        ActionBar actionBar =getSupportActionBar();
        assert actionBar != null;
        actionBar.setDisplayHomeAsUpEnabled(true);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        drawer.addDrawerListener(drawerListener);
        appBarConfig = new AppBarConfiguration
                .Builder(R.id.nav_home, R.id.nav_notes, R.id.nav_food_recomend,
                R.id.nav_leaderbord, R.id.nav_achivements, R.id.nav_levels,R.id.nav_settings)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfig);
        NavigationUI.setupWithNavController(navigationView, navController);
        ((ImageView) navigationView.getHeaderView(0).findViewById(R.id.profile_pic_img_view)).
                setImageBitmap(
                        BitmapFactory.decodeFile(this.getFilesDir().toString() + "/avatar.png"));

    }

    /**
     * THis method will inflate the menu used in the navigator
     * @param menu this is the menu you want ot inflate
     * @return This will return true if successful
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu and adds items to the action bar
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    /**
     * This method changes the fragment
     * @return This is the new fragment
     */
    @Override
    public boolean onSupportNavigateUp() {
        getWindow().setFlags(FLAG_SECURE, FLAG_SECURE); // flag to block all screenshots and recordings.
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, appBarConfig)
                || super.onSupportNavigateUp();
    }

    /**
     * This method reconnects to shealth after resuming
     */
    @Override
    protected void onResume() {
        super.onResume();
        getWindow().setFlags(FLAG_SECURE, FLAG_SECURE); // flag to block all screenshots and recordings.
        if (!globalVariable.isEmulator()) {
            globalVariable.getSamsungHealth().samsungHealthDataStore.disconnectService();
            globalVariable.getSamsungHealth().samsungHealthDataStore = new HealthDataStore(this, globalVariable.getSamsungHealth().samsungHealthConnection);
            globalVariable.getSamsungHealth().samsungHealthDataStore.connectService();
        }
    }

    /**
     * This method overides the default destroy method to disconnect the shealth database
     */
    @Override
    public void onDestroy() {
        if (!globalVariable.isEmulator()) {
            globalVariable.getSamsungHealth().samsungHealthDataStore.disconnectService();
        }
        globalVariable.writeUserProfileToDisk(this);
        super.onDestroy();
    }

    /**
     * A listener that is called when the drawer of the main application is opened
     */
    private final DrawerListener drawerListener = new DrawerListener() {
        @Override
        public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {

        }
        // this changed the users picture to adapt to their feeling
        @Override
        public void onDrawerOpened(@NonNull View drawerView) {
            SnapchatUtilities snapChatUtilitiesObject = globalVariable.getSnapChatObject();
            JSONObject profile = globalVariable.getUserProfile();
            if (snapChatUtilitiesObject.isUserLoggedIn()|globalVariable.isEmulator()) {
                snapChatUtilitiesObject.get_adaptive_pic(profile, url ->
                        Picasso.get()
                                .load(url)
                                .placeholder(R.drawable.place_holder)
                                .error(R.drawable.place_holder)
                                .noFade()
                                .into((ImageView) findViewById(R.id.profile_pic_img_view)));
            }
        }

        @Override
        public void onDrawerClosed(@NonNull View drawerView) {

        }

        @Override
        public void onDrawerStateChanged(int newState) {
        }
    };

    /**
     * This method is called when the user clicks on the profile image
     * @param view This is the button that the user clicked on
     */
    public void go_to_profile(View view) {
        Navigation.findNavController(this,
                R.id.nav_host_fragment)
                .navigate(R.id.nav_profile); // navigates to the users profile
    }
}
