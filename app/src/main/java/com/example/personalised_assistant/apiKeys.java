package com.example.personalised_assistant;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.example.personalised_assistant.utils.CryptographyFunctions;
import com.example.personalised_assistant.utils.MessageAndTextUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;

/*** This class contains the api keys */
public class apiKeys {

    /*** This is the spoonacular api key*/
    public final static String SPOONACULAR_API_KEY ="apiKey=";
    /*** This is googles api key used to auth*/
    public static final String GOOGLE_API_KEY = "";

    /*** This is the caption api key used to generate captions*/
    public final static String GOOGLE_CAPTION_API_KEY = "";

    /**
     * This is the ibm instance url unique to my project and allows for rest calls to the api
     */
    private final static String IBM_SENTIMENT_INSTANCE = "";
    /**
     * This is the IBM url to get users sentiment
     */
    public final static String IBM_SENTIMENT_URL = IBM_SENTIMENT_INSTANCE + "/v3/tone?version=2016-05-19";
    /**
     * This is the IBM Api key
     */
    public final static String IBM_API_KEY = "";
}
