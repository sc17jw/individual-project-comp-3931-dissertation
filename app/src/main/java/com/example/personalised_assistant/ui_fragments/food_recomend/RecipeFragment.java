package com.example.personalised_assistant.ui_fragments.food_recomend;

import android.app.Activity;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.example.personalised_assistant.R;
import com.example.personalised_assistant.utils.GlobalVariables;
import com.example.personalised_assistant.utils.MessageAndTextUtils;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class RecipeFragment extends Fragment {
    private JSONObject selected_item;

    public RecipeFragment() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_recipy, container, false);
        LinearLayout layout = root.findViewById(R.id.recipty_layout);
        Activity activity = (Activity) root.getContext();
        GlobalVariables globalVariable = (GlobalVariables) activity.getApplicationContext();
        assert getArguments() != null;
        String response = FoodRecomendFragmentArgs.fromBundle(getArguments()).getResponce();
        try {
            selected_item = new JSONObject(globalVariable.getFoodItem());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        int normal_Text = 20;
        int Suitability_Text_Size = 30;
        int highlight_colour = getResources().getColor(R.color.colorPrimaryDark, null);
        try {
            JSONObject food_ingredience = new JSONObject(response);
            TextView title = MessageAndTextUtils.makeTextView(root.getContext(), "\n\n" + food_ingredience.get("title").toString()+"\n\n", true, highlight_colour, Suitability_Text_Size);
            title.setAllCaps(true);
            title.setGravity(View.TEXT_ALIGNMENT_CENTER);
            title.setPaintFlags(title.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);
            layout.addView(title);
            ImageView food_picture = new ImageView(root.getContext());
            layout.addView(food_picture);
            Picasso.get()
                    .load(food_ingredience.get("image").toString())
                    .placeholder(R.drawable.place_holder)
                    .error(R.drawable.place_holder)
                    .into(food_picture);
            layout.addView(MessageAndTextUtils.makeTextView(root.getContext(), "\nCooking Time :" + food_ingredience.get("readyInMinutes").toString(), true, highlight_colour, Suitability_Text_Size));
            layout.addView(MessageAndTextUtils.makeTextView(root.getContext(), "Servings :" + food_ingredience.get("servings").toString(), true, highlight_colour, Suitability_Text_Size));

            layout.addView(MessageAndTextUtils.makeTextView(root.getContext(), "Is Vegetarian :" + food_ingredience.get("vegetarian").toString(), true, highlight_colour, Suitability_Text_Size));
            layout.addView(MessageAndTextUtils.makeTextView(getContext(), "Is vegan :" + food_ingredience.get("vegan").toString(), true, highlight_colour, Suitability_Text_Size));
            layout.addView(MessageAndTextUtils.makeTextView(getContext(), "Is glutenFree :" + food_ingredience.get("glutenFree").toString(), true, highlight_colour, Suitability_Text_Size));
            layout.addView(MessageAndTextUtils.makeTextView(getContext(), "Is dairyFree :" + food_ingredience.get("dairyFree").toString(), true, highlight_colour, Suitability_Text_Size));
            layout.addView(MessageAndTextUtils.makeTextView(getContext(), "Is Healthy :" + food_ingredience.get("veryHealthy").toString(), true, highlight_colour, Suitability_Text_Size));
            layout.addView(MessageAndTextUtils.makeTextView(getContext(), "Is cheap :" + food_ingredience.get("cheap").toString(), true, highlight_colour, Suitability_Text_Size));
            layout.addView(MessageAndTextUtils.makeTextView(getContext(), "Is sustainable :" + food_ingredience.get("sustainable").toString() + "\n\n", true, highlight_colour, Suitability_Text_Size));

            layout.addView(MessageAndTextUtils.makeTextView(getContext(), Html.fromHtml(food_ingredience.get("summary").toString(), 0).toString(), false, 0, normal_Text));
            layout.addView(MessageAndTextUtils.makeTextView(getContext(), "\nWeight Watcher Smart Points :" + food_ingredience.get("weightWatcherSmartPoints").toString(), true, highlight_colour, Suitability_Text_Size));
            layout.addView(MessageAndTextUtils.makeTextView(getContext(), "Spoonacular Score :" + food_ingredience.get("spoonacularScore").toString(), true, highlight_colour, Suitability_Text_Size));
            layout.addView(MessageAndTextUtils.makeTextView(getContext(), "Health Score :" + food_ingredience.get("healthScore").toString(), true, highlight_colour, Suitability_Text_Size));
            layout.addView(MessageAndTextUtils.makeTextView(getContext(), "Nutritional Information :\n", true, highlight_colour, Suitability_Text_Size));
            layout.addView(MessageAndTextUtils.makeTextView(getContext(), "Calories: "+selected_item.get("calories")+"\nProtein:"+selected_item.get("protein"), true, highlight_colour, Suitability_Text_Size));
            layout.addView(MessageAndTextUtils.makeTextView(getContext(), "Carbohydrate: "+selected_item.get("carbs")+"\nCalcium: "+selected_item.get("calcium"), true, highlight_colour, Suitability_Text_Size));
            layout.addView(MessageAndTextUtils.makeTextView(getContext(), "SaturatedFat: "+selected_item.get("saturatedFat")+"\nVitamin A: "+selected_item.get("vitaminA"), true, highlight_colour, Suitability_Text_Size));
            layout.addView(MessageAndTextUtils.makeTextView(getContext(), "Vitamin C: "+selected_item.get("vitaminC")+"\n Fiber: "+selected_item.get("fiber"), true, highlight_colour, Suitability_Text_Size));
            layout.addView(MessageAndTextUtils.makeTextView(getContext(), "Iron: "+selected_item.get("iron")+"\nPotassium: "+selected_item.get("potassium"), true, highlight_colour, Suitability_Text_Size));
            layout.addView(MessageAndTextUtils.makeTextView(getContext(), "Sodium: "+selected_item.get("sodium")+"\nSugar: "+selected_item.get("sugar")+"\n", true, highlight_colour, Suitability_Text_Size));

            JSONArray ingrediance = food_ingredience.getJSONArray("extendedIngredients");
            for (int i = 0; i < ingrediance.length(); i++) {
                JSONObject current_ingrediance = ingrediance.getJSONObject(i);
                JSONObject amount = current_ingrediance.getJSONObject("measures").getJSONObject("metric");
                // adds information about the ingredient
                layout.addView(MessageAndTextUtils.makeTextView(getContext(), "\nItem Name :" + current_ingrediance.get("original"), true, highlight_colour, Suitability_Text_Size));
                layout.addView(MessageAndTextUtils.makeTextView(getContext(), "Amount :" + amount.get("amount") + amount.get("unitShort") , true, highlight_colour, Suitability_Text_Size));
                layout.addView(MessageAndTextUtils.makeTextView(getContext(), "Store aisle Location :" + current_ingrediance.get("aisle")+ "\n", true, highlight_colour, Suitability_Text_Size));

            }

            // Tells the user if a wine is good with the meal.
            // This could be disabled as a option if the user is alcoholic.
            // This data is not in the user profile so default added.

            JSONObject wine = food_ingredience.getJSONObject("winePairing");
            if (!wine.isNull("pairedWines")) {
                for (int i = 0; i < wine.getJSONArray("pairedWines").length(); i++) {
                    layout.addView(MessageAndTextUtils.makeTextView(getContext(), "Suggested Wines :" + wine.getJSONArray("pairedWines").get(i).toString(), false, 0, normal_Text));
                }
                if (wine.getString("pairingText").compareTo("") != 0) {
                    layout.addView(MessageAndTextUtils.makeTextView(getContext(), "Wine Comment :" + wine.get("pairingText"), false, 0, normal_Text));
                }
            }
            // Adds the instruction for the recipe
            if (food_ingredience.getJSONArray("analyzedInstructions").length()!=0){
                JSONArray Recipe_Steps_Json = food_ingredience.getJSONArray("analyzedInstructions").getJSONObject(0).getJSONArray("steps");
                layout.addView(MessageAndTextUtils.makeTextView(getContext(), "\n\nInstructions: \n", true, highlight_colour, Suitability_Text_Size));
                for (int i = 0; i< Recipe_Steps_Json.length();i++){
                    JSONArray ingredients =  Recipe_Steps_Json.getJSONObject(i).getJSONArray("ingredients");
                    layout.addView(MessageAndTextUtils.makeTextView(getContext(), "Step "+(i+1)+": "+Recipe_Steps_Json.getJSONObject(i).getString("step")+"\n", true, highlight_colour, Suitability_Text_Size));
                    String temp = "";
                    for (int a = 0;a<ingredients.length();a++){
                        if (a == (ingrediance.length()-1)){
                            temp = temp.concat(ingrediance.getJSONObject(a).getString("name"));
                        }else {
                            temp = temp.concat(ingrediance.getJSONObject(a).getString("name") + " ,");
                        }
                    }
                    if (ingredients.length() != 0) {
                        layout.addView(MessageAndTextUtils.makeTextView(getContext(),getString(R.string.Ingredients_needed_for_step)+temp+"\n", true, highlight_colour, Suitability_Text_Size));
                    }
                    JSONArray equipment =  Recipe_Steps_Json.getJSONObject(i).getJSONArray("equipment");
                    temp = "";
                    for (int a = 0;a<equipment.length();a++){
                        if (a == (equipment.length()-1)){
                            temp = temp.concat(equipment.getJSONObject(a).getString("name"));
                        }else {
                            temp = temp.concat(equipment.getJSONObject(a).getString("name") + " ,");
                        }
                    }
                    if (equipment.length() != 0) {
                        layout.addView(MessageAndTextUtils.makeTextView(getContext(),getString(R.string.Equipment_needed_for_step)+temp+"\n", true, highlight_colour, Suitability_Text_Size));
                    }
                }
            } else if (!food_ingredience.isNull("instructions")) {
                layout.addView(MessageAndTextUtils.makeTextView(getContext(), getString(R.string.Instructions_string_resorce) + food_ingredience.getString("instructions"), true, highlight_colour, Suitability_Text_Size));
            }
            Button add_too_samsung_food = new Button(activity);
            add_too_samsung_food.setText(R.string.add_to_samung_health_food_info);
            layout.addView(add_too_samsung_food);
            add_too_samsung_food.setOnClickListener(v -> {
                if (globalVariable.isEmulator()){
                    MessageAndTextUtils.alert(activity,null,"Emulators have Shealth disabled", " Emulator");
                    return;
                }
                add_too_samsung_food.setClickable(false);
                globalVariable.getSamsungHealth().set_food(selected_item,(Activity) root.getContext(),add_too_samsung_food);
            });
        } catch (Exception e) {
            MessageAndTextUtils.error_alert(activity, null, getString(R.string.error_phasing_recipe) + e, getString(R.string.Recipe_Fragment));
        }
        return root;
    }

}
