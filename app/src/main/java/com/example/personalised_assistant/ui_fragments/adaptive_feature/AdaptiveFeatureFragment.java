package com.example.personalised_assistant.ui_fragments.adaptive_feature;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.personalised_assistant.R;
import com.example.personalised_assistant.utils.GamerTypeAndPersonality;
import com.example.personalised_assistant.utils.GlobalVariables;
import com.example.personalised_assistant.utils.MessageAndTextUtils;
import com.example.personalised_assistant.utils.UserProfile;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * This classed preforms one of the adaptive features of the application. It changes and adapts the bottom adaptation bar of the program based upon the users stereotype
 */
public class AdaptiveFeatureFragment extends Fragment {
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.level_bar_fragment, container, false);
        Activity activity = (Activity) root.getContext();
        GlobalVariables globalVariable = (GlobalVariables) activity.getApplicationContext();
        String directory = activity.getFilesDir().toString();
        try {

            JSONObject profile = globalVariable.getUserProfile();
            int totalCount = profile.getInt("total_steps");
            int levelCount = profile.getInt("level");
            double noteSentiment = profile.getDouble("note_sentiment");
            int position = UserProfile.createLeaderBoard(levelCount, profile.getString("name"), activity.getFilesDir().toString(), globalVariable);

            long next = (long) ((levelCount * 1000 * 1.25) - totalCount);
            float percents = ((float) totalCount / (float) (next + totalCount)) * 100;

            TextView temp = root.findViewById(R.id.temp_adaptive_feature_txtview);
            temp.setVisibility(View.VISIBLE);
            // This will attempt to load the users personality object from the user profile.
            JSONObject bigFivePersonality = new JSONObject(profile.getString("personality"));
            // This will get the users gamer type from the bigFivePersonality jsonObject data
            String gamerType = GamerTypeAndPersonality.getAdaptType(activity,bigFivePersonality);

            switch (gamerType) {
                case "Player":
                    // Displays the Player adaptive feature to the user.
                    // This will display how close they are to leveling up
                    String playerMessage = getString(R.string.you_are) + percents + getString(R.string.percent_towards_next_level);
                    temp.setText(playerMessage);
                    ProgressBar progress_to_next_level = root.findViewById(R.id.progressBar2);
                    progress_to_next_level.setVisibility(View.VISIBLE);
                    progress_to_next_level.setMax((int) Math.pow(100, levelCount));
                    progress_to_next_level.setProgress(totalCount);
                    break;
                case "Free Spirt":
                    // Displays the Free Spirit adaptive feature to the user.
                    // This is the users note sentiment with a encouraging message
                    if (noteSentiment > 0.5) {
                        temp.setText(R.string.note_positive_message);
                    } else {
                        temp.setText(R.string.note_negitive_message);
                    }
                    break;
                case "Socliser":
                    // Displays the Socliser adaptive feature to the user. This is the number of steps they are
                    // From over taking their closet friend
                    String socialiserMessage;
                    if (position != 1) {
                        socialiserMessage = getString(R.string.you_are) +
                                next + getString(R.string.stepsTowardsNextLevelUASsociliser) +
                                UserProfile.getLeaderBoard(directory, globalVariable)
                                        .getJSONObject(levelCount).get("name").toString();
                    } else {
                        socialiserMessage = getResources().getString(R.string.first_place_adaptive_message);
                    }
                    temp.setText(socialiserMessage);

                    break;
                default:
                    // Displays the achiever adaptive feature to the user. Tells the user how many steps they are
                    // away from leveling up.
                    String achievement = getString(R.string.you_are) + next +
                            getString(R.string.steps_way_from_getting_next_achivment);
                    temp.setText(achievement);
                    break;
            }
        } catch (JSONException e) {
            // Displays a error message to the user
            MessageAndTextUtils.error_alert(activity, null ,
                    getString(R.string.creatingUserAdaptiveFeatureError),
                    getString(R.string.UserAdaptiveFragmentErrorTItle));
        }

        return root;
    }

}
