package com.example.personalised_assistant.ui_fragments.food_recomend;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.personalised_assistant.R;
import com.example.personalised_assistant.apiKeys;
import com.example.personalised_assistant.samsung_health.RecommendedIntake;
import com.example.personalised_assistant.utils.GlobalVariables;
import com.example.personalised_assistant.utils.MessageAndTextUtils;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class FoodRecomendFragment extends Fragment {
    private View root;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_food_recomened, container, false);

        int text_size = 14;
        TableLayout table_layout = root.findViewById(R.id.table_main);
        String url = "https://api.spoonacular.com/recipes/findByNutrients?"+ apiKeys.SPOONACULAR_API_KEY;
        Activity activity = (Activity) root.getContext();
        GlobalVariables globalVariable = (GlobalVariables) activity.getApplicationContext();
        if (!globalVariable.isEmulator()) {
            globalVariable.getSamsungHealth().updateNutritionData();
        }
        try {
            JSONObject profile = globalVariable.getUserProfile();
            JSONObject nutrition = new JSONObject(profile.getString("nutrition"));
            RecommendedIntake RecomendedIntake = new RecommendedIntake(profile.getInt("age"),profile.getString("gender"));
            Float [] recomended = RecomendedIntake.getRecommendFoodIntake();
            String[] api_key_max = new String[]{
                    "&maxCalories=","&maxCalcium=","&maxCarbs=","",
                    "&maxFiber=","&maxIron=","&maxSaturatedFat=",
                    "&maxPotassium=","&maxProtein=","&maxSodium=",
                    "&maxSugar=","&maxFat=","","&maxVitaminA=",
                    "&maxVitaminC="
            };
            String[] api_key = new String[]{
                    "&minCalories=","&minCalcium=","&minCarbs=","",
                    "&minFiber=","&minIron=","&minSaturatedFat=",
                    "&minPotassium=","&minProtein=","&minSodium=",
                    "&maxSugar=","&minFat=","","&minVitaminA=",
                    "&minVitaminC="
            };
            boolean vitamins_over = false;
            int meal_count = Integer.parseInt(nutrition.getString("MEAL_COUNT"));
            for (int i = 0; i < recomended.length;++i){
                //NOT SEARCHABLE ITEMS Too little results or api not support.
                //Maybe set maximum over recommended amount and have a minimum of the recommended amount ? more research is needed
                if(i== 12 || i == 3) {
                    continue;
                }
                recomended[i]= recomended[i]-Float.parseFloat(nutrition.getString(RecommendedIntake.key[i]));
                if (vitamins_over && (i == 14 || i == 13)) {
                    url = url.concat(api_key_max[i] + 10);
                    continue;
                }
                    if (recomended[i] <= 0){
                        //Still Recommend users food.
                        if (i == 1)
                            vitamins_over = true;
                        if (i == 9) {
                            url = url.concat(api_key_max[i] + 100);
                            continue;
                        }
                        url=url.concat(api_key_max[i] + 10);
                    }else {
                        // multiple sodium value to get in milligrams
                        if (i == 0&&!globalVariable.isEmulator()){
                            // Adds a users active calories burned to the recommended the recommend doesnt account
                            // for more burned due to activity
                            recomended[i] = recomended[i] + globalVariable.getSamsungHealth().getCaloriesBurned()[0];
                        }
                        if(meal_count == 0){
                            url=url.concat(api_key[i] + (recomended[i] / 6));
                        }else if ( meal_count < 6) {
                            url=url.concat(api_key[i] + (recomended[i] / (6-meal_count)));
                        }else {
                            //For values of sugger and vitamin a and c
                            url=url.concat(api_key[i] + (recomended[i]));
                        }
                    }
                }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        StringRequest get_food_rec_request = new StringRequest(Request.Method.GET, url+"&random=true",
                response -> {
                    try {
                        JSONArray json_array = new JSONArray(response);
                        if (json_array.length() == 0){
                            MessageAndTextUtils.error_alert(getActivity(),null,
                                    getString(R.string.ErrorGettingRecommend),
                                    getString(R.string.ErrorGettingRecommendTitle));
                            return;
                        }
                        if (getContext() == null){
                            return;
                        }
                        TableRow table_row_player = new TableRow(getContext());
                        table_row_player.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

                        TextView text_view_id = new TextView(getContext());
                        TextView text_view_title = new TextView(getContext());
                        TextView text_view_image_title = new TextView(getContext());

                        text_view_id.setTextSize(text_size);
                        text_view_title.setTextSize(text_size);
                        text_view_image_title.setTextSize(text_size);

                        text_view_id.setText(R.string.ID);
                        text_view_title.setText(R.string.Title);
                        text_view_title.setText(R.string.image);

                        table_row_player.addView(text_view_id);
                        table_row_player.addView(text_view_title);
                        table_row_player.addView(text_view_image_title);

                        table_layout.addView(table_row_player);
                        for (int i = 0; i < json_array.length(); i++) {
                            table_row_player = new TableRow(getContext());
                            table_row_player.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

                            text_view_id = new TextView(getContext());
                            text_view_title = new TextView(getContext());
                            ImageView text_view_image = new ImageView(getContext());

                            text_view_id.setTextSize(text_size);
                            text_view_title.setTextSize(text_size);
                            String id = json_array.getJSONObject(i).get("id").toString() + " ";
                            text_view_id.setText(id);
                            String title = json_array.getJSONObject(i).get("title").toString();
                            text_view_title.setText(title);
                            Picasso.get()
                                    .load(json_array.getJSONObject(i).get("image").toString())
                                    .placeholder(R.drawable.place_holder)
                                    .error(R.drawable.place_holder)
                                    .resize(100, 100)
                                    .into(text_view_image);

                            table_row_player.setClickable(true);
                            table_row_player.setId(i);
                            table_row_player.setOnClickListener(getRecipeOnClick);
                            table_row_player.addView(text_view_id);
                            table_row_player.addView(text_view_title);
                            table_row_player.addView(text_view_image);
                            table_layout.addView(table_row_player);
                        }
                        globalVariable.setFoodItem(response);
                    } catch (JSONException e) {
                        MessageAndTextUtils.error_alert(getActivity(),
                                null,
                                getString(R.string.AddingItemToTable)+response,
                                getString(R.string.GettingRecommendErrorTitle));
                    }
                },
                error -> {
            if (error.networkResponse.statusCode == 500){
                MessageAndTextUtils.error_alert(getActivity(),
                        null,
                        getString(R.string.noRecommend) + error.getMessage(),
                        getString(R.string.ErrorRecommendTitile));
            }else {
                MessageAndTextUtils.error_alert(getActivity(),
                        null,
                        "Error " + error.getMessage() + " " + error.getLocalizedMessage()
                                + getString(R.string.PacketHeader) + error.networkResponse.statusCode,
                        getString(R.string.ErrorGettingRecommendedTitle));
                error.printStackTrace();
            }
            }
        );
        get_food_rec_request.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(root.getContext()).add(get_food_rec_request);
        return root;
    }

    private final View.OnClickListener getRecipeOnClick = view -> {

        TableRow table_row_player = view.findViewById(view.getId());
        String url = "https://api.spoonacular.com/recipes/" + (
                (TextView) table_row_player.getChildAt(0))
                .getText().toString().replace(" ", "") + "/information?"+
                apiKeys.SPOONACULAR_API_KEY;

        StringRequest get_food_rec_request = new StringRequest(Request.Method.GET, url,
                response -> {
                    Activity activity = (Activity) this.getContext();
                    assert activity != null;
                    GlobalVariables globalVariable = (GlobalVariables) activity.getApplicationContext();
                    JSONObject selected_food;
                    try {
                        TableRow item_selected = view.findViewById(view.getId());
                        int id = Integer.parseInt(((TextView)item_selected.getChildAt(0)).getText().toString().replace(" ", ""));
                        JSONArray item = new JSONArray(globalVariable.getFoodItem());
                        for (int i = 0; i < item.length(); i++) {
                            if (item.getJSONObject(i).getInt("id") == id){
                                selected_food = item.getJSONObject(i);
                                globalVariable.setFoodItem(selected_food.toString());
                                break;
                            }
                        }
                    } catch (JSONException e) {
                        MessageAndTextUtils.error_alert(getActivity(),
                                null,
                                getString(R.string.Error)+e,
                                getString(R.string.reciepyError));
                        e.printStackTrace();
                    }
                    Navigation
                            .findNavController(root)
                            .navigate(FoodRecomendFragmentDirections.
                                    actionNavFoodRecomendToRecipyFragment(response));
                }, error -> {
            MessageAndTextUtils.error_alert(getActivity(),null,
                    getString(R.string.Error)+ error.getMessage(),
                    getString(R.string.GettingRecipyTitle));
            error.printStackTrace();
        });

        get_food_rec_request.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(view.getContext()).add(get_food_rec_request);
    };
}