package com.example.personalised_assistant.ui_fragments.notes;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.fragment.app.Fragment;

import com.example.personalised_assistant.R;
import com.example.personalised_assistant.utils.FileUtilities;
import com.example.personalised_assistant.utils.GlobalVariables;
import com.example.personalised_assistant.utils.MessageAndTextUtils;
import com.example.personalised_assistant.utils.SentimentCalculator;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * This class is a fragment that displays the user notes and their analysed values
 */
public class ViewNoteFragment extends Fragment {
    /**
     * This is a constant size of the text for this fragment.
     */
    private static final int TEXT_SIZE = 50;

    /**
     * This is the default constructor for the fragment as specified in android documents this has to be blank
     */
    public ViewNoteFragment() {
    }

    /**
     * This method will create the view for the note fragment and add the users note and note analysis.
     *
     * @return This will return the inflated view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // inflates the view
        View root = inflater.inflate(R.layout.fragment_view_note_fragment, container, false);
        // sets the global variables object for the method
        GlobalVariables globalVariable = (GlobalVariables) requireActivity().getApplicationContext();
        // Finds the layout to add the text to
        LinearLayout layout = root.findViewById(R.id.view_note_layout);
        // Creates the note json object from the user.
        JSONObject userNote;
        try {
            // Makes sure that the arguments are not null
            assert getArguments() != null;
            // Checks if its a emulator to alter the notes from encrypted to none encrypted due to demo mode users not having encryption since its a demo.
            // This is because of how the key is generated. This could be changed to a keyStore generated key but would be rather redundant
            if (globalVariable.isEmulator()) {
                // Loads the unencrypted demo note
                userNote = FileUtilities.load_demo(
                        ViewNoteFragmentArgs.fromBundle(getArguments()).getFilePath()
                );
            } else {
                // Loads the encrypted note
                userNote = FileUtilities.load_json_object(
                        ViewNoteFragmentArgs.fromBundle(getArguments()).getFilePath(),
                        globalVariable.getKey()
                );
            }
        } catch (Exception e) {
            // Displays a error message to the user.
            MessageAndTextUtils.alert(requireActivity(), null,
                    getString(R.string.ErrorOperningNotes) + e,
                    getString(R.string.UserNoteErrorTitle));
            return root;
        }
        try {
            // Gets the color for the text.
            int color = getResources().getColor
                    (R.color.colorPrimaryDark,
                            requireActivity().getTheme());
            // makes sure userNote is not null
            assert userNote != null;
            // Gets the note from the file
            String note = userNote.getString("note");
            // loads the analysed values from the file.
            double anger = userNote.getDouble("anger"), disgust = userNote.getDouble("disgust"),
                    fear = userNote.getDouble("fear"), joy = userNote.getDouble("joy"),
                    sadness = userNote.getDouble("sadness"), sentiment = userNote.getDouble("sentiment"),
                    magnitude = userNote.getDouble("magnitude");

            // Determines the feeling of the note being positive, negative, neutral or not able to tell
            String Feeling = SentimentCalculator.feeling(magnitude, sentiment, note);

            // Adds the note to the view
            layout.addView(MessageAndTextUtils.makeTextView(
                    root.getContext(), note + "\n",
                    false, 0, TEXT_SIZE));

            // Adds the sentiment value of the note into the view
            layout.addView(MessageAndTextUtils.makeTextView(
                    root.getContext(), getString(R.string.sentimentViewNote) + sentiment,
                    true, color, TEXT_SIZE));

            // Adds the magnitude value of the note into the view
            layout.addView(MessageAndTextUtils.makeTextView(
                    root.getContext(), getString(R.string.MagnitudeViewNote) + magnitude,
                    true, color, TEXT_SIZE));

            // Adds the feeling (positive, negative, neutral or not able to tell ) of the note into the view
            layout.addView(MessageAndTextUtils.makeTextView(
                    root.getContext(), getString(R.string.FeelingViewNote) + Feeling,
                    true, color, TEXT_SIZE));

            // Adds the probability of Anger detected in the text
            layout.addView(MessageAndTextUtils.makeTextView(
                    root.getContext(), getString(R.string.AngerViewNote) + anger,
                    true, color, TEXT_SIZE));

            // Adds the probability of Anger detected in the text
            layout.addView(MessageAndTextUtils.makeTextView(
                    root.getContext(), getString(R.string.DisgustViewNote) + disgust,
                    true, color, TEXT_SIZE));

            // Adds the probability of Fear detected in the text
            layout.addView(MessageAndTextUtils.makeTextView(
                    root.getContext(), getString(R.string.FearViewNote) + fear,
                    true, color, TEXT_SIZE));

            // Adds the probability of Joy detected in the text
            layout.addView(MessageAndTextUtils.makeTextView(
                    root.getContext(), getString(R.string.JoyViewNote) + joy,
                    true, color, TEXT_SIZE));

            // Adds the probability of Sadness detected in the text
            layout.addView(MessageAndTextUtils.makeTextView(
                    root.getContext(), getString(R.string.SadnessViewNote) + sadness,
                    true, color, TEXT_SIZE));

        } catch (JSONException e) {
            // Displays a error message to the user.
            MessageAndTextUtils.alert(requireActivity(), null,
                    getString(R.string.ErrorMessageViewNote) + e,
                    getString(R.string.ErrorViewFragment));

        }
        // returns the root view
        return root;
    }
}