package com.example.personalised_assistant.ui_fragments.home;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.example.personalised_assistant.R;
import com.example.personalised_assistant.snapchat_utils.SnapchatUtilities;
import com.example.personalised_assistant.utils.GlobalVariables;
import com.example.personalised_assistant.watch_service.WatchService;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;

/**
 *  This class is displayed when the user has logged ing , acting as a hub of information and home page
 */
public class HomeFragment extends Fragment {
    /**
     *  The inflated view used to get the navigation controller to change to graph fragment
     */
    private View root;

    /**
     *  A empty constructor as the documents say require
     */
    public HomeFragment(){}

    /**
     * This is called when the fragment is made and creates the users home page displaying infomation about the user
     *
     * @return This will return the inflated view for the fragment
     */
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        root = inflater.inflate(R.layout.fragment_home, container, false);
        // Gets the global variables object
        GlobalVariables globalVariable = (GlobalVariables) root.getContext().getApplicationContext();
        // Gets the snapchat object for getting adaptive images.
        SnapchatUtilities snapChatUtilitiesObject = globalVariable.getSnapChatObject();
        // Creates a local copy of the user profile
        JSONObject profile = globalVariable.getUserProfile();
        // gets the table layout to add new items.
        TableLayout table_layout = root.findViewById(R.id.table_main);
        int text_size = 30;
        // Keys of all the data to be added to table
        String[] key={"total_steps","level","sleep_streak","max_sleep_streak","total_cals_consumed","total_cals_burned","nutrition","personality","exercises","hart_rate","sleep_data"};
        String[] data_name={"Total Steps","Level","Sleep Streak","Longest\nSleep Streak","Total Calories\nConsumed","Total Calories\nBurned","Nutrition","Personality\nType","Exercise\nData","Hart-Rate\nData","Sleep\nData",};
        // For loop adding all the data to the table
        for (int i = 0; i < key.length; i++) {
            TableRow tableRowPlayer = new TableRow(getContext());
            tableRowPlayer.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

            TextView textViewId = new TextView(getContext());
            TextView text_view_title = new TextView(getContext());

            textViewId.setTextSize(text_size);
            text_view_title.setTextSize(text_size);
            text_view_title.setGravity(View.TEXT_ALIGNMENT_CENTER);
            textViewId.setText(data_name[i]);
            if (i < 6) {
                try {
                    text_view_title.setText(globalVariable.getUserProfile().get(key[i]).toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else{
                text_view_title.setText(R.string.tapForMoreInfo);
            }
            table_layout.addView(tableRowPlayer);
            tableRowPlayer.setClickable(true);
            tableRowPlayer.setId(i);
            tableRowPlayer.setOnClickListener(onClickGetItemGraph);
            tableRowPlayer.addView(textViewId);
            tableRowPlayer.addView(text_view_title);
            // Adds some padding
            tableRowPlayer.setPaddingRelative(5,5,50,5);

        }
        // Checks if the user is logged in or if the application is in demo mode.
        if (snapChatUtilitiesObject.isUserLoggedIn()||globalVariable.isEmulator()) {
            // Find the image to replace with the
            ImageView imageView = root.findViewById(R.id.adaptive_image_one);
            snapChatUtilitiesObject.get_adaptive_pic(profile, url -> {
                Picasso.get()
                        .load(url)
                        .placeholder(R.drawable.place_holder)
                        .error(R.drawable.place_holder)
                        .noFade()
                        .into(imageView);
                // Creates a bitmap of the image and sends it to the watch
                Bitmap bitmap = Bitmap.createBitmap(680, 480, Bitmap.Config.ARGB_8888);
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
                // gets the watch service
                WatchService watchService = globalVariable.getWatchService();
                //Checks if its connected
                if (watchService != null) {
                    // Try's to find watches
                    watchService.findPeers();
                    // sets the byte array image to be decoded
                    watchService.sendData(outputStream.toByteArray(), getActivity());
                }
            });

        }
        return root;
    }

    /**
     *  On click event for changing to the graph fragment and information about the item
     */
    private final View.OnClickListener onClickGetItemGraph = view -> {
        TableRow tableRowPlayer = view.findViewById(view.getId());
        // changes to the graph fragment
        Navigation.findNavController(root).
                navigate(HomeFragmentDirections.
                        actionNavHomeToGraphFragment(
                                ((TextView)tableRowPlayer.getChildAt(0)).getText().toString()));
    };
}
