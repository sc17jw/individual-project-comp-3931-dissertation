package com.example.personalised_assistant.ui_fragments.settings;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;

import androidx.fragment.app.Fragment;

import com.example.personalised_assistant.R;
import com.example.personalised_assistant.utils.CryptographyFunctions;
import com.example.personalised_assistant.utils.MessageAndTextUtils;
import com.example.personalised_assistant.utils.UserProfile;

import java.io.IOException;
import java.security.GeneralSecurityException;

/**
 * This is the settings fragment and is used to change settings
 */
public class SettingsFragment extends Fragment {
    /**
     * This is a empty constructor as the documents say is required
     */
    public SettingsFragment() { }

    /**
     * This method runs when the fragment is created and will allow the user to alter settings
     * eg delete user profile, go to accessibility menu  and change theme
     *
     * @return Returns a inflated view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_settings, container, false);
        // Changes theme
        root.findViewById(R.id.theme_switch_Settings).setOnClickListener(v ->{
            try {
                if (((Switch) v).isChecked()) {
                    CryptographyFunctions.getFromEncryptedSharedPreferences(
                            requireActivity(), true,
                            "light_theme", "true",
                            "true");
                    requireActivity().recreate();
                } else {

                    CryptographyFunctions.getFromEncryptedSharedPreferences(
                            requireActivity(), true,
                            "light_theme", "false",
                            "false");

                    requireActivity().recreate();
                }
            } catch (GeneralSecurityException | IOException e) {
                MessageAndTextUtils.alert(requireActivity(), null,
                        getString(R.string.ErrorPrefrenceMessage),
                        getString(R.string.themePrefreceError));
            }
        });
        // takes the user to the accessibility menu
        root.findViewById(R.id.accessibility_button_Settings).setOnClickListener(v ->
                MessageAndTextUtils.alert((Activity) v.getContext(),
                (dialog, which) ->
                requireActivity().startActivityForResult(new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS), 0),
                requireActivity().getString(R.string.acessability_message_to_settings),
                requireActivity().getString(R.string.Accessibility)));
        // deletes the user profile
        root.findViewById(R.id.Delete_Button_Settings).setOnClickListener(
                // Asks the user if they are sure they want to delete their user profile
                v -> MessageAndTextUtils.error_message(requireActivity(),
               (dialog, which) ->{
                   try{
                       CryptographyFunctions.getFromEncryptedSharedPreferences(
                               requireActivity(), true,
                               "setup", "true",
                               "true");
                       UserProfile.delete_profile(requireActivity().getFilesDir().toString());
                       requireActivity().finishAffinity();
                       android.os.Process.killProcess(android.os.Process.myPid());
                       System.exit(1);
                   }catch (GeneralSecurityException | IOException e) {
                            MessageAndTextUtils.alert(requireActivity(), null,
                                    getString(R.string.ErrorPrefrenceMessage),
                                    getString(R.string.themePrefreceError));
                        }},
               null,
               getString(R.string.DeleteProfileDialog),
               getString(R.string.AreYouSure)));
        return root;
    }
}
