package com.example.personalised_assistant.ui_fragments.levels;

import android.app.Activity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TableLayout;
import android.widget.TableRow;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.personalised_assistant.R;
import com.example.personalised_assistant.utils.GlobalVariables;
import com.example.personalised_assistant.utils.MessageAndTextUtils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * This class is a fragment that displays the users level data.
 */
public class LevelsFragment extends Fragment {
    /**
     * This method is called on create and populates a table will all the relivent data
     *
     * @return It will return a inflated view
     */
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_levels, container, false);

        Activity activity = (Activity) root.getContext();
        GlobalVariables globalVariable = (GlobalVariables) activity.getApplicationContext();
        //gets the step count
        long totalStepCount = 0;
        // stores the level count
        int levelCount = 0;
        try {
            JSONObject profile = globalVariable.getUserProfile();
            globalVariable.insertKeyIntoProfile("level_number_of_opens", String.valueOf(profile.getInt("level_number_of_opens") + 1), activity);
            //gets the step count from the user profile
            totalStepCount = profile.getInt("total_steps");
            //gets the level count from the user profile
            levelCount = profile.getInt("level");
        } catch (JSONException e) {
            MessageAndTextUtils.error_alert(activity, null,
                    getString(R.string.ErrorGettingUserData),
                    getString(R.string.LevelError));
        }
        // works out the next level in steps
        long next = (long) ((levelCount * 1000 * 1.25) - totalStepCount);
        //works out the percent to the text level
        double percent = (100 - (next / (levelCount * 1000 * 1.25)));
        //sets the text size
        int textSize = 16;
        // finds the table and puts it into a table object
        TableLayout tableLayout = root.findViewById(R.id.level_table);
        //creates a new table row object
        TableRow table_row = new TableRow(getContext());

        // set the row alignment
        table_row.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        table_row.setGravity(Gravity.CENTER);

        // sets the layout of the table row
        table_row.setLayoutParams(new TableLayout.LayoutParams(
                TableLayout.LayoutParams.MATCH_PARENT,
                TableLayout.LayoutParams.WRAP_CONTENT,
                2.0f));

        table_row.addView(MessageAndTextUtils.makeTextView(activity
                , getString(R.string.current_level_level_fragment),
                true, 0, textSize));
        table_row.addView(MessageAndTextUtils.makeTextView(activity,
                getString(R.string.current_Steps_level_fragment),
                true, 0, textSize));
        table_row.addView(MessageAndTextUtils.makeTextView(activity,
                getString(R.string.Steps_To_Next_Level_level_fragment),
                true, 0, textSize));
        //adds the title row to the table
        tableLayout.addView(table_row);

        // creates a new row object
        TableRow tableRowData = new TableRow(getContext());
        // set the row alignment
        tableRowData.setGravity(Gravity.CENTER);
        tableRowData.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        // sets the layout of the table row
        tableRowData.setLayoutParams(new TableLayout.LayoutParams(
                TableLayout.LayoutParams.MATCH_PARENT,
                TableLayout.LayoutParams.WRAP_CONTENT,
                2.0f));

        // Adds the level count to the table
        tableRowData.addView(MessageAndTextUtils.makeTextView(activity,
                String.valueOf(levelCount),
                true, 0, textSize));

        // Adds the step count to the table
        tableRowData.addView(MessageAndTextUtils.makeTextView(activity,
                String.valueOf(totalStepCount),
                true, 0, textSize));

        // Adds the number of steps to next
        tableRowData.addView(MessageAndTextUtils.makeTextView(activity,
                String.valueOf(next),
                true, 0, textSize));

        // Adds the table row to the table
        tableLayout.addView(tableRowData);
        // Sets the progress bar to the next level
        ProgressBar progress_to_next_level = root.findViewById(R.id.progressBar);
        progress_to_next_level.setMax((int) next + levelCount);
        progress_to_next_level.setProgress((int) totalStepCount);
        // sets a on click event to display to the user their progress to the next level.
        progress_to_next_level.setOnClickListener(v ->
                MessageAndTextUtils.alert(activity, null,
                getString(R.string.percent_towards_next_Level_Fragemnt) +
                        percent + getString(R.string.pecent),
                getString(R.string.next_level_level_fragment)));
        // adds a text view telling the user how far they are from the next level
        tableLayout.addView(MessageAndTextUtils.makeTextView(activity,
                getString(R.string._Percent_towards_next_level_level_fragment) +
                        percent + getString(R.string.pecent),
                true, 0, textSize));
        // Writes the user profile to disk
        globalVariable.writeUserProfileToDisk(activity);
        return root;
    }
}
