package com.example.personalised_assistant.ui_fragments.achivements;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.personalised_assistant.R;
import com.example.personalised_assistant.utils.GlobalVariables;
import com.example.personalised_assistant.utils.MessageAndTextUtils;
import com.example.personalised_assistant.utils.UserProfile;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;
import java.util.Objects;

import static com.example.personalised_assistant.R.drawable.place_holder;
import static com.example.personalised_assistant.R.id;
import static com.example.personalised_assistant.R.layout;

/**
 * This is a fragment used to display all the users achievement in a table.
 */
public class AchievementsFragment extends Fragment {

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        // Inflates the fragment xml
        View root = inflater.inflate(layout.fragment_achivemnets, container, false);
        Activity activity = (Activity) root.getContext();
        // This gets the globalVariables object used to store and get user profile
        GlobalVariables globalVariable = (GlobalVariables) activity.getApplicationContext();
        // sets the text size for this fragment
        final int textSize = 16;
        // gets the user profile and updates the open time count for the fragment
        try {
            JSONObject profile = globalVariable.getUserProfile();
            globalVariable.insertKeyIntoProfile("achievements_number_of_opens",
                    String.valueOf(profile.getInt("achievements_number_of_opens") + 1)
                    ,activity);
        } catch (JSONException e) {
            // Displays a error message to the user
            MessageAndTextUtils.error_alert(activity, null ,
                    getString(R.string.ErrorUpdatingUserProfile),
                    getString(R.string.AchivementsFragment));
        }

        // Loads the achievements from the user profile
        JSONObject achievement = null;
        try {
            // Gets the achievements object from the user profile
            achievement = new JSONObject(
                    Objects.requireNonNull(UserProfile.getAchievements(globalVariable))
            );
        } catch (JSONException e) {
            // Displays a error message to the user
            MessageAndTextUtils.error_alert(activity, null ,
                    getString(R.string.errorGettingAchivemnts)+ e.getMessage(),
                    getString(R.string.AchivementsFragment));
        }
        // adds achievements to a table that is displayed to the user
        TableLayout tableLayout = root.findViewById(id.table_main);
        TableRow tableRow = new TableRow(getContext());
        TextView textViewPositions = new TextView(getContext());
        TextView text_view_name = new TextView(getContext());

        textViewPositions.setText(R.string.AchivemnetTableName);
        text_view_name.setText(R.string.IconAchivementTable);

        textViewPositions.setTextSize(textSize);
        text_view_name.setTextSize(textSize);

        tableRow.addView(textViewPositions);
        tableRow.addView(text_view_name);

        tableRow.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        tableLayout.addView(tableRow);
        try {
            Iterator<String> key = Objects.requireNonNull(achievement).keys();
            key.next();
            while (key.hasNext()) {
                TableRow table_row_player = new TableRow(getContext());
                table_row_player.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                TextView text_view_user_possition = new TextView(getContext());
                ImageView text_view_user_name = new ImageView(getContext());
                text_view_user_possition.setTextSize(textSize);
                // Sets the current achievement name
                text_view_user_possition.setText(
                        String.format("%s%s%s",
                                getString(R.string.DoubleSpace),
                                key.next(),
                                activity.getString(R.string.space)
                        )
                );
                // Adds a placeholder image for achievements
                text_view_user_name.setImageResource(place_holder);
                // adds the rows and text ot the table
                table_row_player.addView(text_view_user_possition);
                table_row_player.addView(text_view_user_name);
                tableLayout.addView(table_row_player);
            }

        } catch (Exception e) {
            // displays a error to the user
            MessageAndTextUtils.error_alert(activity, null ,
                    getString(R.string.ErrorAchivementJson),
                    getString(R.string.AchivementsFragment));
        }
        // writes the profile to disk
        globalVariable.writeUserProfileToDisk(activity);
        return root;
    }
}
