package com.example.personalised_assistant.ui_fragments.notes;

import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.personalised_assistant.R;
import com.example.personalised_assistant.apiKeys;
import com.example.personalised_assistant.utils.FileUtilities;
import com.example.personalised_assistant.utils.GamerTypeAndPersonality;
import com.example.personalised_assistant.utils.GlobalVariables;
import com.example.personalised_assistant.utils.MessageAndTextUtils;
import com.example.personalised_assistant.utils.SentimentCalculator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.TimeZone;

import static com.example.personalised_assistant.utils.GamerTypeAndPersonality.initStereotype;
import static java.util.Calendar.DAY_OF_WEEK;

/**
 * This class is a fragment that gets and analyses the users note from a imputed text-box.
 */
public class NewNoteFragment extends Fragment {
    /**
     * This is the url for googles sentiment analysis
     */
    private final String GOOGLE_SENTIMENT_URL = "https://language.googleapis.com/v1/documents:analyzeSentiment?key=" + apiKeys.GOOGLE_API_KEY;

    /**
     * This is the data sent to google sentiment analysis in a post request
     */
    private final JSONObject googleSentimentPostJson = new JSONObject();
    /**
     * This is the data sent to ibm tone analysis in a post request
     */
    private final JSONObject ibmToneAnalysisPostJson = new JSONObject();
    /**
     * This is the global variables object
     */
    private GlobalVariables globalVariable;

    /**
     * The note text box the user enters their new note in.
     * This is used to send the data over a secure connection to the server for analysis
     */
    private TextView noteTextBox;
    /**
     * A dialog box for the onclick event giving the user the option to change gamer type based upon notes
     */
    private final DialogInterface.OnClickListener updateStereotypeListener = (dialog, which) -> {
        try {
            //Gets the personality object
            JSONObject temp = globalVariable.getUserProfile().getJSONObject("personality");

            // resets the users stereotype
            JSONObject updated = initStereotype(temp.getString("Gamer_Type"));

            // inserts the new personality.
            globalVariable.insertKeyIntoProfile("personality",
                    updated.toString(), requireActivity());
        } catch (JSONException e) {
            // gives the user a error
            MessageAndTextUtils.error_alert(requireActivity(), null,
                    getString(R.string.ErrorUpdateGamerType) + e,
                    getString(R.string.UpdatingGamerTypeErrorTitle));
        }
    };

    /**
     * A empty constructor as required according to the android documents
     */
    public NewNoteFragment() {
    }

    /**
     * This method is called on create and sets up the fragment. This also sets up a onclick event c
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // inflates the instance
        View root = inflater.inflate(R.layout.fragment_new_note, container, false);
        // makes the text box not push up with keyboard
        requireActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
        // Creates a onclick listener for the save button
        root.findViewById(R.id.save_button).setOnClickListener(v -> {
            noteTextBox = root.findViewById(R.id.editText);
            globalVariable = (GlobalVariables) requireActivity().getApplicationContext();

            if ((noteTextBox.getText().toString()).split(" ").length < 15) {
                MessageAndTextUtils.error_alert(requireActivity(), null,
                        getString(R.string.EntryTooShort),
                        getString(R.string.NewNoteEntryTooShortErrorTitle));
                return;
            }
            try {
                // creates the json object to be sent
                ibmToneAnalysisPostJson.put("text", noteTextBox.getText());
                JSONObject document = new JSONObject();
                document.put("type", "PLAIN_TEXT");
                document.put("content", noteTextBox.getText());
                googleSentimentPostJson.put("document", document);
                googleSentimentPostJson.put("encodingType", "UTF8");
            } catch (Exception e) {
                MessageAndTextUtils.error_alert(requireActivity(), null,
                        getString(R.string.ErrorCreatingNewNote),
                        getString(R.string.NewNote));
                return;
            }
            get_google_sentiment();

        });
        return root;
    }

    /**
     * A method to get and anlise the result from google sentiment analysis
     */
    private void get_google_sentiment() {
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, GOOGLE_SENTIMENT_URL, googleSentimentPostJson,
                response -> {
                    Log.d("TAG", response.toString());

                    JSONObject temp = new JSONObject();
                    try {
                        JSONObject response_sentiment_document = response.getJSONObject("documentSentiment");
                        String note = noteTextBox.getText().toString();
                        double Sentiment = response_sentiment_document.getDouble("score"), Magnitude = response_sentiment_document.getDouble("magnitude");

                        temp.put("note", note);
                        temp.put("sentiment", Sentiment);
                        temp.put("magnitude", Magnitude);

                        if (response_sentiment_document.getDouble("magnitude") > SentimentCalculator.MAGNITUDE_MINIMUM) {
                            globalVariable.insertKeyIntoProfile("note_sentiment", String.valueOf(SentimentCalculator.normaliseSentiment(Magnitude, Sentiment, note)), requireActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    get_IBM_SENTIMENT(temp);
                },
                error -> {
                    // displays a error message to the user
                    MessageAndTextUtils.alert(requireActivity(), null,
                            getString(R.string.SentimentErrorNewFragment) + error.getMessage(),
                            getString(R.string.ErrorGettingSentimentNewNote));
                }
        ) {

        };
        Volley.newRequestQueue(requireActivity()).add(postRequest);
    }

    /**
     * This method gets tone analysis from IBM and adds it to the user profile
     *
     * @param temp This is the current note json object that will be written to a file
     */
    private void get_IBM_SENTIMENT(JSONObject temp) {
        JsonObjectRequest IBM_SENTIMENT = new JsonObjectRequest(Request.Method.POST, apiKeys.IBM_SENTIMENT_URL, ibmToneAnalysisPostJson,
                response -> {
                    try {
                        JSONArray emotion_tones = response.getJSONObject("document_tone").getJSONArray("tone_categories").getJSONObject(0).getJSONArray("tones");
                        JSONArray personality_tones = response.getJSONObject("document_tone").getJSONArray("tone_categories").getJSONObject(2).getJSONArray("tones");
                        String[] key = {"anger", "disgust", "fear", "joy", "sadness"};


                        double highest = 0.0;
                        String current_mood = "";
                        //itter though the keys and checks the users current mood.
                        for (int i = 0; i < 5; i++) {
                            String current_score = emotion_tones.getJSONObject(i).getString("score");
                            temp.put(key[i], current_score);
                            if (highest < Double.parseDouble(current_score)) {
                                highest = Double.parseDouble(current_score);
                                current_mood = key[i];
                            }

                        }
                        // Very basic Adaptation only selects highest and then preforms search on bit to get adaptive pic
                        globalVariable.insertKeyIntoProfile("current_mood", current_mood, requireActivity());
                        globalVariable.insertKeyIntoProfile("current_mood_value", String.valueOf(highest), requireActivity());
                        // Updates the users personally values
                        GamerTypeAndPersonality.updatePersonalityValues(globalVariable, personality_tones, requireActivity(), updateStereotypeListener);

                    } catch (JSONException e) {
                        // displays the error to the user
                        MessageAndTextUtils.error_alert(requireActivity(), null,
                                getString(R.string.ErrorUpdateGamerType) + e,
                                getString(R.string.ErrorUpdateBigFive));
                    }

                    Calendar calender = Calendar.getInstance(TimeZone.getDefault());

                    String file_path = requireActivity().getFilesDir() + "/Notes/" + calender.get(Calendar.YEAR) +
                            "/" + new DateFormatSymbols().getMonths()[calender.get(Calendar.MONTH)] + "/" +
                            new DateFormatSymbols().getWeekdays()[calender.get(DAY_OF_WEEK)] +
                            "-" + calender.get(Calendar.DAY_OF_MONTH) + "/" + "Entry-" + calender.get(Calendar.HOUR_OF_DAY)
                            + "-" + calender.get(Calendar.MINUTE) + "-" + calender.get(Calendar.MILLISECOND);

                    File file_exists = new File(file_path);
                    //Checks if the file exists if not then make a the folder to make sure it exists
                    if (!file_exists.exists()) {
                        //checks if the folder exits or not
                        if (!Objects.requireNonNull(file_exists.getParentFile()).exists()) {
                            // Gets the result of trying to make the folder
                            boolean folderMade = file_exists.getParentFile().mkdirs();
                            if (!folderMade) {
                                // displays a error message to the user
                                MessageAndTextUtils.alert(requireActivity(), null,
                                        getString(R.string.SentimentErrorNewFragment) + " Can not create folder",
                                        getString(R.string.ErrorGettingSentimentNewNote));
                                return;
                            }
                        }
                    } else {
                        // displays a error message to the user
                        MessageAndTextUtils.alert(requireActivity(), null,
                                getString(R.string.SentimentErrorNewFragment) + " File Already Made",
                                getString(R.string.ErrorGettingSentimentNewNote));
                        return;
                    }

                    //Writes the note to encrypted or not disk depending on if its a emulator or not
                    if (!globalVariable.isEmulator()) {
                        FileUtilities.write_json_object(temp, file_path, globalVariable.getKey());
                    } else {
                        FileUtilities.write_demo(temp, file_path);
                    }
                    //Wrote the user profile to disk
                    globalVariable.writeUserProfileToDisk(requireActivity());
                    //clears text box
                    noteTextBox.setText("");
                    // Alerts the user to the note being saved
                    MessageAndTextUtils.alert(requireActivity(), null,
                            getString(R.string.NoteSaved),
                            getString(R.string.NoteSavedTitle));

                },
                error -> {
                    // displays a error message to the user
                    MessageAndTextUtils.alert(requireActivity(), null,
                            getString(R.string.SentimentErrorNewFragment) + error.getMessage(),
                            getString(R.string.ErrorGettingSentimentNewNote));
                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                //Creates the required auth headers to use IBM api
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Basic " + Base64.encodeToString(("apikey:" + apiKeys.IBM_API_KEY).getBytes(), Base64.NO_WRAP));
                return headers;
            }
        };

        Volley.newRequestQueue(requireActivity()).add(IBM_SENTIMENT);
    }
}
