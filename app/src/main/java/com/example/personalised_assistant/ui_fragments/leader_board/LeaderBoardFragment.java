package com.example.personalised_assistant.ui_fragments.leader_board;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.personalised_assistant.R;
import com.example.personalised_assistant.utils.FileUtilities;
import com.example.personalised_assistant.utils.GlobalVariables;
import com.example.personalised_assistant.utils.MessageAndTextUtils;
import com.example.personalised_assistant.utils.UserProfile;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * This class is a fragment and will display the leader board for the user,
 */
public class LeaderBoardFragment extends Fragment {
    /**
     * Sets the text size for the text view
     */
    private final static int TEXT_SIZE = 16;

    /**
     * This method is called on create and will display the leader board will all the users friends on it
     * @return This will return the inflated fragment
     */
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_leaderbord, container, false);
        // gets the current activity
        Activity activity = (Activity) root.getContext();
        // Creates the globalVariables object for updating and getting user profile
        GlobalVariables globalVariable = (GlobalVariables) activity.getApplicationContext();
        // the applications file directory used to get the lead board file
        String directory = activity.getFilesDir().toString();

        try {
            JSONObject profile = globalVariable.getUserProfile();
            // Updates the leader board
            UserProfile.createLeaderBoard(profile.getInt("level"), profile.getString("name"), activity.getFilesDir().toString(), globalVariable);
            // Increase the leader board open count
            globalVariable.insertKeyIntoProfile("leaderbord_number_of_opens", String.valueOf(profile.getInt("leaderbord_number_of_opens") + 1), activity);
        } catch (JSONException e) {
            // displays the user a error message
            MessageAndTextUtils.error_alert(activity, null,
                    getString(R.string.ErrorGettingUserData),
                    getString(R.string.LeaderBoardFragmentErrorTItle));
        }

        // loads the leader board
        JSONArray leaderBoardJSONArray;
        if (globalVariable.isEmulator()) {
            leaderBoardJSONArray = FileUtilities.load_demo_array(directory + "/leader_board.json");
        } else {
            leaderBoardJSONArray = FileUtilities.load_json_array(directory + "/leader_board.json", globalVariable.getKey());
        }

        // Adds the headings to the tables
        TableLayout table_layout = root.findViewById(R.id.table_main);
        TableRow table_row = new TableRow(getContext());

        table_row.addView(
                MessageAndTextUtils.makeTextView(activity,
                        getString(R.string.possition),
                        true, 0, TEXT_SIZE));

        table_row.addView(
                MessageAndTextUtils.makeTextView(activity,
                        getString(R.string.LeaderBoardNameHeading),
                        true, 0, TEXT_SIZE));

        table_row.addView(
                MessageAndTextUtils.makeTextView(activity,
                        getString(R.string.levelHeading),
                        true, 0, TEXT_SIZE));

        table_row.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        table_layout.addView(table_row);
        assert leaderBoardJSONArray != null;
        try {
            // Itter though the leader board adding each player
            for (int i = leaderBoardJSONArray.length()-1,
                 currentPosition = 1; i >= 0; i--) {

                // creates new row
                TableRow table_row_player = new TableRow(getContext());
                //sets alignment
                table_row_player.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                // Gets the player from the leader board array
                JSONObject currentUserPositionJSONObject = new JSONObject(leaderBoardJSONArray.getString(i));

                // Adds the current position level to the table
                table_row_player.addView(
                        MessageAndTextUtils.makeTextView(activity,
                                getString(R.string.Spacer) + currentPosition++ + getString(R.string.Spacer),
                                true, 0, TEXT_SIZE));
                // Adds the current plays name to the table
                table_row_player.addView(
                        MessageAndTextUtils.makeTextView(activity,
                                getString(R.string.Spacer) + currentUserPositionJSONObject.get("name").toString() + getString(R.string.Spacer),
                                true, 0, TEXT_SIZE));
                // Adds the current plays level to the table
                table_row_player.addView(
                        MessageAndTextUtils.makeTextView(activity,
                                getString(R.string.Spacer) + currentUserPositionJSONObject.getString("level") + getString(R.string.Spacer),
                                true, 0, TEXT_SIZE));
                // Adds the row to the table
                table_layout.addView(table_row_player);
            }
        } catch (Exception e) {
            // Gives the user a error message
            MessageAndTextUtils.error_alert(activity,null,
                    getString(R.string.ErrorWhileMakingLeaderBoard)+e.getMessage(),
                    getString(R.string.leaderboard));
        }
        // Writes the user profile to disk
        globalVariable.writeUserProfileToDisk(activity);
        return root;
    }
}
