package com.example.personalised_assistant.ui_fragments.profile;

import android.app.Activity;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.personalised_assistant.R;
import com.example.personalised_assistant.snapchat_utils.SnapchatUtilities;
import com.example.personalised_assistant.utils.GlobalVariables;
import com.example.personalised_assistant.utils.MessageAndTextUtils;
import com.snapchat.kit.sdk.SnapLogin;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * This fragment displays the users profile information
 */
public class ProfileFragment extends Fragment {
    /**
     * This method overrides the fragment on create and is called when the fragment is viewed
     * @return This method returns the view.
     */
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
    // Inflates the fragment
        View root = inflater.inflate(R.layout.fragment_profile, container, false);
        // Reduces code lenght by creating a pointer to the activity
        Activity activity = requireActivity();
        // Creates the fragments globalVariables object
        GlobalVariables globalVariable = (GlobalVariables) activity.getApplicationContext();
        // Creates a temp copy of the users users profile
        JSONObject profile = globalVariable.getUserProfile();
        // Checks if the user is logged into snapchat
        boolean loggedIn = SnapLogin.isUserLoggedIn(getContext());
        // Loads the avatar of the user from bitemoji
        ((ImageView) root.findViewById(R.id.profile_image)).setImageBitmap(BitmapFactory.decodeFile(activity.getFilesDir().toString() + "/avatar.png"));
        // Sets a onclick listener for the connect/disconnect snapchat button.
        root.findViewById(R.id.button).setOnClickListener(v -> {
            if (loggedIn){
                SnapchatUtilities snapchatUtilities = new SnapchatUtilities(activity);
                snapchatUtilities.logout();
                ((TextView) root.findViewById(R.id.snap_status_value)).setText(R.string.disconnected);
                ((Button)root.findViewById(R.id.button)).setText(R.string.connect);
            }else{
                SnapchatUtilities snapchatUtilities = new SnapchatUtilities(activity);
                snapchatUtilities.login();
                ((TextView) root.findViewById(R.id.snap_status_value)).setText(R.string.connected);
                ((Button)root.findViewById(R.id.button)).setText(R.string.disconnect);
            }
        });
        // Checks if the user is connected to snapchat and displays the correct login/ disconnect status
        if (loggedIn) {
            ((TextView) root.findViewById(R.id.snap_status_value)).setText(R.string.connected);
            ((Button)root.findViewById(R.id.button)).setText(R.string.disconnect);
        } else {
            ((TextView) root.findViewById(R.id.snap_status_value)).setText(R.string.disconnected);
            ((Button)root.findViewById(R.id.button)).setText(R.string.connect);
        }

        try {
            // Sets the text to the users leader board position
            ((TextView) root.findViewById(R.id.leaderbord_position_value))
                    .setText(String.valueOf(profile.getInt("leader_bord_position")));
            // Sets the text to the users current level
            ((TextView) root.findViewById(R.id.levels_value))
                    .setText(String.valueOf(profile.getInt("level")));
            // Sets the text to display the current note sentiment as got from the last user note. This is made from google sentiment analysis
            ((TextView) root.findViewById(R.id.note_sentimate))
                    .setText(String.valueOf(profile.getInt("note_sentiment")));
            // Sets the text to display the total number of steps
            ((TextView) root.findViewById(R.id.number_of_steps_txt))
                    .setText(String.valueOf(profile.getInt("total_steps")));
            // Sets the text to display the number of hours sleep that are 8 hours or longer in a row.
            ((TextView) root.findViewById(R.id.sleep_streek_value))
                    .setText(String.valueOf(profile.getInt("sleep_streak")));
            // Sets the text to display number of achievements
            ((TextView) root.findViewById(R.id.achivement_number_txt))
                    .setText(String.valueOf(profile.getJSONObject("achievements").get("number_of_achievements")));
        } catch (JSONException e) {
            // In case of JSONException display error to user.
            MessageAndTextUtils.error_alert(activity,null,
                    getString(R.string.JsonErrorProfileFragment)+e.getMessage(),
                    getString(R.string.ErrorProfile));
        }

        return root;
    }
}
