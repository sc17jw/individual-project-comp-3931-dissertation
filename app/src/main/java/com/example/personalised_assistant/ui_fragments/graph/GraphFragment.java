package com.example.personalised_assistant.ui_fragments.graph;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.util.Pair;
import androidx.fragment.app.Fragment;

import com.example.personalised_assistant.R;
import com.example.personalised_assistant.samsung_health.RecommendedIntake;
import com.example.personalised_assistant.utils.CryptographyFunctions;
import com.example.personalised_assistant.utils.GlobalVariables;
import com.example.personalised_assistant.utils.GraphMaker;
import com.example.personalised_assistant.utils.MessageAndTextUtils;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.google.android.material.datepicker.CalendarConstraints;
import com.google.android.material.datepicker.MaterialDatePicker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import static java.lang.Math.floor;

/**
 * This class is a fragment that displays key information to the user.
 */
public class GraphFragment extends Fragment {
    /**
     * Sets a constant text size. This field is used for flexibility and ease of changing
     */
    private static final int TEXT_SIZE = 30;
    /**
     * This is the current key id used as a index for the arrays
     */
    private int key_id = 0;
    /**
     * This is the dataset for getting the
     */
    private final String[] dataKeys = {"total_steps", "level", "sleep_streak", "max_sleep_streak", "total_cals_consumed",
            "total_cals_burned", "nutrition", "personality", "exercises", "hart_rate", "sleep_data"};
    /**
     * This is the keys the user can select.
     */
    private final String[] selectedKeys = {"Total Steps", "Level", "Sleep Streak", "Longest\nSleep Streak",
            "Total Calories\nConsumed", "Total Calories\nBurned", "Nutrition", "Personality\nType",
            "Exercise\nData", "Hart-Rate\nData", "Sleep\nData",};
    /**
     * This is the data keys used to make line graph. Each key corresponds to a value in a JSON object in a JSON array in the user profile
     */
    private final String[][] graphDataKeys = {
            {"exercise", "calorie", "duration", "start_time", "end_time"},
            {"HEART_RATE", "MAX", "MIN", "start_time", "end_time"},
            {"sleep_time", "start_time", "end_Time"}
    };
    /**
     * This is a key list to reduce code size of all the personality types used to make the pie chart.
     */
    private final String[] Personality_types = new String[]{"Openness", "Conscientiousness", "Extraversion", "Agreeableness", "Neuroticism"};
    /**
     * This is  a array of colours that is used to make the graph.
     */
    private int[] graphColors;
    /**
     * This is the inflated view.
     */
    private View root;

    /**
     * The public constructor for fragments has to be blank as stated in android docs
     */
    public GraphFragment() {
    }

    /**
     * This method is called when the fragment is created and will display the user selected data item.
     *
     * @return This method will return a inflated view.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_graph_fragment, container, false);
        // sets the screen size
        int[] screen_size = GraphMaker.get_screen_size((Activity) root.getContext());
        // sets the graph colors.
        graphColors = new int[]{
                getResources().getColor(R.color.colorPrimary, null),
                getResources().getColor(R.color.colorAccent, null),
                getResources().getColor(R.color.design_default_color_primary, null)
        };
        // allows for shorter code.
        Activity activity = (Activity) root.getContext();
        // gets the globalVariables object
        GlobalVariables globalVariable = (GlobalVariables) activity.getApplicationContext();
        // loads the data from the fragment args that have been pasted
        assert getArguments() != null;
        String graph = GraphFragmentArgs.fromBundle(getArguments()).getGraphToMake();
        LinearLayout layout = root.findViewById(R.id.info_chart_layout);
        // Creates the text view for the message or graph data info.
        TextView message = new TextView(root.getContext());
        message.setTypeface(null, Typeface.BOLD);
        message.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
        // gets current theme false = dark, true = light
        boolean theme = false;
        try {
            // Gets theme from encrypted preferences
            theme = Boolean.parseBoolean(
                    CryptographyFunctions.getFromEncryptedSharedPreferences(
                            requireContext(), false,
                            "light_theme", "",
                            "false"));

        } catch (GeneralSecurityException | IOException e) {
            // Gives the user the error to report.
            MessageAndTextUtils.error_alert((Activity) root.getContext(), null,
                    "Error while creating graph: " + e.toString(),
                    " Graph Fragment");
            e.printStackTrace();
        }
        // Gets the chosen item key
        for (int i = 0; i < selectedKeys.length; i++) {
            if (selectedKeys[i].equals(graph)) {
                key_id = i;
                break;
            }
        }
        // update values depending if selected for heart rate and sleep and exercise
        if (!globalVariable.isEmulator()) {
            if (key_id == 8) {
                globalVariable.getSamsungHealth().updateExerciseData();
            } else if (key_id == 9) {
                globalVariable.getSamsungHealth().updateHeartData();
            } else if (key_id == 10) {
                globalVariable.getSamsungHealth().updateSleep();
            }
        }
        try {
            if (key_id > 7) {
                Calendar temp_calender = Calendar.getInstance(TimeZone.getDefault());
                JSONArray start_tine = new JSONArray(globalVariable.getUserProfile().getString(dataKeys[key_id]));

                long start = start_tine.getJSONObject(0).getLong(graphDataKeys[key_id - 8][graphDataKeys[key_id - 8].length - 2]),
                        end_time = temp_calender.getTimeInMillis() + TimeZone.getDefault().getOffset(temp_calender.getTimeInMillis());

                LineChart lineChart = new LineChart(activity);
                boolean finalTheme = theme;
                root.findViewById(R.id.slect_date_range_button).setOnClickListener(v -> {
                    // Creates a new date picker allowing the user to select a date range for the selected data.
                    MaterialDatePicker.Builder<androidx.core.util.Pair<Long, Long>> builder = MaterialDatePicker.Builder.dateRangePicker();
                    // Sets the title of the date picker
                    builder.setTitleText(R.string.SelectATimeRange);
                    // Adds the data constraints
                    CalendarConstraints.Builder dateConstraintsBuilder =
                            new CalendarConstraints.Builder()
                                    .setStart(start)
                                    .setEnd(end_time);
                    // Adds the constraints to the date picker
                    builder.setCalendarConstraints(dateConstraintsBuilder.build());
                    // Creates the date picker
                    MaterialDatePicker<Pair<Long, Long>> datePicker = builder.build();
                    // Sets a onclick listener for the picker
                    datePicker.addOnPositiveButtonClickListener(dateSelected -> {
                        // Clears the graph
                        lineChart.clear();
                        // Makes sure their is no null values
                        assert dateSelected.first != null;
                        assert dateSelected.second != null;
                        // Generates and adds the data to the graph
                        get_data_for_graph(dateSelected.first, dateSelected.second, start_tine, lineChart, key_id - 8, finalTheme);
                        lineChart.invalidate();
                    });
                    // Ignore no section
                    datePicker.addOnNegativeButtonClickListener(selection -> {
                    });
                    // Displays the date picker
                    datePicker.show(getParentFragmentManager(), datePicker.toString());
                });
                // Sets the chart size to a appropriate size
                lineChart.setMinimumWidth(screen_size[0]);
                lineChart.setMinimumHeight(((int) floor(screen_size[1] * 0.7)));

                lineChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
                    // Change the text to the value selected at the bottom of the graph
                    @Override
                    public void onValueSelected(Entry e, Highlight h) {
                        String selected_name = lineChart.getData().getDataSetByIndex(h.getDataSetIndex()).getLabel();
                        String data_x_axis_lable = lineChart.getXAxis().getValueFormatter().getAxisLabel(h.getX(), lineChart.getXAxis());
                        String text = getString(R.string.data_point) + selected_name + getString(R.string.valueis) + e.getY() + getString(R.string.at) + data_x_axis_lable + getString(R.string.double_newline);
                        message.setText(text);
                    }

                    // Clear the displayed text if no value is selected
                    @Override
                    public void onNothingSelected() {
                        message.setText("");
                    }
                });
                // Adds the graph to the layout
                layout.addView(lineChart);
                // Adds the message to the underneath of the graph
                layout.addView(message);
                // Sets the date to the graph
                lineChart.invalidate();
            } else {
                // If the user selects a non chart item it will remove the chart date selector
                root.findViewById(R.id.slect_date_range_button).setVisibility(View.GONE);
                // Changes the color based upon the theme
                int color_text;
                if (theme) {
                    color_text = Color.BLACK;
                } else {
                    color_text = Color.WHITE;
                }
                // Displays the appropiate
                switch (key_id) {
                    case 0:
                        // check if emulator if not updates information
                        if (!globalVariable.isEmulator()) {
                            globalVariable.getSamsungHealth().updateStepCount();
                        }
                        layout.addView(MessageAndTextUtils.makeTextView(requireContext(),
                                globalVariable.getUserProfile().get(dataKeys[0]) + getString(R.string.steps_graph_function),
                                true, color_text, TEXT_SIZE));
                        break;
                    case 1:
                        // check if emulator if not updates information
                        if (!globalVariable.isEmulator()) {
                            globalVariable.getSamsungHealth().updateStepCount();
                        }
                        layout.addView(MessageAndTextUtils.makeTextView(requireContext(),
                                globalVariable.getUserProfile().get(dataKeys[1]) + getString(R.string.level_graph),
                                true, color_text, TEXT_SIZE));
                        break;

                    case 2:
                        // check if emulator if not updates information
                        if (!globalVariable.isEmulator()) {
                            globalVariable.getSamsungHealth().updateSleep();
                        }
                        layout.addView(MessageAndTextUtils.makeTextView(requireContext(),
                                globalVariable.getUserProfile().get(dataKeys[2]) + getString(R.string.sleep_streek_graph),
                                true, color_text, TEXT_SIZE));
                        break;

                    case 3:
                        // check if emulator if not updates information
                        if (!globalVariable.isEmulator()) {
                            globalVariable.getSamsungHealth().updateSleep();
                        }
                        layout.addView(MessageAndTextUtils.makeTextView(requireContext(),
                                globalVariable.getUserProfile().get(dataKeys[3]) + getString(R.string.longest_sleep_streek),
                                true, color_text, TEXT_SIZE));
                        break;

                    case 4:
                        // check if emulator if not updates information
                        if (!globalVariable.isEmulator()) {
                            globalVariable.getSamsungHealth().updateCaloriesConsumed();
                        }
                        layout.addView(MessageAndTextUtils.makeTextView(requireContext(),
                                globalVariable.getUserProfile().get(dataKeys[4]) + getString(R.string.cals_consumed_graph),
                                true, color_text, TEXT_SIZE));
                        break;

                    case 5:
                        // check if emulator if not updates information
                        if (!globalVariable.isEmulator()) {
                            globalVariable.getSamsungHealth().updateCaloriesBurned();
                        }
                        layout.addView(MessageAndTextUtils.makeTextView(requireContext(),
                                globalVariable.getUserProfile().get(dataKeys[5]) + getString(R.string.cals_burned_graph),
                                true, color_text, TEXT_SIZE));
                        break;
                    case 6:
                        // check if emulator if not updates information
                        if (!globalVariable.isEmulator()) {
                            globalVariable.getSamsungHealth().updateNutritionData();
                        }
                        // Displays the nutrition information
                        JSONObject nutrition = new JSONObject(globalVariable.getUserProfile().getString(dataKeys[6]));
                        for (int i = 0; i < RecommendedIntake.key.length; i++) {
                            layout.addView(MessageAndTextUtils.makeTextView(requireContext(),
                                    RecommendedIntake.key[i] + ": " +
                                            nutrition.getString(RecommendedIntake.key[i])
                                            + "\n", false, 0, 20));
                        }
                        break;
                    case 7:
                        //gets the personality data from the user profile
                        JSONObject Personality = globalVariable.getUserProfile().getJSONObject(dataKeys[7]);
                        //Creates a new pie chart object
                        PieChart pieChart = new PieChart(activity);
                        // Creates the data keys used to make the chart
                        double[] personality_data = {Personality.getDouble("Openness"),
                                Personality.getDouble("Conscientiousness"),
                                Personality.getDouble("Extraversion"),
                                Personality.getDouble("Agreeableness"),
                                Personality.getDouble("Neuroticism")
                        };
                        //Creates a the pie chart
                        GraphMaker.get_pie_chart(pieChart, color_text, Personality_types,
                                getString(R.string.personalityType), personality_data);

                        // Sets the appropriate chart size
                        pieChart.setMinimumWidth(screen_size[0]);
                        pieChart.setMinimumHeight(screen_size[1] / 2);
                        // Creates A text view for displaying personality type data
                        TextView Message = new TextView(requireContext());
                        Message.setTypeface(null, Typeface.BOLD);
                        Message.setTextSize(TypedValue.COMPLEX_UNIT_SP, TEXT_SIZE);
                        // creates a on click listener for each item in the pie chart
                        pieChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
                            @Override
                            public void onValueSelected(Entry e, Highlight h) {
                                String value = ((PieEntry) e).getLabel();
                                // sets the text for each personality selected explains a bit about it
                                if (value.equals(Personality_types[0])) {
                                    String text = getString(R.string.personality_type_openess_1) + ((PieEntry) e).getValue() +
                                            getString(R.string.space) + getString(R.string.extract_openess);
                                    Message.setText(text);
                                } else if (value.equals(Personality_types[1])) {
                                    String text = getString(R.string.conscientoiusness);
                                    Message.setText(text);
                                } else if (value.equals(Personality_types[2])) {
                                    String text = getString(R.string.extraverts);
                                    Message.setText(text);
                                } else if (value.equals(Personality_types[3])) {
                                    String text = getString(R.string.agreeableness);
                                    Message.setText(text);
                                } else if (value.equals(Personality_types[4])) {
                                    String text = getString(R.string.neuritiasm);
                                    Message.setText(text);
                                } else {
                                    Message.setText("");
                                }
                            }

                            @Override
                            public void onNothingSelected() {
                            }
                        });
                        // Adds the pie chart to the layout
                        layout.addView(pieChart);
                        // Adds the selected item to the the layout
                        layout.addView(Message);
                        break;
                }
            }
        } catch (JSONException e) {
            MessageAndTextUtils.error_alert((Activity) root.getContext(), null,
                    getString(R.string.ErrorCreatingGraphMessage) + e.toString(),
                    getString(R.string.GraphFragmentErrorTitle));
            e.printStackTrace();
        }
        return root;
    }

    /**
     * This method will create the data set for the line graph it is set to display as dictated by the keyId
     *
     * @param start_time This is the start time for the data set search
     * @param end_time   This is the end time for the data set search
     * @param array      This is a json array of the data to be processed
     * @param lineChart  This is the line chart object for the data to populate
     * @param keyID      This is the key id used for selecting what data is being added to the graph
     * @param theme      This is the users current theme setting to adapt to,
     */
    private void get_data_for_graph(long start_time, long end_time, JSONArray array, LineChart lineChart, int keyID, boolean theme) {
        try {
            ArrayList<LineDataSet> line_data = new ArrayList<>();
            String[] date = new String[array.length()];
            lineChart.setHapticFeedbackEnabled(true);
            IndexAxisValueFormatter formatter = new IndexAxisValueFormatter();
            lineChart.setScaleMinima(10, 1);
            for (int keyIndex = 0; keyIndex < graphDataKeys[keyID].length - 2; keyIndex++) {
                ArrayList<Entry> graphEntryArrayList = new ArrayList<>();
                if (keyID == 0 && keyIndex == 0) {
                    continue;
                }
                for (int i = 0; i < array.length(); i++) {
                    // Gets the time of the data for use of comparison of the current item
                    long time = array.getJSONObject(i).getLong(graphDataKeys[keyID][graphDataKeys[keyID].length - 2]);
                    // Removes outlires in data to increase visablity of data. A value of 0 is either a error while recording the data or if the watch is off
                    // A value of 0 could also be the watch charging Also checks if the start time is less thant the user specified. And checks if its over exiting the loop
                    // This allows for a reduction in json searching. This also unlike quries does not give error if the time range is out of the databases entry
                    if (array.getJSONObject(i).getLong(graphDataKeys[keyID][keyIndex]) == 0) {
                        continue;
                    } else if (time < start_time) {
                        continue;
                    } else if (time > end_time) {
                        break;
                    }
                    // Creates the data object for the x axis time and adds the current value to the arrau
                    Date data_date = new Date(time);
                    date[i] = (data_date.toString());
                    if (keyID == 0 && keyIndex == 2) {
                        date[i] = date[i] + " " + array.getJSONObject(i).getString(graphDataKeys[0][0]);
                        long currentDataDate = (((array.getJSONObject(i).getLong(graphDataKeys[keyID][keyIndex])) / 1000L) / 60);
                        graphEntryArrayList.add(new Entry(i, (currentDataDate)));
                    } else {
                        graphEntryArrayList.add(new Entry(i, array.getJSONObject(i).getInt(graphDataKeys[keyID][keyIndex])));
                    }
                }
                if (graphEntryArrayList.size() <= 0 || date.length <= 0) {
                    MessageAndTextUtils.error_alert((Activity) root.getContext(), null,
                            getString(R.string.ErrorWhileCreatingGraph),
                            getString(R.string.graph_fragment_error));
                    return;
                }
                // Creates a new data set object with the entry list and the key as a label
                LineDataSet dataSet = new LineDataSet(graphEntryArrayList, graphDataKeys[keyID][keyIndex]);
                // sets all the data set graph information
                dataSet.setAxisDependency(YAxis.AxisDependency.RIGHT);
                dataSet.setColor(graphColors[keyIndex]);
                dataSet.setFillColor(graphColors[keyIndex]);
                dataSet.setDrawFilled(true);
                dataSet.setLineWidth(2f);
                dataSet.setDrawCircles(true);
                dataSet.setHighLightColor(graphColors[keyIndex]);
                line_data.add(dataSet);
            }

            // Sets the formatter for the dates to the dates
            formatter.setValues(date);
            // Checks theme and adjusts to see in background
            if (theme) {
                GraphMaker.get_line_chart(lineChart, Color.BLACK, formatter, line_data);
            } else {
                GraphMaker.get_line_chart(lineChart, Color.WHITE, formatter, line_data);
            }


        } catch (Exception e) {
            // displays a error to the user
            MessageAndTextUtils.error_alert((Activity) root.getContext(), null,
                    getString(R.string.ErrorCreatingGraphMessage) + e.toString(),
                    getString(R.string.GraphFragmentErrorTitle));
        }
    }

}
