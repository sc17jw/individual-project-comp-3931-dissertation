package com.example.personalised_assistant.ui_fragments.notes;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.example.personalised_assistant.R;
import com.example.personalised_assistant.utils.GlobalVariables;
import com.example.personalised_assistant.utils.MessageAndTextUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.Objects;


public class NotesFragment extends Fragment {
    /**
     * This is the layout used to add text views and display directory's and note files
     */
    private LinearLayout layout;
    /**
     * This is a text view used to create the file ,directory names and no entry's text.
     */
    private TextView temp_view;
    /**
     * This is the base directory to stop traversal before this directory
     */
    private File baseDirectory;
    /**
     * This is the size of the text view size.
     */
    private static final int text_size = 80;

    /**
     * This method will display the users notes and allow them to create new notes
     *
     * @return this method will return a inflated view.
     */
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_notes, container, false);
        GlobalVariables globalVariable = (GlobalVariables) requireActivity().getApplicationContext();
        layout = root.findViewById(R.id.notes_layout);
        // sets the base directory
        String noteDirectory = root.getContext().getFilesDir().getPath() + "/Notes/";
        baseDirectory = new File(noteDirectory);
        // Tries to update number of page visits.
        try {
            JSONObject profile = globalVariable.getUserProfile();
            globalVariable.insertKeyIntoProfile("note_number_of_opens",
                    String.valueOf(profile.getInt("note_number_of_opens") + 1), requireActivity());

        } catch (JSONException e) {
            // Gives the user a error message saying it couldn't update this value
            MessageAndTextUtils.error_alert(requireActivity(), null,
                    getString(R.string.ErrorGettingUserData),
                    getString(R.string.NoteFragementErrorTitle));
        }

        File fileNoteDirectory = new File(noteDirectory);
        // checks to see if the base directory exists
        if (!fileNoteDirectory.exists()) {
            boolean mkdirs = fileNoteDirectory.mkdirs();
            // checks if the directory was made
            if (!mkdirs){
                MessageAndTextUtils.error_alert(requireActivity(),null,
                        getString(R.string.ErrorCreatingNoteDir),
                        getString(R.string.noteFragmentErrorTitle));
                return root;
            }
            // Asks the user to create a note since their is not any
            temp_view = new TextView(root.getContext());
            temp_view.setTextSize(text_size);
            temp_view.setText(R.string.no_notes_add_entru);
            layout.addView(temp_view);
            // adds the new note button
            addNewEntryButton(root);
        } else {
            // shows the users the directory list.
            getCurrentDirectoryFiles(fileNoteDirectory, root);
        }
        // writes user profile to disk.
        globalVariable.writeUserProfileToDisk(requireActivity());
        return root;
    }

    /**
     * This method will display and traverse the users notes. It allows for users to view past notes and create new notes.
     *
     * @param currentFileObject This is the file object for the current directory and is used to traverse the notes directory
     * @param root This is the root view used to add text views to the view.
     */
    private void getCurrentDirectoryFiles(File currentFileObject, View root) {
        // Checks if not in the base directory. If not then it will not allow the user to go past the file directory in the application folder
        if (currentFileObject.toPath().compareTo(baseDirectory.toPath()) > 0) {
            temp_view = MessageAndTextUtils.makeTextView(
                    requireContext(),"...",
                    true,0,text_size);
            // on click listener for going back a directory
            temp_view.setOnClickListener(v -> {
                File fileUpOneDirectory = new File(currentFileObject.toPath().getParent() + "/");
                layout.removeAllViews();
                getCurrentDirectoryFiles(fileUpOneDirectory, root);

            });
            layout.addView(temp_view);
        }
        // Adds and displays the current folders and notes
        for (int i = 0; i < Objects.requireNonNull(currentFileObject.list()).length; i++) {
            temp_view = new TextView(root.getContext());
            temp_view.setTextSize(text_size);
            // Changes directory colours
            if (Objects.requireNonNull(currentFileObject.listFiles())[i].isDirectory()) {
                temp_view.setTextColor(getResources().getColor(R.color.colorPrimaryDark, null));
            }
            temp_view.setText(Objects.requireNonNull(currentFileObject.list())[i]);
            // Sets a onclick listener updating the directory list
            temp_view.setOnClickListener(v -> {
                TextView current_item = (TextView) v;
                File temp = new File(currentFileObject.toPath() + "/" + current_item.getText().toString());
                if (temp.isDirectory()) {
                    layout.removeAllViews();
                    getCurrentDirectoryFiles(temp, root);
                } else {
                    // Navigates to the view note fragment
                    Navigation
                            .findNavController(root)
                            .navigate(
                                    NotesFragmentDirections.actionNavNotesToViewNoteFragment(temp.toString()
                                    )
                            );
                }
            });
            // adds the text to the layout
            layout.addView(temp_view);
        }
        // adds the button to the layout
        addNewEntryButton(root);
    }

    /**
     * This method will add a button to the view to allow the user to create a new entry.
     *
     * @param root this is the current inflated view used to add a button to the
     */
    private void addNewEntryButton(View root) {
        Button create_new_button = new Button(root.getContext());
        create_new_button.setText(R.string.new_entry);
        // on click event to navigate to create a new note fragment
        create_new_button.setOnClickListener(v ->
                Navigation.findNavController(root)
                        .navigate(
                                NotesFragmentDirections.actionNavNotesToNewNoteFragment()
                        )
        );
        // adds the button to the layout
        layout.addView(create_new_button);

    }

}
