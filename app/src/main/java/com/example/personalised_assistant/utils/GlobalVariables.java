package com.example.personalised_assistant.utils;

import android.app.Activity;
import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;

import com.example.personalised_assistant.R;
import com.example.personalised_assistant.samsung_health.SamsungHealth;
import com.example.personalised_assistant.snapchat_utils.SnapchatUtilities;
import com.example.personalised_assistant.watch_service.WatchService;

import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 */
public class GlobalVariables extends Application {
    /**
     * This is the users file encryption key.
     */
    private String key;
    /**
     * A pointer to the watch service
     */
    private WatchService watchService = null;
    /**
     * This is a service on connect listener for the watch service
     */
    private final ServiceConnection connect_watch = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            watchService = ((WatchService.LocalBinder) service).getService();
            watchService.findPeers();
        }

        @Override
        public void onServiceDisconnected(ComponentName className) {
            watchService = null;
        }
    };
    /**
     * This is the temp user profile loaded into memory. This is used to reduce i/o transaction. This could be obfuscated in memory with some kind of encryption but is rather pointless as if they have permissions to dump memoery they could get the key or if the key also has some kind of obfuscation function reverse engineer that process making it irrelevant only increasing reverse engineering time or a extra step in the process but this could be added here. Or we could utilise the strongbox (A Isolated processor on the phone pcb that preforms encryption functions and stores keys in new smartphone) and keystore for some smartphone to generate a AES key and encrypt and decrypt this data on the fly but this is costly and will not add much added protection.
     */
    private JSONObject userProfileJSONObject = null;
    /**
     * This is the setup flag. This starts the set up if set to true
     */
    private boolean setup = false;
    /**
     * This is a error flag that if true will limit functionality eg login because it indicates reading profile their was error.
     */
    private boolean errorUserProfileJSONObject = false;
    /**
     * This flag indicants that the basic emulator detection has discovered that its running in a emulator.
     */
    private boolean isEmulator = false;
    /**
     * This is a pointer to the samsung health object which allows us to get updated values from shealth.
     */
    private SamsungHealth health;
    /**
     * This is the current food item the user want to add.
     */
    private String foodItem;
    /**
     * This is a pointer to the snapchat object
     */
    private SnapchatUtilities snapChatUtilitiesObject;
    /**
     * This is the data that has come from the watch sister application.
     */
    private byte[] watchData;

    /**
     * This method is a getter method for getting the setup flag that tells the application weather to enter setup mode.
     *
     * @return This method will return a boolean value true if the application is in setup mode false if not.
     */
    public boolean getSetUpFlag() {
        return setup;
    }

    /**
     * This is a setter method for setting the setup flag if the setup file does not exist.
     */
    public void setSetUpFlag(boolean is_setup) {
        setup = is_setup;
    }

    /**
     * @return This is a getter method that will return the snapchat object.
     */
    public SnapchatUtilities getSnapChatObject() {
        return snapChatUtilitiesObject;
    }

    /**
     * This method is a setter method for setting the global snapchat object.
     *
     * @param snapChatObject This is the current snapchat object.
     */
    public void setSnapChatObject(SnapchatUtilities snapChatObject) {
        snapChatUtilitiesObject = snapChatObject;
    }

    /**
     * This method is a getter method returning the samsung health object
     *
     * @return This method will return the current samsung health object.
     */
    public SamsungHealth getSamsungHealth() {
        return health;
    }

    /**
     * @param activity this is needed to connect to the samsung health store.
     */
    public void setSamsungHealth(Activity activity) {
        health = new SamsungHealth(activity);
    }

    /**
     * This method will attempt to set the user profile by reading it from memory
     */
    public void setUserProfile() {
        //Check if the device is a emulator and sets the user profile to the demo user profile
        if (isEmulator) {
            // Gets the demo profile.
            userProfileJSONObject = UserProfile.getDemoProfile(this.getFilesDir().toString());
            return;
        }
        // Gets the user profile or non demo users.
        try {
            userProfileJSONObject = UserProfile.getProfile(this.getFilesDir().toString(), key);
            errorUserProfileJSONObject = false;
        } catch (Exception e) {
            // Their was a error while getting user profile
            errorUserProfileJSONObject = true;
        }
    }

    /**
     * This method will attempt to read and return the user profile from memory if not then try load the profile in from storage
     *
     * @return The return value is the user profile in a JSONObject format
     */
    public JSONObject getUserProfile() {
        if (userProfileJSONObject == null) {
            setUserProfile();
        }
        // returns the loaded json profile
        return userProfileJSONObject;
    }

    /**
     * This method returns a boolean for weather the user profile has loaded or failed
     *
     * @return This will return true if the user profile fails to be loaded else return false.
     */
    public Boolean getUserProfileIsError() {
        return errorUserProfileJSONObject;
    }

    /**
     * This setter method will set the user profle error flag indicating their was a issue when reading the user profile.
     *
     * @param error this is boolean value true if their was a error while reading the file.
     */
    public void setUserProfileIsError(Boolean error) {
        errorUserProfileJSONObject = error;
    }

    /**
     * This method will write the user profile to disk.
     *
     * @param activity This is the current activity to allows for pop up error messages
     */
    public void writeUserProfileToDisk(Activity activity) {
        // checks if its a emulator
        if (isEmulator) {
            UserProfile.WriteDemoProfileToDisk(userProfileJSONObject, activity);
            return;
        }
        // writes the encrypted user profile to disk.
        UserProfile.Write_Profile_To_Disk(userProfileJSONObject, activity);
    }

    /**
     * This method will insert a value into the json object
     *
     * @param key      This is the key for the value you want to insert
     * @param value    this is the value you want to insert
     * @param activity This is the current activity to allows for pop up error messages
     */
    public void insertKeyIntoProfile(String key, String value, Activity activity) {
        // inserts a key into the user profile
        UserProfile.insertStringIntoUserProfile(userProfileJSONObject, key, value, activity);
    }

    /**
     * This will insert a object into the user profile
     *
     * @param key      This is the key for the value you want to insert
     * @param object   This is the JSONObject that you want to insert to the user profile
     * @param activity This is the current activity to allows for pop up error messages
     */
    public void insertObjectIntoProfile(String key, JSONObject object, Activity activity) {
        // Check to see if the app is in demo mode
        if (isEmulator){
            try {
                userProfileJSONObject.put(key,object);
                return;
            } catch (JSONException e) {
                // displays a error to the user
                MessageAndTextUtils.error_alert(activity, null,
                        getString(R.string.demoGamerTypeError) + e.toString(),
                        getString(R.string.signUpError));
                return;
            }
        }
        // inserts a object into the user profile
        UserProfile.insertObjectIntoUserProfile(userProfileJSONObject, key, object, activity);
    }

    /**
     * Sets the users encryption key.
     *
     * @param user_key This is the users encryption key.
     */
    public void setUserKey(String user_key) {
        key = user_key;
    }

    /**
     * gets the user profile encryption key
     *
     * @return the users encryption key
     */
    public String getKey() {
        return key;
    }

    /**
     * gets the current food item being recommended.
     *
     * @return This is the food item that has been recommended
     */
    public String getFoodItem() {
        return foodItem;
    }

    /**
     * Sets the current food item being recommended.
     *
     * @param foodItem This is the food item that has been recommended
     */
    public void setFoodItem(String foodItem) {
        this.foodItem = foodItem;
    }

    /**
     * This is used to test if the device is a emulator and if so will alter code execution to accommodate to the device eg putting in demo mode and disabling samsung health.
     *
     * @return Returns true if the device is a emulator and false if not.
     */
    public boolean isEmulator() {
        return isEmulator;
    }

    /**
     * sets the emulator flag altering code execution and enabling demo mode
     *
     * @param isEmulator this is a boolean value true being its running in a emulator false not.
     */
    public void setIsEmulator(boolean isEmulator) {
        this.isEmulator = isEmulator;
    }

    /**
     * This method will return any data the watch sends back
     *
     * @return returns a byte array of the returned received watchData
     */
    public byte[] getWatchData() {
        return watchData;
    }

    /**
     * This function sets the watchData byte array to the sent data from the watch.
     *
     * @param data this is a byte array of the data received from the watch
     */
    public void setWatchData(byte[] data) {
        watchData = data;
    }

    /**
     * This method will return the watch service object allowing communication between watch and phone.
     *
     * @return the watch service object
     */
    public WatchService getWatchService() {
        return watchService;
    }

    /**
     * This will set the current watch object.
     */
    public void setWatchService() {
        // Connects to the watch service.
        bindService(new Intent(getApplicationContext(), WatchService.class), connect_watch, Context.BIND_AUTO_CREATE);
    }
}
