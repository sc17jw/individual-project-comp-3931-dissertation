package com.example.personalised_assistant.utils;

import android.app.Activity;
import android.util.Log;

import com.example.personalised_assistant.R;
import com.google.firebase.auth.FirebaseAuth;

import org.apache.commons.compress.compressors.FileNameUtil;
import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

/**
 * This class has only static functions and is used to handle all of the profile functions.
 */
public class UserProfile {
    /**
     * This is the user profile aperture to the file directory of the application
     */
    private static final String PROFILE_FILE_NAME = "/profile.json";
    /**
     * This is the leader-board aperture to the file directory of the application
     */
    private static final String LEADER_BOARD_FILE_NAME = "/leader_board.json";
    /**
     * This is the maximum number of leader-board positions
     */
    private static final int LEADER_BOARD_MAX = 200;
    /**
     * This is the globalVariables object and is used to store variables across the application
     */
    static private GlobalVariables globalVariables;

    public static void setGlobalVars(GlobalVariables globalVariables) {
        UserProfile.globalVariables = globalVariables;
    }

    /**
     * This method is a wrapper function for the global variables to read in the user profile. This function returns a decypted json object from the profile
     *
     * @param file_directory This is the directory of the application eg /data/data/com.example.personalisedassistant/files/
     * @param key            This is the deception key created when the user logs in
     * @return This function returns the loaded json file unless it calls a exception
     * @throws Exception This function throws IllegalStateException,IllegalBlockSizeException,ShortBufferException,BadPaddingException , AEADBadTagException and file reading errors IOExeption and JAVA SecurityException (security policy set) etc..
     */
    static JSONObject getProfile(String file_directory, String key) throws Exception {
        return FileUtilities.load_json_object(file_directory + PROFILE_FILE_NAME, key);
    }

    /**
     * This method will insert a string into the user profile into the key provided
     *
     * @param userProfileJSONObject This is the user profile object
     * @param key This is the key you want to insert the value into
     * @param value This is the value you want to insert into the user profile
     * @param activity This is the current activity used to create a error dialog message
     */
    static void insertStringIntoUserProfile(JSONObject userProfileJSONObject, String key, String value, Activity activity) {
        try {
            userProfileJSONObject.put(key, value);
        } catch (JSONException e) {
            MessageAndTextUtils.alert(activity, null,
                    activity.getString(R.string.ErrorUpdateUserProfileKey) + e,
                    activity.getString(R.string.ErrorUpdateUserProfileTitleError));
        }
    }

    /**
     * This method will insert a json object into the user profile in the key provided
     *
     * @param userProfileJSONObject This is the user profile from the global variables object
     * @param key This is the key that you want to insert into the profile
     * @param object This is the JSONObject you want ot insert into the profile
     * @param activity This is the calling activity used to create dialog
     */
    static void insertObjectIntoUserProfile(JSONObject userProfileJSONObject, String key, JSONObject object, Activity activity) {
        try {
            userProfileJSONObject.put(key, object);
        } catch (JSONException e) {
            MessageAndTextUtils.alert(activity, null, 
                    activity.getString(R.string.ErrorUpdateProfile) + e, 
                    activity.getString(R.string.ErrorUpdateUserProfileTitleError));
        }
    }

    /**
     * This method is a wrapper function for the global variables to read in the demo user profile. This function returns a json Object
     *
     * @param file_directory This is the directory of the application eg /data/data/com.example.personalisedassistant/files/
     * @return This function returns the loaded json file unless it calls a exception
     */
    static JSONObject getDemoProfile(String file_directory) {
        return FileUtilities.load_demo(file_directory + PROFILE_FILE_NAME);
    }

    /**
     * This method writes the user profile passed to it to disk and is a wrapper for Write_Read_File.write_json_object;
     *
     * @param userProfileJSONObject this is the user profile to be writen to disk.
     * @param activity              This is a pointer to a activity to get globals_vars object and the enycption key
     */
    static void Write_Profile_To_Disk(JSONObject userProfileJSONObject, Activity activity) {
        globalVariables = (GlobalVariables) activity.getApplicationContext();
        FileUtilities.write_json_object(userProfileJSONObject, activity.getFilesDir().toString() + PROFILE_FILE_NAME, globalVariables.getKey());
    }

    /**
     * This method writes the demo profile passed to it to disk and is a wrapper for Write_Read_File.writeDemofile;
     *
     * @param userProfileJSONObject this is the user profile to be writen to disk.
     * @param activity              This is a pointer to a activity to get globals_vars object and the enycption key
     */
    static void WriteDemoProfileToDisk(JSONObject userProfileJSONObject, Activity activity) {
        FileUtilities.write_demo(
                userProfileJSONObject,
                activity.getFilesDir().toString() + PROFILE_FILE_NAME
        );
    }

    /**
     * This method generates the users profile. It takes a array of values that are generated when the user signs up 27 long
     *
     * @param string_array This is a array of strings that contains all the user data and is phased to create the user profile
     * @param activity     This is a pointer to the current activity to display error dialog and to get the user key to write to disk
     */
    static void create_profile(String[] string_array, Activity activity) {
        try {
            // checks if array is right size if not gives user error message then quits
            if (string_array.length != 27) {
                MessageAndTextUtils.error_alert(activity,
                        (dialog, which) -> System.exit(-1),
                        activity.getString(R.string.fatalErrorInvaidArraySize),
                        activity.getString(R.string.createUserProfileError));
            }
            globalVariables = (GlobalVariables) activity.getApplicationContext();
            JSONObject json = new JSONObject();
            json.put("name", string_array[0]); // sets the users name in the user profile used to create leader board
            json.put("age", string_array[1]); // sets their age used to get the recommend intake for their age group
            json.put("gender", string_array[2]); // sets their gender this is used to calculate the recommend intake for their gender
            json.put("gamer_type", string_array[3]); // this is used for the adaptive feature.
            json.put("total_steps", string_array[4]); // This is used to create leveling
            json.put("level", string_array[5]); //This is the users current level
            json.put("sleep_streak", string_array[6]); // This is the users current sleep streak of longer than 8 hours in a row
            json.put("max_sleep_streak", string_array[7]);  // This is the maximum sleep streak of 8 hours or longer
            json.put("note_sentiment", string_array[8]); // This is the users current note sentiment from google cloud sentiment analysis
            json.put("level_number_of_opens", string_array[9]); // This is the number of opens for the level fragment
            json.put("leaderbord_number_of_opens", string_array[10]); // This is the number of opens for the Leader-Board fragment
            json.put("achievements_number_of_opens", string_array[11]); // This is the number of opens for the Achievement fragment
            json.put("note_number_of_opens", string_array[12]); // This is the number of opens for the note fragment
            json.put("leader_bord_position", string_array[13]); // This is the users current Leader-Board position
            JSONObject achievements = new JSONObject(); // creates a temp json object for adding the achievements to the user profile
            // Puts the number of achievement in the json object
            achievements.put("number_of_achievements",
                    Integer.parseInt(string_array[5]) + 1);
            // Puts the start achievement to encourage users
            json.put("achievements", achievements.put("Start_achievement", "true"));
            // Loops thought and adds all the levels achievements from the imported step count
            for (int i = 1; i <= Integer.parseInt(string_array[5]); i++) {
                json.put("achievements", achievements.put("level_achievement" + i, "true"));
            }
            // Sets the last times since the data points were updated in milliseconds Unix Epoch time
            json.put("last_step_time", string_array[14]);
            json.put("last_sleep_time", string_array[15]);
            json.put("last_exercises_time", string_array[16]);
            json.put("last_cal_burned_time", string_array[17]);
            json.put("last_cal_intake_time", string_array[18]);
            json.put("last_hartrate_time", string_array[19]);
            // Nutrition information about the user from SamsungHealth
            json.put("total_cals_consumed", string_array[20]);
            json.put("total_cals_burned", string_array[21]);
            /* This is a json object of the nutrition data recorded by shealth in the format
             * "nutrition":{"CALORIE":"","CALCIUM":"","CARBOHYDRATE":"","CHOLESTEROL":"",
             * "DIETARY_FIBER":"","IRON":"","SATURATED_FAT":"","POTASSIUM":"","PROTEIN":"",
             * "SODIUM":"","SUGAR":"","TOTAL_FAT\":"","TRANS_FAT":"","VITAMIN_A":"","VITAMIN_C":"",
             * "MEAL_COUNT":""}*/
            json.put("nutrition", new JSONObject(string_array[22]));
            /* This is a json array of the activities recoded by shealth in the format
             * "exercise":"","calorie":,"duration":,"start_time":,"end_time".*/
            json.put("exercises", new JSONArray(string_array[23]));
            /*This is a json array of the hart rate data recoded by shealth in the format
             * "hart_rate":"[{"HEART_BEAT_COUNT":,"HEART_RATE":,"MAX":,"MIN":,"start_time":,"end_time":}]*/
            json.put("hart_rate", new JSONArray(string_array[24]));
            /* This is a json array of the sleep data recoded by shealth in the format
             * "sleep_data":[{"sleep_time":,"start_time":,"end_Time":}]*/
            json.put("sleep_data", new JSONArray(string_array[25]));

            json.put("personality", new JSONObject(string_array[26]));
            // Writes the user profile to disk
            Write_Profile_To_Disk(json, activity);
            globalVariables.setUserProfile();
        } catch (Exception e) {
            Log.e("Create Profile", "Error while creating Profile", e);
        }
    }

    /**
     * This method will create a leader board for the user and will return the current position in the leader board.
     *
     * @param level           This is the current user level and is used to add them to the leader board
     * @param name            This is the name the user signed up with it and is displayed on the leader board
     * @param file_directory  This is the file directory for saving the leader board
     * @param globalVariables This is the global variables object and is used to get the encryption key if not in a emulator
     * @return This method will return the player position in the generated leader board
     */
    public static int createLeaderBoard(int level, String name, String file_directory, GlobalVariables globalVariables) {
        // Sets the user to default last place if in last place this will be their position
        int position = LEADER_BOARD_MAX;
        try {
            JSONArray leaderBoadJsonArray = new JSONArray();
            for (int i = 1; i < LEADER_BOARD_MAX; i++) {
                JSONObject currentPlayer = new JSONObject();
                // inserts the user position in the leader board
                if (i <= level && ((i + 1) > level || i == LEADER_BOARD_MAX - 1)) {
                    currentPlayer.put("name", name);
                    currentPlayer.put("level", i);
                    leaderBoadJsonArray.put(currentPlayer);
                    position = LEADER_BOARD_MAX - i;
                    continue;
                }
                // Puts a fake leader board for demo purposes
                currentPlayer.put("name", "Friend " + i);
                currentPlayer.put("level", i);
                leaderBoadJsonArray.put(currentPlayer);
            }
            // Checks if the device is a emulator and writes the leader board for the appropriate type
            if (!globalVariables.isEmulator()) {
                FileUtilities.write_json_array(leaderBoadJsonArray,
                        file_directory + LEADER_BOARD_FILE_NAME, // The leader board file name
                        globalVariables.getKey()); // the users encryption key
            } else {
                FileUtilities.write_demo_array(leaderBoadJsonArray,
                        file_directory + LEADER_BOARD_FILE_NAME);
            }
            return position;
        } catch (Exception e) {
            // logs the error
            Log.e("create leader-board error", "error while creating leader-board", e);
        }
        return position;
    }

    /**
     * This method will return the leader board in a json array
     *
     * @param directory  This is the directory path of the application
     * @param globalVariables This is the global variables object used to get the user key if its not in demo mode
     * @return This method will return the leader board of the user
     */
    public static JSONArray getLeaderBoard(String directory, GlobalVariables globalVariables) {
        if (!globalVariables.isEmulator()) {
            return FileUtilities.load_json_array(directory + "/leader_board.json", globalVariables.getKey());
        }
        return FileUtilities.load_demo_array(directory + "/leader_board.json");
    }

    /**
     * THis method will return the users achievement in a string format
     *
     * @param globalVariable This is the global variables object used to get the users profile
     * @return This will return the users achievement in a string format
     */
    public static String getAchievements(GlobalVariables globalVariable) {
        JSONObject achievements = globalVariable.getUserProfile();
        try {
            return achievements.getString("achievements");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * This method adds achievements to the user profile
     *
     * @param achievement_name The name if the achievement
     * @param activity         The calling activity is passed as a parameters to be used to get the global vars
     */
    public static void add_achievements(String achievement_name, Activity activity) {
        globalVariables = (GlobalVariables) activity.getApplicationContext();
        JSONObject achievements = globalVariables.getUserProfile();
        try {
            achievements.getJSONObject("achievements").put(achievement_name, "true");
            int level = achievements.getJSONObject("achievements").getInt("number_of_achievements");
            achievements.getJSONObject("achievements").put("number_of_achievements", level + 1);
            FileUtilities.write_json_object(achievements, activity.getFilesDir().toString() + PROFILE_FILE_NAME, globalVariables.getKey());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Method that deletes the users profile
     *
     * @param file_directory This is the directory of the user application
     */
    public static void delete_profile(String file_directory) throws IOException {
        File file = new File(file_directory);
        FileUtils.deleteDirectory(file);
    }

}
