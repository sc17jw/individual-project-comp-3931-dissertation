package com.example.personalised_assistant.utils;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.example.personalised_assistant.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;

/*** This class writes and reads the encrypted user profile. All method's are static. */
public class FileUtilities {
    /**
     * Sets the salt size for key deviation
     */
    private static final int SALT_SIZE = 128;
    /**
     * Sets the iv size
     */
    private static final int IV_SIZE = 16;
    /*** This is used to clean up the code a bit*/
    private static OutputStream outputStream = null;

    /**
     * This function writes a json array to a file
     *
     * @param json           This is the json array you want to write to the file
     * @param file_directory This is the file path of the file you want to write.
     * @param key            This is the encryption key.
     */
    static void write_json_array(JSONArray json, String file_directory, String key) {
        //Creates a byte array the size of the salt
        byte[] salt = new byte[128];
        // Creates a byte array for the IV
        byte[] iv;
        try {
            // Creates a new outputStream for the file to be whiten
            outputStream = new FileOutputStream(new File(file_directory));
            // Creates a sufficiently random salt for key deviation
            SecureRandom secure_random_salt = new SecureRandom();
            secure_random_salt.nextBytes(salt);
            // Creates the cipher object
            Cipher encryptionCipher = CryptographyFunctions
                    .getEncryptionCipher(key, salt);
            assert encryptionCipher != null;
            // Gets the iv from the cipher
            iv = encryptionCipher
                    .getParameters()
                    .getParameterSpec(IvParameterSpec.class)
                    .getIV();
            // Writes the salt to the file
            outputStream.write(salt);
            // Writes the IV to the file
            outputStream.write(iv);
            //Writes the encrypted data to the file
            outputStream.write(encryptionCipher.doFinal(json.toString().getBytes()));
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method is used to write json objects to a file. Primary in this application the user profile
     *
     * @param json           This is the json object you want to write to encrypt and write to a file.
     * @param file_directory This is the path of the file you want to write
     * @param key            This is the key used to encrypt the file
     */
    public static void write_json_object(JSONObject json, String file_directory, String key) {
        //Creates a byte array the size of the salt
        byte[] salt = new byte[SALT_SIZE];
        // Creates a byte array for the iv
        byte[] iv;
        try {
            // Creates a new outputStream for the file to be whiten
            outputStream = new FileOutputStream(new File(file_directory));
            // Creates a sufficiently random salt for key deviation
            SecureRandom secure_random_salt = new SecureRandom();
            secure_random_salt.nextBytes(salt);
            // Creates the cipher object
            Cipher encryptionCipher = CryptographyFunctions
                    .getEncryptionCipher(key, salt);
            assert encryptionCipher != null;
            // Gets the iv from the cipher
            iv = encryptionCipher
                    .getParameters()
                    .getParameterSpec(IvParameterSpec.class)
                    .getIV();
            // Writes the salt to the file
            outputStream.write(salt);
            // Writes the iv to the file
            outputStream.write(iv);
            //Writes the encrypted data to the file
            outputStream.write(encryptionCipher.doFinal(json.toString().getBytes()));
            outputStream.close();
        } catch (Exception e) {
            // Prints a stack trace for debugging
            e.printStackTrace();
        }
    }

    /**
     * This method is used to write the bitemoji data base of the user.
     *
     * @param json           This is the json array of the database that is going to be writen to the file
     * @param file_directory This is hte output file directory to be write to.
     */
    public static void write_bitemoji_data(JSONArray json, String file_directory) {
        try {
            // creates a new output stream for the designated file.
            outputStream = new FileOutputStream(new File(file_directory));
            //Writes the database to the database file.
            outputStream.write(json.toString().getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method reads the bitemoji database from file and returns the json array.
     *
     * @param file_directory This is the path to the file
     * @return The function will return a json array
     */
    public static JSONArray read_bitemoji_data(String file_directory) {
        try {
            // Reads in the bitemoji database into a byte array to then be written to file
            ByteBuffer buffer = ByteBuffer.wrap(Files.readAllBytes(Paths.get(file_directory)));
            return new JSONArray(new String(buffer.array(), StandardCharsets.UTF_8));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * This function writes a image to a file
     *
     * @param picture        This is the bitmap image you want to write to a file.
     * @param file_directory This is the file path that you want to write the bitmap to.
     */
    public static void write_image(Bitmap picture, String file_directory) {
        try {
            //Creates a new output stream with the file directory specified in the method parameter
            outputStream = new FileOutputStream(new File(file_directory));
            //Writes the image to the file.
            picture.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * This static method is used to load a json array from a file.
     *
     * @param file_directory This is the file path you want to read form
     * @param key            this is the encryption key
     * @return This returns a unencrypted json array
     */
    public static JSONArray load_json_array(String file_directory, String key) {
        // Creates salt and iv byte array
        byte[] salt = new byte[SALT_SIZE];
        byte[] iv = new byte[IV_SIZE];
        try {
            ByteBuffer buffer = ByteBuffer.wrap(Files.readAllBytes(Paths.get(file_directory)));
            //sets the salt byte array to the value from file
            buffer.get(salt);
            //sets the IV byte array to the value from file
            buffer.get(iv);
            byte[] encryptedDB = new byte[buffer.remaining()];
            buffer.get(encryptedDB);
            Cipher temp = CryptographyFunctions.getDecryptionCipher(key, salt, iv);
            assert temp != null;
            return new JSONArray(new String(temp.doFinal(encryptedDB), StandardCharsets.UTF_8));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * This static method is used to load and return a json object form a file primary the user profile in this application.
     *
     * @param file_directory This is the path to the file
     * @param key            This is the encryption key
     * @return this returns the decrypted json object
     * @throws Exception This function throws a bad decrypt error and file opening errors.
     */
    public static JSONObject load_json_object(String file_directory, String key) throws Exception {
        // Creates salt and iv byte array
        byte[] salt = new byte[SALT_SIZE];
        byte[] iv = new byte[IV_SIZE];
        ByteBuffer buffer = ByteBuffer.wrap(Files.readAllBytes(Paths.get(file_directory)));
        //sets the salt byte array to the value from file
        buffer.get(salt);
        //sets the IV byte array to the value from file
        buffer.get(iv);
        byte[] encryptedDB = new byte[buffer.remaining()];
        buffer.get(encryptedDB);
        Cipher temp = CryptographyFunctions.getDecryptionCipher(key, salt, iv);
        assert temp != null;
        return new JSONObject(new String(temp.doFinal(encryptedDB), StandardCharsets.UTF_8));
    }

    /**
     * This method reads the demo files.
     *
     * @param file_directory This is the path to the file
     * @return The function will return a json object
     */
    public static JSONObject load_demo(String file_directory) {
        try {
            // Loads the demo file into a byte array
            ByteBuffer buffer = ByteBuffer.wrap(Files.readAllBytes(Paths.get(file_directory)));
            // Returns the demo json object
            return new JSONObject(new String(buffer.array(), StandardCharsets.UTF_8));
        } catch (Exception e) {
            // Prints a stack trace for debugging
            e.printStackTrace();
        }
        return null;
    }

    /**
     * This method reads the demo files.
     *
     * @param file_directory This is the path to the file
     * @return The function will return a json array
     */
    public static JSONArray load_demo_array(String file_directory) {
        try {
            // Loads the demo file into a byte array
            ByteBuffer buffer = ByteBuffer.wrap(Files.readAllBytes(Paths.get(file_directory)));
            // Returns the demo json array
            return new JSONArray(new String(buffer.array(), StandardCharsets.UTF_8));
        } catch (Exception e) {
            // Prints a stack trace for debugging 
            e.printStackTrace();
        }
        return null;
    }

    /**
     * This method writes a unencrypted file to the application directory and is used when the demo mode is activate
     *
     * @param json           This is the JSONObject you want to write to the disk.
     * @param file_directory This is the path to the file
     */
    public static void write_demo(JSONObject json, String file_directory) {
        try {
            // Creates a new file output stream to write the json object to the file.
            outputStream = new FileOutputStream(new File(file_directory));
            // gets the bytes of the JSONObject and writes it to the file.
            outputStream.write(json.toString().getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method writes a unencrypted file to the application directory and is used when the demo mode is activate
     *
     * @param json           This is the JSONArray you want to write to a unencrypted file
     * @param file_directory This is the path to the file
     */
    static void write_demo_array(JSONArray json, String file_directory) {
        try {
            // Creates a new file output stream to write the json object to the file.
            outputStream = new FileOutputStream(new File(file_directory));
            // gets the bytes of the JSONArray and writes it to the file.
            outputStream.write(json.toString().getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method creates the demo files needed to run in demo mode. It gets the demo files from a raw resource and writes them to the app folder.
     *
     * @param activity This is a pointer to the current activity
     * @return It will return the result of writing files if not 0 then their was a error while getting and writing the demo profile to disk
     */
    public static int makeDemoFiles(Activity activity) {
        try {
            // This is used to test if writing the files is successful
            int result = 0;
            // Get the demo avatar image
            InputStream stream = activity.getResources().openRawResource(R.raw.avatar);
            // Creates a storage byte buffer for the avatar the size of the image due to the small size of the image
            byte[] temp = new byte[stream.available()];
            // Reads into the buffer
            result += stream.read(temp);
            // Writes the image to a file.
            FileUtilities.write_image(
                    BitmapFactory.decodeByteArray(temp, 0, temp.length),
                    activity.getFilesDir() + "/avatar.png");
            // Gets the demo snapchat database
            stream = activity.getResources().openRawResource(R.raw.bitemoji_cartoon);
            // Creates a buffer the size of the database. This is again because of the small size.
            temp = new byte[stream.available()];
            // Reads in the snapchat database into the buffer.
            result += stream.read(temp);
            // Writes the snapchat database to disk
            FileUtilities.write_bitemoji_data(
                    new JSONArray(new String(temp)),
                    activity.getFilesDir() + "/bitemoji_cartoon.json");
            //Gets the demo user profile
            stream = activity.getResources().openRawResource(R.raw.profile);
            // Creates a buffer the size of the user profile. This is again because of the small size.
            temp = new byte[stream.available()];
            // Reads in the user profile into the byte array buffer
            result += stream.read(temp);
            /// Writes the user profile to disk
            FileUtilities.write_demo(
                    new JSONObject(new String(temp)),
                    activity.getFilesDir() + "/profile.json");
            // Returns the result 0 if successful
            return result;
        } catch (Exception e) {
            // Notifies the user of a exception if one happens
            MessageAndTextUtils.error_alert(activity, null, "We have encountered a small error while reading and writing in a the demo database", " Demo mode Error Writing Profile");
        }
        return -1; // Exception error returns -1
    }
}
