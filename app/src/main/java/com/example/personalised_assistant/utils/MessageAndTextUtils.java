package com.example.personalised_assistant.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.widget.TextView;

import com.example.personalised_assistant.R;

/**
 * This class is used to create Error and Dialog messages as well as text views.
 */
public class MessageAndTextUtils {
    /**
     * This method will create a dialog option notifying the user of a error with the provided message
     *
     * @param activity       This is the current activity used to create the dialog box
     * @param okListener     This is the on click listener (which can be null ) for a positive responses.
     *                       This will call the onclick listener if the value is positive
     * @param CancelListener This is a onclick listener (which can be null) for a negative response form the user.
     *                       This will call the onclick listener provided
     * @param message        This is the message you want to display to the user
     * @param classNameError This is the title fo the popup menu
     */
    static public void error_message(Activity activity,
                                     DialogInterface.OnClickListener okListener,
                                     DialogInterface.OnClickListener CancelListener,
                                     String message, String classNameError) {

        //Creates and shows a new error message popup box displayed in the activity provided
        new AlertDialog.Builder(activity)
                .setTitle("Error" + classNameError) // Sets the error title
                .setMessage(message) // sets the message to be displayed
                .setPositiveButton(android.R.string.yes, okListener) // sets the positive button listener for the popup
                .setNegativeButton(android.R.string.no, CancelListener) // Sets the negative button listener for the popup
                .setIcon(android.R.drawable.ic_dialog_alert) // sets the icon for the popup
                .show(); // Shows the popup
    }

    /**
     * This method will create a dialog option notifying the user of a error with the provided message
     *
     * @param activity       This is the current activity used to create the dialog box
     * @param okListener     This is the on click listener (which can be null ) for a positive responses. T
     *                       his will call the onclick listener if the value is positive
     * @param message        This is the message you want to display to the user
     * @param classNameError This is the title fo the popup menu
     */
    static public void error_alert(Activity activity,
                                   DialogInterface.OnClickListener okListener,
                                   String message, String classNameError) {

        //Creates and shows a new error message popup box displayed in the activity provided
        new AlertDialog.Builder(activity)
                .setTitle("Error" + classNameError)// Sets the error title
                .setMessage(message) // sets the message to be displayed
                .setPositiveButton(android.R.string.yes, okListener)// sets the positive button for the popup
                .setIcon(android.R.drawable.ic_dialog_alert)// sets the icon for the popup
                .show();// Shows the popup
    }

    /**
     * This method will create a dialog option notifying the user of a provided message
     *
     * @param activity   This is the current activity used to create the dialog box
     * @param okListener This is the on click listener (which can be null ) for a positive responses.
     *                   This will call the onclick listener if the value is positive
     * @param message    This is the message you want to display to the user
     * @param title      this is the title for the popup box
     */
    static public void alert(Activity activity,
                             DialogInterface.OnClickListener okListener,
                             String message, String title) {
        new AlertDialog.Builder(activity)
                .setTitle(title)// Sets the alert title
                .setMessage(message)// sets the message to be displayed
                .setPositiveButton(android.R.string.yes, okListener) // sets the positive button for the popup
                .setIcon(android.R.drawable.presence_online)// sets the icon for the popup
                .show();// Shows the popup
    }

    /**
     * This method will give the user the option to reset their user profile to a new type.
     *
     * @param activity       This is the current activity used to create the dialog box
     * @param okListener     This is the on click listener (which can be null ) for a positive responses.
     *                       This will call the onclick listener if the value is positive
     * @param cancelListener This is the negative onclick listener
     * @param message        This is the message you want to put in the text view
     * @param title          This is the title for the popup
     */
    static void reset(Activity activity,
                      DialogInterface.OnClickListener okListener,
                      DialogInterface.OnClickListener cancelListener,
                      String message, String title) {

        new AlertDialog.Builder(activity)
                .setTitle(title)// Sets the title for the popup box
                .setMessage(message) // sets the message to be displayed
                .setPositiveButton(android.R.string.yes, okListener)
                .setNegativeButton(R.string.no_thankyou, cancelListener)// Sets the negative button listener for the popup
                .setIcon(android.R.drawable.presence_online)// sets the icon for the popup
                .show();// Shows the popup
    }

    /**
     * This method will create a text view with the values added. This is used to reduce code size
     *
     * @param context This is the context of the current activity used to create the text view
     * @param message This is the message you want to put in the text view
     * @param isBold  This sets the message to be bold if true
     * @param color   This sets the color if 0 sets default system
     * @param size    This is the size of the text
     * @return This method will return the textView made in the function
     */
    static public TextView makeTextView(Context context, String message,
                                        boolean isBold, int color, int size) {
        // Creates the text view object
        TextView textViewTemp = new TextView(context);
        //set the size of the text
        textViewTemp.setTextSize(TypedValue.COMPLEX_UNIT_SP, size);
        // if the color is 0 then set to default theme
        if (color != 0) {
            textViewTemp.setTextColor(color);
        }
        // makes the text bold
        if (isBold) {
            textViewTemp.setTypeface(null, Typeface.BOLD);
        }
        //sets the text of the text view
        textViewTemp.setText(message);
        return textViewTemp; // Returns the created object.
    }

}
