package com.example.personalised_assistant.utils;

import android.app.Activity;
import android.content.DialogInterface;

import com.example.personalised_assistant.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * This class preforms the gamer type calculations and updates the users personality based upon collected data from notes.
 */
public class GamerTypeAndPersonality {
    /**
     * This is the amount of user notes required to change the gamer type if less than the user wont be prompt to change.
     * THis needs to be tweaked to best match user preferences. This also could be changed dynamically to better match the user or on certain values
     */
    private static final int COUNT_INBETWEEN_ADAPTIVE_CHANGE = 50;

    /**
     * This method is used to init the users stereotype for their gamer type.
     *
     * @param gamer_type This is the gamer type the user selected during setup
     * @return This method will return the stereotype of their gamer type in personalities
     * @throws JSONException This method will throw a json Exception for better error handling
     */
    public static JSONObject initStereotype(String gamer_type) throws JSONException {
        JSONObject personality = new JSONObject();

        //init values so not multiplying by zero
        personality.put("Openness", 1);
        personality.put("Conscientiousness", 1);
        personality.put("Extraversion", 1);
        personality.put("Agreeableness", 1);
        personality.put("Neuroticism", 1);
        personality.put("Gamer_Type", gamer_type);

        // needed to reduce computation getting average from the data set
        personality.put("Openness_added", 1);
        personality.put("Conscientiousness_added", 1);
        personality.put("Extraversion_added", 1);
        personality.put("Agreeableness_added", 1);
        personality.put("Neuroticism_added", 1);

        // Sets the stereotype values based upon the user selected type.
        switch (gamer_type) {
            case "Achiever":
                personality.put("Conscientiousness", 2);
                personality.put("Conscientiousness_added", 1);
                personality.put("Neuroticism", -2);
                personality.put("Neuroticism_added", 1);
                break;
            case "Player":
                personality.put("Conscientiousness", 2);
                personality.put("Conscientiousness_added", 1);
                personality.put("Openness", 2);
                personality.put("Openness_addedd", 1);
                break;
            case "Socialiser":
                personality.put("Extraversion", 2);
                personality.put("Extraversion_added", 1);
                personality.put("Agreeableness", 2);
                personality.put("Agreeableness_added", 1);
                break;
            case "Free Spirit":
                personality.put("Extraversion", 2);
                personality.put("Extraversion_added", 1);
                personality.put("Openness", 2);
                personality.put("Openness_added", 1);
                personality.put("Neuroticism", -2);
                personality.put("Neuroticism_added", 1);
                break;
        }
        // Increases the total count of updates
        personality.put("Count", 1);
        // Return the jsonObject
        return personality;
    }

    /**
     * This method will update the users personality type and gamer type if pasted the threshold.
     * it will notify the user asking them if they want to change it.
     *
     * @param globalVariable      This is the globalVariables object used to update the user profile
     * @param personality_tones   This is the response from the api to about note tones
     * @param activity            This is a pointer to the activity to allow for dialogs
     * @param newStereotypeDialog This is a listener for when the method has finished updating the values
     * @throws JSONException This method will throw a JSON exception for better error handling
     */
    public static void updatePersonalityValues(GlobalVariables globalVariable,
                                               JSONArray personality_tones,
                                               Activity activity,
                                               DialogInterface.OnClickListener newStereotypeDialog) throws JSONException {

        String[] personality_type = {"Openness", "Conscientiousness", "Extraversion", "Agreeableness", "Neuroticism"};
        String[] personality_last_added = {"Openness_added", "Conscientiousness_added", "Extraversion_added", "Agreeableness_added", "Neuroticism_added"};

        JSONObject personality = globalVariable.getUserProfile().getJSONObject("personality");

        int count = personality.getInt("Count") + 1;
        // itter though each of the 5 personality types
        for (int i = 0; i < 5; i++) {
            double last_added = (personality.getDouble(personality_last_added[i]) + (personality_tones.getJSONObject(i).getDouble("score")));
            // Checks if double is about ot overflowed or under flowed
            if ((Double.MIN_VALUE + 0.000001) > last_added || last_added > (Double.MAX_VALUE - 100)) {
                // Display a user error message
                MessageAndTextUtils.error_alert(activity, null,
                        globalVariable.getString(R.string.UnderOverFlowError) +
                                personality.getString("Gamer_Type") // tells the user the stereotype its resetting back to
                        , globalVariable.getString(R.string.ErrorSteroTypeTitle));
                // init the stereotype giving default values
                initStereotype(personality.getString("Gamer_Type"));
            }
            personality.put(personality_last_added[i], String.valueOf(last_added));
            personality.put(personality_type[i], String.valueOf((last_added / count)));
            // Allows the user to change stereotype
            String gamer_type = getAdaptType(activity, personality);
            // Checks to see if the application has got enough data to trigger a new gamer type update allowing the user to make the choice
            if (gamer_type.compareTo(personality.getString("Gamer_Type")) != 0 && count >= COUNT_INBETWEEN_ADAPTIVE_CHANGE) {
                // tells the user what the app recommends as the new gamer type.
                MessageAndTextUtils.reset(activity, newStereotypeDialog, null,
                        globalVariable.getString(R.string.GamerTypeChangePrompt)
                                + gamer_type + globalVariable.getString(R.string.moreThan) +
                                personality.getString("Gamer_Type"), // gets the current gamer type
                        globalVariable.getString(R.string.UpdateGamerType));
            }
        }
        // update the Count for the number of entries of personality
        personality.put("Count", count);
        // updates the user profile with the new personality values
        globalVariable.insertObjectIntoProfile("personality", personality, activity);
    }

    /**
     * This method will get the adaptive feature for the passed current gamer type
     *
     * @param activity This is used to create dialogs and is the calling activity
     * @param bigFive  This is a JSONObject of the personality in the format
     *                 "personality":{"
     *                 Openness":,"Conscientiousness":,"Extraversion":,"Agreeableness":,"Neuroticism":,
     *                 "Openness_added":,"Conscientiousness_added":,"Extraversion_added":,"Agreeableness_added":,"Neuroticism_added":,
     *                 "Gamer_Type":"Socialiser",
     *                 "Count":1}
     * @return This method will return a String of the users gamer type.
     */
    public static String getAdaptType(Activity activity, JSONObject bigFive) {
        try {
            // Loads the users values from the profile for their personality traits.
            double extroversion = bigFive.getDouble("Extraversion"), conscientiousness = bigFive.getDouble("Conscientiousness"),
                    openness = bigFive.getDouble("Openness"), agreeableness = bigFive.getDouble("Agreeableness"),
                    neuroticism = bigFive.getDouble("Neuroticism");

            // Calculates the probability of the users being a gamer type 
            double socialiserProbability = (extroversion + agreeableness) / 2,
                    freeSpiritProbability = (extroversion + openness + (-neuroticism)) / 3,
                    achieverProbability = conscientiousness + (-neuroticism) / 2,
                    playerProbability = (conscientiousness + agreeableness) / 2;

            // Uses probability to determine type 
            if (playerProbability > achieverProbability &&
                    playerProbability > socialiserProbability &&
                    playerProbability > freeSpiritProbability) {
                // Returns the Player type to adapt to
                return "Player";
            } else if (freeSpiritProbability > socialiserProbability &&
                    freeSpiritProbability > achieverProbability) {
                // Returns the Free Spirit type to adapt to
                return "Free Spirt";
            } else if (socialiserProbability > achieverProbability) {
                // Returns the Socliser type to adapt to
                return "Socliser";
            } else {
                // Returns the achiever type to adapt to 
                return "Achiver";
            }
        } catch (Exception e) {
            // Gives the user a error if it fails to get adaptive feature 
            MessageAndTextUtils.error_alert(activity, null,
                    activity.getString(R.string.erroAdataptiveFeature) + e,
                    activity.getString(R.string.ErrorAdaptation));
        }
        return "";
    }

}