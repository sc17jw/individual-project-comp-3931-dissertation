package com.example.personalised_assistant.utils;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Point;
import android.view.Display;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.charts.RadarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.data.RadarData;
import com.github.mikephil.charting.data.RadarDataSet;
import com.github.mikephil.charting.data.RadarEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IRadarDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;

/**
 *  This class populates and sets up graphs.
 */
public class GraphMaker {
    /**
     * This method generates the data for the pie chart and populates the chart
     *
     * @param pieChart This is the pie chart object
     * @param color This is the text color of the chart
     * @param axisItems This is the axsis names
     * @param dataLable This is the label for the graph
     * @param dataPoints These are the data points for each item
     */
    public static void get_pie_chart(PieChart pieChart, int color,String[] axisItems,String dataLable,double[] dataPoints){
        // sets the graph colors and chart settings
        pieChart.setDrawEntryLabels(true);
        pieChart.getDescription().setEnabled(false);
        pieChart.setExtraOffsets(5, 10, 5, 5);
        pieChart.setDragDecelerationFrictionCoef(0.95f);
        pieChart.setCenterText(dataLable);
        pieChart.setDrawHoleEnabled(true);
        pieChart.setHoleColor(color);
        pieChart.setTransparentCircleColor(color);
        pieChart.setTransparentCircleAlpha(110);
        pieChart.setHoleRadius(30f);
        pieChart.setTransparentCircleRadius(40f);
        pieChart.setDrawCenterText(true);
        // sets the rotation
        pieChart.setRotationAngle(0);
        pieChart.setRotationEnabled(true);
        // allows for selecting and highlighting of pie chart segments
        pieChart.setHighlightPerTapEnabled(true);
        // Sets the chart animation time
        pieChart.animateY(1000, Easing.EaseInOutQuad);
        //Creates a list of colors for the chart
        ArrayList<Integer> colors = new ArrayList<>();
        //populates this list
        for (int i : ColorTemplate.COLORFUL_COLORS) {
            colors.add(i);
        }
        // Creates a new pie data object used to add values to the chart
        PieData data = new PieData();
        // Adds all the data to the chart
        ArrayList<PieEntry> values = new ArrayList<>();
        for (int i = 0; i < dataPoints.length; i++) {
            //fixes bug for negative values
            if(dataPoints[i]<0){
                values.add(new PieEntry((float) (-(dataPoints[i])),"Negative "+axisItems[i]));
                continue;
            }
            values.add(new PieEntry((float) dataPoints[i],axisItems[i]));
        }
        // adds the entries to a data set and sets formatting
        PieDataSet dataSet= new PieDataSet(values,dataLable);
        dataSet.setSliceSpace(3.1f);
        dataSet.setSelectionShift(5.2f);
        dataSet.setColors(colors);
        //adds the data to the chart
        data.addDataSet(dataSet);
        // Sets a formatter for the labels and sets the formatting of the chart
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(11f);
        data.setValueTextColor(color);
        // sets the chart data
        pieChart.setData(data);
        // sets the chart layout
        pieChart.getLegend().setTextColor(color);
        pieChart.getLegend().setOrientation(Legend.LegendOrientation.VERTICAL);
        pieChart.highlightValues(null);
        pieChart.invalidate();

    }

    /**
     * This method formats the line chart and adds data to it.
     *
     * @param line_chart This is a line chart object you want ot populate wit the data provided
     * @param color THis is the color for the text
     * @param formatter This is the axis formatter used to create dates in this application
     * @param data_entry This is the data entry set for the graph generating the portions of data points in the chart
     */
    public static void get_line_chart(LineChart line_chart, int color, IndexAxisValueFormatter formatter, ArrayList<LineDataSet> data_entry){
        // Gets the axis information
        XAxis x_axis_bottom = line_chart.getXAxis();
        YAxis y_axis_left = line_chart.getAxisLeft();
        // Enables user interaction with the graph
        line_chart.setTouchEnabled(true);
        line_chart.setDragEnabled(true);
        line_chart.setScaleEnabled(true);
        line_chart.setHighlightPerDragEnabled(true);
        line_chart.setPinchZoom(true);

        // Sets the x axis
        y_axis_left.setDrawAxisLine(true);
        y_axis_left.setDrawLabels(true);
        y_axis_left.setDrawTopYLabelEntry(true);
        y_axis_left.setTextColor(color);

        // Sets the x axis position and draws it
        x_axis_bottom.setPosition(XAxis.XAxisPosition.BOTTOM);
        x_axis_bottom.setDrawGridLines(true);
        x_axis_bottom.setDrawAxisLine(true);
        // Fixes bottom labels
        x_axis_bottom.setGranularity(1);
        x_axis_bottom.setGranularityEnabled(true);
        // sets the x axis labels
        x_axis_bottom.setDrawLabels(true);
        x_axis_bottom.setTextColor(color);
        x_axis_bottom.setLabelRotationAngle(-90);
        // Sets the formatter so the graph will show the x axis in date format
        x_axis_bottom.setValueFormatter(formatter);

        // Disables the left y axis from showing
        YAxis y_axis_1 = line_chart.getAxisRight();
        y_axis_1.setDrawLabels(false);

        LineData data = new LineData();
        // Adds the data set added as a entry to the line chart
        for (int i = 0; i < data_entry.size();i++) {
            data.addDataSet(data_entry.get(i));
        }

        data.setValueTextColor(color);
        data.setValueTextSize(9f);
        data.setDrawValues(true);

        //Sets the legend color and disables description
        line_chart.getLegend().setTextColor(color);
        line_chart.getDescription().setEnabled(false);
        line_chart.setData(data);
    }

    /**
     * This method will set the radar chart provided up with a theme and populate with data.
     *
     * @param radar_chart This is the radar chart
     * @param color this is the background color
     * @param X_axis_items this is the x axis labels
     * @param data_points This is the data points used to create the chart
     * @param dataLabel This is the label for the chart
     */
    static void get_radar_chart(RadarChart radar_chart, int color, String[] X_axis_items, int[] data_points, String dataLabel){
        // sets chart styling and colors
        radar_chart.setBackgroundColor(color);
        radar_chart.setWebLineWidth(10f);
        radar_chart.setWebLineWidthInner(10f);
        radar_chart.setWebAlpha(50);
        radar_chart.setWebColor(Color.BLACK);
        radar_chart.setWebColorInner(Color.BLACK);
        // creates a radar entry object used to create points in the chart
        ArrayList<RadarEntry> data_entry = new ArrayList<>();
        // Adds the data into the radar chart
        for (int data_point : data_points) {
            data_entry.add(new RadarEntry(data_point));
        }
        // Creates radar data sets and their lables
        RadarDataSet data_set = new RadarDataSet(data_entry,dataLabel);
        //sets the data color and fill
        data_set.setColor(Color.rgb(255, 0, 0));
        data_set.setFillColor(Color.rgb(0, 0, 0));
        // fills and sets graph features
        data_set.setDrawFilled(true);
        data_set.setFillAlpha(180);
        data_set.setLineWidth(10f);
        data_set.setDrawHighlightCircleEnabled(true);
        data_set.setDrawHighlightIndicators(false);
        /// Creates a new list of data entry objects for the graph
        ArrayList<IRadarDataSet> sets = new ArrayList<>();
        // adds data to the set
        sets.add(data_set);
        // Creates a new Radar chat data object. This allows for the data to be displayed in the chart
        RadarData data = new RadarData(sets);
        //Set the text size to 20
        data.setValueTextSize(20f);
        // draw values on chart
        data.setDrawValues(true);
        // Set the color to white
        data.setValueTextColor(Color.WHITE);
        // Sets the data on the chart
        radar_chart.setData(data);
        // Sets the x size
        radar_chart.setY(20f);
        // Animates the chart to 1 second
        radar_chart.animateXY(1000, 1000, Easing.EaseInOutQuad);
        // Gets the x Axis
        XAxis xAxis = radar_chart.getXAxis();
        // sets the text size on the x axis to 15
        xAxis.setTextSize(15f);
        // sets the text color to green
        xAxis.setTextColor(Color.GREEN);
        // sets the formatter to give correct scaling
        ValueFormatter format =  new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                return X_axis_items[(int) value % X_axis_items.length];
            }
        };
        // sets the axis formatter
        xAxis.setValueFormatter(format);
        // gets the y axis to set axis min and max and disables labels for better looks
        YAxis yAxis = radar_chart.getYAxis();
        yAxis.setDrawLabels(false);
        //sets the maximum axis to be 12 the lowest possible value
        yAxis.setAxisMaximum(12f);
        //sets the minimum axis to be -12 the lowest possible value
        yAxis.setAxisMinimum(-12f);
        radar_chart.getLegend().setEnabled(false);
        radar_chart.getDescription().setEnabled(false);

    }

    /**
     * This method will return the screen size of the device and allow for dynamic graph sizing
     * @param activity This is the current activity.
     * @return This is a int array with the screen with begin first and the screen height begin second
     */
    public static int[] get_screen_size(Activity activity){
        Display display = activity.getWindowManager().getDefaultDisplay(); // This gets the display object
        Point screenSize = new Point(); // this creates a new point object used to derive screen coords
        try {
            display.getRealSize(screenSize); // This get the size of the screen in x and y coords
        } catch (NoSuchMethodError err) {
            display.getSize(screenSize);
        }
        return new int[]{screenSize.x,screenSize.y}; // return x and y values x being the far right pixel
        // and y bing the top right pixel so the top right corner
    }
}
