package com.example.personalised_assistant.utils;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.provider.Settings;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;

import com.example.personalised_assistant.R;
import com.example.personalised_assistant.ui_activities.Home;
import com.example.personalised_assistant.watch_service.WatchService;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.snapchat.kit.sdk.SnapLogin;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;

/**
 * This class logs the user in.
 */
public class Login {
    /**
     * These are constants for the caption system and the lock out attempts
     */
    private static final int LOGIN_ATTEMPTS_BEFORE_CAPTION_VALUE = 5, MAX_LOGIN_ATTEMPTS_BEFORE_LOCK = 100;
    /**
     * This is the firebase object used to sign in and get user info.
     */
    private static FirebaseAuth firebaseAuth;
    /**
     * This is a counter for the number of login attempts and is used to caption the user after so many attempt and then exit
     */
    private static int loginCounter = 0;
    /**
     * This is the users email and password used to login using fire base
     */
    private static String email, password;
    /**
     * This is the global variables object and is used to store values across classes
     */
    private static GlobalVariables globalVariables;
    /**
     * This listener will be triggered when the user completes the google caption or if they close it.
     * When the user completes the caption correctly it will attempt to log the user in.
     * If the user closes it a dialog box will show notifying the user they must complete the caption due to too many attempts with incorrect details.
     */
    private static final SecurityVerification.caption captionResponseTooManyLoginAttempts = (Message, context) -> {
        Activity activity = (Activity) context;
        if (Message.compareTo("Successful") == 0) {
            firebaseAuth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            FirebaseUser user = firebaseAuth.getCurrentUser();
                            assert user != null;
                            globalVariables.setUserKey(CryptographyFunctions.sha512((user.getUid() + email + password), activity));
                            // Tries to decrypt the user profile
                            globalVariables.setUserProfile();
                            // if theirs a error tell the user that it is incorrect
                            if (globalVariables.getUserProfileIsError()) {
                                MessageAndTextUtils.error_alert(activity, null, "Incorrect User or Password", " Login");
                                activity.findViewById(R.id.login_btn).setClickable(true);
                                return;
                            }
                            activity.findViewById(R.id.login_btn).setClickable(true);
                            int level = Level.getLevel(globalVariables, activity);
                            globalVariables.insertKeyIntoProfile("level", String.valueOf(level), activity);
                            globalVariables.writeUserProfileToDisk(activity);
                            Intent intent = new Intent(activity, Home.class);
                            activity.startActivity(intent);
                        } else {
                            activity.findViewById(R.id.login_btn).setClickable(true);
                            MessageAndTextUtils.error_alert(activity, null, "Incorrect User or Password", "Login");
                        }
                    });
        } else {
            MessageAndTextUtils.error_alert((Activity) context, null, Message, " CAPTION VERIFICATION ERROR");
        }
    };
    /**
     * THis listener will change the theme of the application to dark or light depending on what the user sets it to.
     */
    private static final View.OnClickListener setTheme = v -> {
        Activity activity = (Activity) v.getContext();
        try {
            if (((Switch) v).isChecked()) {
                CryptographyFunctions.getFromEncryptedSharedPreferences(
                        activity, true,
                        "light_theme", "true",
                        "true");
                activity.recreate();
            } else {

                CryptographyFunctions.getFromEncryptedSharedPreferences(
                        activity, true,
                        "light_theme", "false",
                        "false");

                activity.recreate();
            }
        } catch (GeneralSecurityException | IOException e) {
            e.printStackTrace();
        }
    };
    /**
     * This listener will open the android accessibility settings due to android not allowing applications to change these settings.
     * This allows the user to change the settings with a quick button in the application.
     */
    private static final View.OnClickListener setAccessibility = v -> {
        Activity activity = (Activity) v.getContext();
        MessageAndTextUtils.alert((Activity) v.getContext(),
                (dialog, which) ->
                        activity.startActivityForResult(new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS), 0),
                activity.getString(R.string.acessability_message_to_settings),
                activity.getString(R.string.Accessibility));
    };
    /**
     * THis listener will try log the user in by checking if the user has not tried to spam or brute force the login system
     */
    private static final View.OnClickListener loginAttempt = view -> {
        Activity activity = (Activity) view.getContext();
        final EditText emailAddressTxtbox = activity.findViewById(R.id.email_address_txtbox);
        final EditText passwordTxtbox = activity.findViewById(R.id.password_txtbox);

        loginCounter++;
        activity.findViewById(R.id.login_btn).setClickable(false);

        email = emailAddressTxtbox.getText().toString();
        password = passwordTxtbox.getText().toString();

        if (password.isEmpty() || email.isEmpty()) {
            MessageAndTextUtils.error_message(activity, null, null,
                    activity.getString(R.string.please_type_in_user_name_and_password),
                    activity.getString(R.string.login_error));

            activity.findViewById(R.id.login_btn).setClickable(true);
            return;
        }
        // Locks the user out after 100 attempts maybe give the user the option to delete their profile if this many attempts happens
        if (loginCounter > MAX_LOGIN_ATTEMPTS_BEFORE_LOCK) {
            // Add a appropriate lowdown timer here.
            MessageAndTextUtils.error_alert(activity, null, "Too many login attempts try again later", " Login");
            return;
        }
        // If the user passes the login attempts limit of 5 then they will be captioned to increase the difficulty of brute forcing the password
        if (loginCounter > LOGIN_ATTEMPTS_BEFORE_CAPTION_VALUE) {
            SecurityVerification.captionTheUser(activity, captionResponseTooManyLoginAttempts); // caption the user then attempt to login
            return;
        }
        if (globalVariables.isEmulator()) {
            globalVariables.getUserProfile();
            Intent intent = new Intent(activity, Home.class);
            activity.startActivity(intent);
            return;
        }
        // Trys to login the user in
        firebaseAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        FirebaseUser user = firebaseAuth.getCurrentUser();
                        assert user != null;
                        // sets the encryption key
                        globalVariables.setUserKey(CryptographyFunctions.sha512((user.getUid() + email + password), activity));
                        // Tries to decrypt the user profile
                        globalVariables.setUserProfile();
                        if (globalVariables.getUserProfileIsError()) {
                            MessageAndTextUtils.error_alert(activity, null, "Incorrect User or Password", " Login");
                            activity.findViewById(R.id.login_btn).setClickable(true);
                            return;
                        }
                        activity.findViewById(R.id.login_btn).setClickable(true);
                        int level = Level.getLevel(globalVariables, activity);
                        globalVariables.writeUserProfileToDisk(activity);
                        Intent intent = new Intent(activity, Home.class);
                        activity.startActivity(intent);
                    } else {
                        activity.findViewById(R.id.login_btn).setClickable(true);
                        MessageAndTextUtils.error_alert(activity, null, "Incorrect User or Password", " Login");
                    }
                });
    };

    /**
     * This method will attempt to login and decrypt the user profile
     *
     * @param activity This is the current activity.
     */
    public static void login(Activity activity) {
        Switch light_theme = activity.findViewById(R.id.toggle_theme);
        boolean light;
        try {
            light = Boolean.parseBoolean(
                    CryptographyFunctions.getFromEncryptedSharedPreferences(
                            activity, false,
                            "light_theme", "",
                            "false"));

            // If light mode is enabled set the light toggle to ticked
            if (light) {
                (light_theme).setChecked(true);
                light_theme.setTextColor(Color.BLACK);
            } else {
                light_theme.setTextColor(Color.WHITE);
            }
        } catch (GeneralSecurityException | IOException e) {
            e.printStackTrace();
        }
        globalVariables = (GlobalVariables) activity.getApplicationContext();

        light_theme.setOnClickListener(setTheme);
        activity.findViewById(R.id.acessibility_button).setOnClickListener(setAccessibility);

        ImageView welcome_image = activity.findViewById(R.id.imageView);
        if (SnapLogin.isUserLoggedIn(activity) || globalVariables.isEmulator()) {
            activity.findViewById(R.id.connect_bit_emoji__loginbtn).setVisibility(View.GONE);
            globalVariables.getSnapChatObject().

                    getBitEmojiImage("welcome", new String[]{"no biggie", "your", "no problem"},
                            url -> {
                                Picasso.get()
                                        .load(url)
                                        .noFade()
                                        .placeholder(R.drawable.place_holder)
                                        .error(R.drawable.place_holder)
                                        .into(welcome_image);
                                Bitmap bitmap = Bitmap.createBitmap(640, 480, Bitmap.Config.ARGB_8888);
                                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                                bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
                                WatchService watchService = globalVariables.getWatchService();
                                if (watchService != null) {
                                    watchService.findPeers();
                                    watchService.sendData(outputStream.toByteArray(), activity);
                                }
                            });

        } else {
            welcome_image.setVisibility(View.GONE);
        }
        // INIT vars
        firebaseAuth = FirebaseAuth.getInstance();
        activity.findViewById(R.id.login_btn).setOnClickListener(loginAttempt);

    }

}
