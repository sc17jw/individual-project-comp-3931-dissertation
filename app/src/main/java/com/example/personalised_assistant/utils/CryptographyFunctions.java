package com.example.personalised_assistant.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import androidx.security.crypto.EncryptedSharedPreferences;
import androidx.security.crypto.MasterKeys;

import java.io.IOException;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * This class is a collection of cryptographic functions from getting cipher instances to hashing ; This class only consists of static functions and has no constructor
 */
public class CryptographyFunctions {
    /**
     * This sets the iteration count and key length for the cipher
     */
    private static final int iterationCount = 12000, encryptionKeyLength = 256;
    /**
     * Sets the hash function to be used
     */
    private static final String hashFunction = "SHA-512";
    /**
     * Sets the cipher mode to user
     */
    private static final String cipherMode = "AES/CBC/PKCS5Padding";
    /**
     * Sets the secret key generation algorithm
     */
    private static final String keyFactoryMode = "PBKDF2WithHmacSHA512";
    /**
     * Sets the encryption algorithm to use
     */
    private static final String encryptionAlgorithm = "AES";

    /**
     * This method will create a encrypted Shared-Preferences file for holding non critical values or values that do not require high security. This will create a key in the KeyStore and use it to create a encrypted preference, It uses AES 256 bit encryption in GCM mode. For security Reasons this should be made package private or class private. This if made public allows writing and reading whatever to the file defeating the point.
     *
     * @param context     This is the context of the application. This is used to create the EncryptedSharedPreferences object. It is required by that function as my best guess to do getSharedPreferences() to get the file directory of the program
     * @param commit      This changes it into commit mode adding the values to the encrypted EncryptedSharedPreferences object
     * @param key         This is the key for the value you want to get or enter.
     * @param value       This is the value you want to enter to the encrypted preference.
     * @param default_val This is the default value if not found in the preference.
     * @return it will either return a empty string if its in commit mode or will return the value you want to get
     * @throws GeneralSecurityException This is thrown by the keystore object and will stop getting or setting values.
     * @throws IOException              This is thrown when writing the Shared preferences to the preferences folder of the application
     */
    public static String getFromEncryptedSharedPreferences(Context context, boolean commit, String key, String value, String default_val) throws GeneralSecurityException, IOException {
        // Gets the key alias from the keystore
        String keyAlias = MasterKeys.getOrCreate(MasterKeys.AES256_GCM_SPEC);
        // Creates a encrypted shared preferences object from the androidx security class
        SharedPreferences sharedPreferences = EncryptedSharedPreferences.create(
                "encrypted_preference",
                keyAlias,
                context,
                EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
                EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
        );
        // Checks weather its a set or get preference and preforms the required actions.
        if (commit) {
            SharedPreferences.Editor preference_edition = sharedPreferences.edit();
            // puts the string and the key into the shared preference.
            preference_edition.putString(key, value);
            preference_edition.apply();
        } else {
            // Returned shared preference if in get mode
            return sharedPreferences.getString(key, default_val);
        }
        // Return empty if in set mode .
        return "";
    }

    /**
     * This method hashes a string and returns the results it is set to SHA512
     *
     * @param toHash  this is the string to be hashed,
     * @param activity This is the activity so we can give the user a error message thought a popup message box
     * @return This will return the hashed string entered to the function.
     */
    public static String sha512(String toHash, Activity activity) {
        try {
            // Creates a hash instance of type hash_function
            MessageDigest messageDigest = MessageDigest.getInstance(hashFunction);
            // Hashes the string and puts it into a byte array
            byte[] hashed_user = messageDigest.digest(toHash.getBytes());
            // Converts the hexadecimal byte array back to a String format.
            return String.format("%032X", new BigInteger(1, hashed_user));
        } catch (NoSuchAlgorithmException e) {
            // Gives the user a error if can hash item
            MessageAndTextUtils.error_message(activity, null, null,
                    "error while creating hash " + e.toString(),
                    " HASH generation");
        }
        return null;
    }

    /**
     * This method creates a Cipher object in encryption mode  and returns it.
     *
     * @param encryptionKey This is the encryption key that you want to use. This is used to init the cipher,
     * @param salt          This is the salt that is generated
     * @return This function returns a cipher object in encryption mode
     */
    static Cipher getEncryptionCipher(String encryptionKey, byte[] salt) {
        try {
            // Creates a a instance of PBKDF2 for key deviation
            SecretKeyFactory pbkdf2Instance = SecretKeyFactory.getInstance(keyFactoryMode);
            // Creates a key spec for ken generation
            KeySpec keySpec = new PBEKeySpec(encryptionKey.toCharArray(), salt, iterationCount, encryptionKeyLength);
            // Creates a cipher instance with the desired cipher mode
            Cipher cipher = Cipher.getInstance(cipherMode);
            // Creates a cipher with the desired spec
            cipher.init(Cipher.ENCRYPT_MODE,
                    new SecretKeySpec(
                            pbkdf2Instance.generateSecret(keySpec).getEncoded(),
                            encryptionAlgorithm)
            );
            // returns the cipher in encryption mode.
            return cipher;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * This method creates a Cipher object in decryption mode and returns it.
     *
     * @param decryptionKey This is the key used to create a decryption cipher object.
     * @param salt          This is the salt used when encrypting the file
     * @param iv            This is the initialisation vector or iv used to start the AES cipher. This is used to allow the first block to be decrypted
     * @return This function returns a Cipher object in decryption mode
     */
    static Cipher getDecryptionCipher(String decryptionKey, byte[] salt, byte[] iv) {
        try {
            // Creates a a instance of PBKDF2 for key deviation
            SecretKeyFactory pbkdf2Instance = SecretKeyFactory.getInstance(keyFactoryMode);
            // Creates a key spec for ken generation
            KeySpec keySpec = new PBEKeySpec(decryptionKey.toCharArray(), salt, iterationCount, encryptionKeyLength);
            // Creates a cipher instance with the desired cipher mode
            Cipher cipher = Cipher.getInstance(cipherMode);
            // Creates a cipher with the desired spec
            cipher.init(
                    Cipher.DECRYPT_MODE, new SecretKeySpec(
                            pbkdf2Instance.generateSecret(keySpec).getEncoded(),
                            encryptionAlgorithm),
                    new IvParameterSpec(iv)
            );
            // returns the cipher in decryption mode.
            return cipher;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
