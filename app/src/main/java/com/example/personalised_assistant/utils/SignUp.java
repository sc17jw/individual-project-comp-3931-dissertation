package com.example.personalised_assistant.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.provider.Settings;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.example.personalised_assistant.R;
import com.example.personalised_assistant.samsung_health.SamsungHealth;
import com.example.personalised_assistant.ui_activities.Home;
import com.github.mikephil.charting.charts.RadarChart;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.json.JSONObject;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Calendar;
import java.util.Objects;
import java.util.TimeZone;

/**
 * This class is used to sign a user up to the application
 */
public class SignUp {
    /**
     * This is the firebase authentication object used to log users in
     */
    private static FirebaseAuth firebaseAuth;
    /**
     * This is the firebase user object and is used to get user data from the login
     */
    private static FirebaseUser user;
    /**
     * This is a string array used as temporary storage for user profile creation
     */
    private static String[] userDataStringArray;
    /**
     * This is the array for the gamer type this determines what gamer type the user is.
     */
    private static final int[] gamer_type = {-100, 0, 0, 0, 0, 0};
    /**
     * This is the email and password string hold as temporary storage to pass to the firebase sign up method
     */
    private static String email, password;
    /**
     * This is the globalVariables object and is used to store variables across the application
     */
    private static GlobalVariables globalVariables;
    /**
     * Changes code execution depending on if the user need to verify their email 
     */
    private static boolean verifyEmail = false;
    /**
     * Counter for how many emails are sent.
     */
    private static int emailSend = 0;
    /**
     * The number of retry's for the email resend
     */
    private final static int MAX_EMAIL_RESEND = 5;
    /**
     * THis listener will change the theme of the application to dark or light depending on what the user sets it to.
     */
    private static final View.OnClickListener set_theme = v -> {
        Activity activity = (Activity) v.getContext();
        try {
            if (((Switch) v).isChecked()) {
                CryptographyFunctions.getFromEncryptedSharedPreferences(
                        activity, true,
                        "light_theme", "true",
                        "true");
                ((Switch) v).setTextColor(Color.BLACK);

                activity.recreate();
            } else {

                CryptographyFunctions.getFromEncryptedSharedPreferences(
                        activity, true,
                        "light_theme", "false",
                        "false");
                ((Switch) v).setTextColor(Color.WHITE);

                activity.recreate();
            }
        } catch (GeneralSecurityException | IOException e) {
            e.printStackTrace();
        }
    };
    /**
     * This is the public interface for the caption response. If the caption is correct then it will sign the user up to the application.
     */
    private static final SecurityVerification.caption captionResponseAndSignUp = new SecurityVerification.caption() {
        @Override
        public void onChanged(String Message, Context context) {

            Activity activity = (Activity) context;
            if (Message.compareTo("Successful") == 0) {
                firebaseAuth.createUserWithEmailAndPassword(email, password)
                        .addOnCompleteListener((Activity) context, task -> {
                            if (globalVariables.isEmulator()) {
                                try {
                                    // This set the setup value to be false disabling setup
                                    CryptographyFunctions.getFromEncryptedSharedPreferences(
                                            context, true,
                                            "setup", "false",
                                            "");
                                } catch (Exception exception) {
                                    // Notifies the user of a error creating setup files.
                                    MessageAndTextUtils.error_alert((Activity) context, null,
                                            context.getString(R.string.ErrorWritingFile) + exception.toString(),
                                            context.getString(R.string.ErrorWritingFIleTitle));
                                }


                                user = firebaseAuth.getCurrentUser();
                                globalVariables.setSetUpFlag(false);
                                globalVariables.getUserProfile();
                                try {
                                    final Spinner gamerTypeSpinBox = activity.findViewById(R.id.gamer_type_spin);
                                    JSONObject personality = GamerTypeAndPersonality.initStereotype(
                                            gamerTypeSpinBox.getSelectedItem().toString()
                                    );
                                    globalVariables.insertObjectIntoProfile("personality", personality, activity);
                                } catch (Exception e) {
                                    MessageAndTextUtils.error_alert(activity, null,
                                            activity.getString(R.string.errorCreatingSterotype) + e.toString(),
                                            activity.getString(R.string.SignUpError));
                                }
                                user.delete();
                                Intent intent = new Intent(context, Home.class);
                                context.startActivity(intent);
                                return;
                            }
                            if (task.isSuccessful()) {
                                user = firebaseAuth.getCurrentUser();
                                try {
                                    // This set the setup value to be false disabling setup
                                    CryptographyFunctions.getFromEncryptedSharedPreferences(
                                            context, true,
                                            "setup", "false",
                                            "true");
                                } catch (Exception exception) {
                                    // Notifies the user of a error creating setup files.
                                    MessageAndTextUtils.error_alert((Activity) context, null,
                                            context.getString(R.string.ErrorWritingFile) + exception.toString(),
                                            context.getString(R.string.ErrorWritingFIleTitle));
                                }

                                //Creates a hash of the firebase user id and email for KeyStore alias name.
                                //This is done to generate a Alias with a random name tied to the users account
                                globalVariables.setUserKey(CryptographyFunctions.sha512((user.getUid() + email + password), activity));
                                //This sets the key to allow for decryption)
                                UserProfile.create_profile(userDataStringArray, (Activity) context);
                                //disables setup flag.
                                globalVariables.setSetUpFlag(false);
                                //Sets the users encryption key.
                                globalVariables.setUserKey(
                                        CryptographyFunctions.sha512(
                                                (user.getUid() + email + password), activity)
                                );
                                firebaseAuth.getCurrentUser();
                                user.sendEmailVerification()
                                        .addOnCompleteListener(activity, task1->{
                                            verifyEmail = true;
                                            if (task1.isSuccessful()) {
                                                MessageAndTextUtils.alert(activity,
                                                        null,
                                                        activity.getString(R.string.PleaseVerifyEmail),
                                                        activity.getString(R.string.PleaseVerifyEmail));
                                            } else {
                                                MessageAndTextUtils.error_alert(activity,null,
                                                        activity.getString(R.string.ErrorSendingVeriEmail),
                                                        activity.getString(R.string.VeriEmailNotSent));
                                            }
                                });

                            } else {
                                // Displays the sign up error to the user
                                MessageAndTextUtils.error_alert((Activity) context, null,
                                        Objects.requireNonNull(
                                                task.getException()).toString().split(":")[1]
                                        , context.getString(R.string.sign_up_error));

                            }
                        });
            } else {
                // Notifies the user their was a caption error
                MessageAndTextUtils.error_alert(
                        (Activity) context, null,
                        Message,
                        activity.getString(R.string.captionError));

            }
        }
    };
    /**
     * This is the gamer type test on click method changing the display to the gamer type test page.
     */
    private static final View.OnClickListener take_test_listener = v -> {
        Activity activity = (Activity) v.getContext();
        (activity).setContentView(R.layout.activity_gamer_type_test);
        // Sets a onclick listener for getting the sterotype results
        activity.findViewById(R.id.gamer_type_get_results_button).setOnClickListener(v1 -> {
            // Creates a array of spinner objects getting the values from the user filled in survey
            Spinner[] Items = {
                    activity.findViewById(R.id.Philanthropist_1), activity.findViewById(R.id.Philanthropist_2), activity.findViewById(R.id.Philanthropist_3), activity.findViewById(R.id.Philanthropist_4),
                    activity.findViewById(R.id.Achiever_1), activity.findViewById(R.id.Achiever_2), activity.findViewById(R.id.Achiever_3), activity.findViewById(R.id.Achiever_4),
                    activity.findViewById(R.id.Disruptor_1), activity.findViewById(R.id.Disruptor_2), activity.findViewById(R.id.Disruptor_3), activity.findViewById(R.id.Disruptor_4),
                    activity.findViewById(R.id.Player_1), activity.findViewById(R.id.Player_2), activity.findViewById(R.id.Player_3), activity.findViewById(R.id.Player_4),
                    activity.findViewById(R.id.Free_Spirit_1), activity.findViewById(R.id.Free_Spirit_2), activity.findViewById(R.id.Free_Spirit_3), activity.findViewById(R.id.Free_Spirit_4),
                    activity.findViewById(R.id.Socialiser_1), activity.findViewById(R.id.Socialiser_2), activity.findViewById(R.id.Socialiser_3), activity.findViewById(R.id.Socialiser_4),
            };
            gamer_type[0] = 0;
            StringBuilder errorString = new StringBuilder(" ");
            // Itter though each item in the spinner array and gets its value 
            for (int i = 0; i < Items.length; i++) {
                int temp = Items[i].getSelectedItemPosition();
                int current_points = 0;
                switch (temp) {
                    case 0:
                        errorString.append(" ").append(i + 1);
                        break;
                    case 1:
                        current_points = 3;
                        break;
                    case 2:
                        current_points = 2;
                        break;
                    case 3:
                        current_points = 1;
                        break;
                    case 4:
                        current_points = 0;
                        break;
                    case 5:
                        current_points = -1;
                        break;
                    case 6:
                        current_points = -2;
                        break;
                    case 7:
                        current_points = -3;
                        break;
                }
                // Checks its index and adds it to a current point int array depending on the users input will determine which gamer type is recommended 
                if (i < 4) {
                    gamer_type[0] = gamer_type[0] + current_points;
                } else if (i < 8) {
                    gamer_type[1] = gamer_type[1] + current_points;
                } else if (i < 12) {
                    gamer_type[2] = gamer_type[2] + current_points;
                } else if (i < 16) {
                    gamer_type[3] = gamer_type[3] + current_points;
                } else if (i < 20) {
                    gamer_type[4] = gamer_type[4] + current_points;
                } else {
                    gamer_type[5] = gamer_type[5] + current_points;
                }
            }
            // Displays a error message if the user has not filled out the whole questionnaire 
            if (!errorString.toString().equals(" ")) {
                MessageAndTextUtils.error_message(activity,
                        null, (dialog, which) -> {
                            gamer_type[0] = (-100);
                            setup(activity);
                        },
                        activity.getString(R.string.QuestionsNotAnsweredSignUp)
                                + errorString +
                                activity.getString(R.string.OrPressCancelSignUp),
                        activity.getString(R.string.QuestionsNotAnswered));
                return;
            }
            setup(activity);
        });
    };
    /**
     * This listener will open the android accessibility settings due to android not allowing applications to change these settings. This allows the user to change the settings with a quick button in the application.
     */
    private static final View.OnClickListener setAccessibility = v -> {
        // cleans up code a bit.
        Activity activity = (Activity) v.getContext();
        // Notifies the user and takes them to the accessibility menu
        MessageAndTextUtils.alert(activity,
                (dialog, which) ->
                        // Starts the android accessibility menu
                        activity.startActivityForResult(
                                new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS),
                                0),
                activity.getString(R.string.acessability_message_to_settings),
                activity.getString(R.string.Accessibility));
    };

    /**
     * This method will create the user profile and caption the user and sign them up for the application. It will also create a user type graph if done the
     * gamer type test.
     *
     * @param activity this is the current calling activity and is used to create dialog
     */
    private static void setup(Activity activity) {
        activity.setContentView(R.layout.activity_user_info); // Changes activity to the sign up user information page
        firebaseAuth = FirebaseAuth.getInstance();// Init firebase
        RadarChart radar_chart = activity.findViewById(R.id.gamer_type_chart); // Gets the radar chart for gamer type test. If not done used to hide chart
        //Creates the gamer type spin box and populates it.
        final Spinner gamerTypeSpinbox = activity.findViewById(R.id.gamer_type_spin);
        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(activity, R.array.Gamer_Types, android.R.layout.simple_spinner_dropdown_item);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        gamerTypeSpinbox.setAdapter(adapter2);

        // Checks if the test has been preformed or not.
        if (gamer_type[0] != -100) {
            int highest = 0, highest_index = 0;
            int second_highest = 0, second_highest_index = 0;
            // Finds the highest and second highest score
            for (int i = 0; i < 6; i++) {
                if (highest < gamer_type[i]) {
                    highest = gamer_type[i];
                    highest_index = i;
                } else if (second_highest < gamer_type[i] && second_highest < highest) {
                    second_highest = gamer_type[i];
                    second_highest_index = i;
                }
            }
            // all are equal so select the first two
            if (highest_index == second_highest_index) {
                second_highest_index = 1;
            }
            // Notifies the user what their main and secondary gamer type is
            MessageAndTextUtils.alert(activity, null,
                    activity.getString(R.string.GamerTypeTestYouAre) +
                            get_gamer_type(highest_index) +
                            activity.getString(R.string.mainAndSecondayGamerTypeDialog) +
                            get_gamer_type(second_highest_index),
                    activity.getString(R.string.GamerTypeResults));

            // sets the gamer type spin box to the highers result from the test.
            gamerTypeSpinbox.setSelection(highest_index);
            int color = Color.rgb(120, 120, 120);
            //Creates the radar chart for gamer type.
            GraphMaker.get_radar_chart(radar_chart, color,
                    new String[]{"Philanthropists", "Achievers", "Disruptors", "Players", "Free Spirits", "Socialisers"},
                    gamer_type, "Gamer Type");
        }
        // Hides the chart if their is no data in the graph eg the test wasnt done
        if (radar_chart.isEmpty()) {
            radar_chart.setVisibility(View.GONE);
        }
        // Creates and populates the gender spinner box
        final Spinner genderSpinbox = activity.findViewById(R.id.Gender_Spin);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(activity, R.array.Gender, android.R.layout.simple_spinner_dropdown_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        genderSpinbox.setAdapter(adapter);
        // Add the other gender text box when the other option is specified
        genderSpinbox.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                EditText other_gender = activity.findViewById(R.id.Other_Gender_txt);
                if (genderSpinbox.getSelectedItem().equals("Other Please Specify")) {
                    other_gender.setVisibility(View.VISIBLE);
                } else {
                    other_gender.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        // Finds the required objects in the UI to get and check the data.
        final EditText emailTextBox = activity.findViewById(R.id.signup_email_txtbox);
        final EditText passwordTextBox = activity.findViewById(R.id.signup_password_txtbox);
        final EditText passwordConfirmTextBox = activity.findViewById(R.id.signup_password_confirm_txtbox);
        final TextView ageTextBox = activity.findViewById(R.id.age_txtbox);
        final EditText otherGenderTextBox = activity.findViewById(R.id.Other_Gender_txt);
        final EditText nameTextBox = activity.findViewById(R.id.name_txtbox);
        // Sign up button on click event checks the data inputted to see if vail if not
        // Then a dialog error message is displayed
        activity.findViewById(R.id.sign_up_btn).setOnClickListener(view -> {
            // Checks the gamer type is valid and is a supported gamer type
            int gamerTypePosition = gamerTypeSpinbox.getSelectedItemPosition();
            if (gamerTypePosition == 0) {
                MessageAndTextUtils.error_alert(activity, null,
                        activity.getString(R.string.unsupportedGamerType),
                        activity.getString(R.string.SignUpErrorString));
                return;
            } else if (gamerTypePosition == 2) {
                MessageAndTextUtils.error_alert(activity, null,
                        activity.getString(R.string.disrupterGamerTypeSelected),
                        activity.getString(R.string.SignUpErrorString));
                return;
            }
            // Checks if the email box is empty
            if (emailTextBox.getText().toString().isEmpty()) {
                // gives a nice red error box
                emailTextBox.setError(activity.getString(R.string.EmailAddressEmpty));
                // creates a dialog box notifying the user of the issue
                MessageAndTextUtils.error_alert(activity, null,
                        activity.getString(R.string.EmailAddressEmpty),
                        activity.getString(R.string.SignUpErrorString));
                return;
            }
            // Basic regex check to see if the email is vail. This could be improved by domain validation and whitelisting email domains.
            if (!emailTextBox.getText().toString().matches("^(.+)@(.+)$")) {
                // gives a nice red error box
                emailTextBox.setError(activity.getString(R.string.InvalidEmail));
                // creates a dialog box notifying the user of the issue
                MessageAndTextUtils.error_alert(activity, null,
                        activity.getString(R.string.InvalidEmail),
                        activity.getString(R.string.SignUpErrorString));
                return;
            }
            // This checks if the entered age is valid
            if (ageTextBox.getText().toString().isEmpty() || Integer.parseInt(ageTextBox.getText().toString()) < 12 || Integer.parseInt(ageTextBox.getText().toString()) > 120) {
                // gives a nice red error box
                ageTextBox.setError(activity.getString(R.string.invalidAge));
                // creates a dialog box notifying the user of the issue
                MessageAndTextUtils.error_alert(activity, null,
                        activity.getString(R.string.invalidAge),
                        activity.getString(R.string.SignUpErrorString));
                return;
            }
            if (passwordTextBox.getText().toString().isEmpty()) {
                passwordTextBox.setError(activity.getString(R.string.PasswordMustNotBeEmpty));
                MessageAndTextUtils.error_alert(activity, null,
                        activity.getString(R.string.PasswordMustNotBeEmpty),
                        activity.getString(R.string.SignUpErrorString));
                return;
            }

            if (!passwordTextBox.getText().toString().matches("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$")) {
                passwordTextBox.setError(activity.getString(R.string.passwordRequirements));
                MessageAndTextUtils.error_alert(activity, null,
                        activity.getString(R.string.passwordRequirements),
                        activity.getString(R.string.SignUpErrorString));
                return;
            }

            if (genderSpinbox.getSelectedItem().equals("")) {
                otherGenderTextBox.setError(activity.getString(R.string.SelectAGender));
                MessageAndTextUtils.error_alert(activity, null,
                        activity.getString(R.string.EmptyBoxError),
                        activity.getString(R.string.SignUpErrorString));
                return;
            }

            if (genderSpinbox.getSelectedItem().equals("Other Please Specify") && otherGenderTextBox.getText().toString().isEmpty()) {
                otherGenderTextBox.setError(activity.getString(R.string.EmptyBoxError));
                MessageAndTextUtils.error_alert(activity, null,
                        activity.getString(R.string.EmptyBoxError),
                        activity.getString(R.string.SignUpErrorString));
                return;
            }

            if (passwordConfirmTextBox.getText().toString().compareTo(passwordTextBox.getText().toString()) != 0) {
                passwordTextBox.setError(activity.getString(R.string.PasswordNotMatchedSignUp));
                MessageAndTextUtils.error_alert(activity, null,
                        activity.getString(R.string.PasswordNotMatchedSignUp),
                        activity.getString(R.string.SignUpErrorString));
                return;
            }

            email = emailTextBox.getText().toString();
            password = passwordTextBox.getText().toString();

            int level_count = Level.getLevel(globalVariables, activity);

            String gender;
            if (genderSpinbox.getSelectedItem().equals("Other Please Specify")) {
                gender = otherGenderTextBox.getText().toString();
            } else {
                gender = genderSpinbox.getSelectedItem().toString();
            }
            int level_number_of_opens = 0, leaderbord_number_of_opens = 0, achivements_number_of_opens = 0, note_number_of_opens = 0;

            JSONObject personality = new JSONObject();
            try {
                personality = GamerTypeAndPersonality.initStereotype(gamerTypeSpinbox.getSelectedItem().toString());
            } catch (Exception e) {
                MessageAndTextUtils.error_alert(activity, null,
                        activity.getString(R.string.errorCreatingSterotype) + e.toString(),
                        activity.getString(R.string.SignupErrorTitle));
            }

            if (!globalVariables.isEmulator()) {
                Calendar today = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
                long end_time = today.getTimeInMillis();
                SamsungHealth health = globalVariables.getSamsungHealth();
                userDataStringArray = new String[]{
                        nameTextBox.getText().toString(),
                        ageTextBox.getText().toString(),
                        gender,
                        gamerTypeSpinbox.getSelectedItem().toString(),
                        String.valueOf(health.getTotalStepCount()),
                        String.valueOf(level_count),
                        String.valueOf(health.getSleepStreak()),
                        String.valueOf(health.getMaxSleepStreak()),
                        String.valueOf(0),
                        String.valueOf(level_number_of_opens),
                        String.valueOf(leaderbord_number_of_opens),
                        String.valueOf(achivements_number_of_opens),
                        String.valueOf(note_number_of_opens),
                        String.valueOf(0),
                        String.valueOf(end_time),
                        String.valueOf(end_time),
                        String.valueOf(end_time),
                        String.valueOf(end_time),
                        String.valueOf(end_time),
                        String.valueOf(end_time),
                        String.valueOf(health.getTotalCaloriesConsumed()),
                        String.valueOf(health.getTotalCaloriesBurned()),
                        health.getFoodNutritionInformation().toString(),
                        health.getExercisesDataJSONArray().toString(),
                        health.getHeartDataJSONArray().toString(),
                        health.getSleepDataJSONArray().toString(),
                        personality.toString(),
                };
            }
            // checks if the user has verified their email or not after a successful signup attempt.
            if (verifyEmail){
                //reloads the user profile to check if verified.
                user.reload().addOnCompleteListener(activity, task -> {
                    // logs user in
                    if (user.isEmailVerified()){
                        Intent intent = new Intent(activity, Home.class);
                        activity.startActivity(intent);
                    }
                    // checks if it reaches the limit
                    if (emailSend==MAX_EMAIL_RESEND){
                        MessageAndTextUtils.alert(activity, (dialog, which) -> {
                                    user.delete();
                                    emailSend=0;
                                    verifyEmail = false;
                                },
                                activity.getString(R.string.maxattepsEmailVerification),
                                activity.getString(R.string.maxAttempsEmailVerificationTitle));
                        return;
                    }
                    // allows the user to resend email.
                    MessageAndTextUtils.error_alert(activity, (dialog, which) ->
                                    user.sendEmailVerification()
                                            .addOnCompleteListener(activity, task1->{
                                                verifyEmail = true;
                                                if (task1.isSuccessful()) {
                                                    MessageAndTextUtils.alert(activity,
                                                            null,
                                                            activity.getString(R.string.PleaseVerifyEmail),
                                                            activity.getString(R.string.PleaseVerifyEmail));
                                                } else {
                                                    MessageAndTextUtils.error_alert(activity,null,
                                                            activity.getString(R.string.ErrorSendingVeriEmail),
                                                            activity.getString(R.string.VeriEmailNotSent));
                                                }
                                            })
                            , activity.getString(R.string.PleaseVerifyEmail),
                            activity.getString(R.string.EmailNotVerified));

                }).addOnFailureListener(activity, e ->
                        MessageAndTextUtils.error_alert(activity,null,
                        activity.getString(R.string.ErrorSendingVeriEmail)+e.toString(),
                        activity.getString(R.string.VeriEmailNotSent)));

             }else{
            SecurityVerification.captionTheUser(activity, captionResponseAndSignUp);
            }
        });
    }

    /**
     * Function to get the gamer type from a int value
     *
     * @param index This is the index of the gamer type
     * @return This will return the value of index as a string or will return null if its out of bounds.
     */
    private static String get_gamer_type(int index) {
        switch (index) {
            case 0:
                return "Philanthropist";
            case 1:
                return "Achiever";
            case 2:
                return "Disruptor";
            case 3:
                return "Player";
            case 4:
                return "Free Spirit";
            case 5:
                return "Socialiser";
        }
        return null;
    }

    /**
     * This method is the main setup activity that calls the gamer type test and the user info sighup activity
     *
     * @param activity this is a pointer to the current activity
     */
    public static void start_setup(Activity activity) {
        activity.setContentView(R.layout.activity_setup);
        globalVariables = (GlobalVariables) activity.getApplicationContext();
        Switch light_theme = activity.findViewById(R.id.theme_switch_Setup);
        // sets accessibility listener options
        light_theme.setOnClickListener(set_theme);
        activity.findViewById(R.id.Accessibility_Button_Setup).setOnClickListener(setAccessibility);
        boolean light;
        try {
            // gets the theme preference from the encrypted preferences
            light = Boolean.parseBoolean(
                    CryptographyFunctions.getFromEncryptedSharedPreferences(
                            activity, false,
                            "light_theme", "",
                            "false"));

            // If light mode is enabled set the light toggle to ticked
            if (light) {
                (light_theme).setChecked(true);
                // Fixes bug with the switch not changing text color
                light_theme.setTextColor(Color.BLACK);
            } else {
                light_theme.setTextColor(Color.WHITE);
            }

        } catch (GeneralSecurityException | IOException e) {
            e.printStackTrace();
        }
        //Takes the user to the test menu and checks their gamer type
        activity.findViewById(R.id.take_test_btn).setOnClickListener(take_test_listener);
        // Takes the user to the sign up page
        activity.findViewById(R.id.continue_to_setup).setOnClickListener(view -> setup(activity));
    }

}
