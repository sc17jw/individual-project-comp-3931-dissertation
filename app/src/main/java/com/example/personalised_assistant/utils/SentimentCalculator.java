package com.example.personalised_assistant.utils;

import android.util.Log;

/**
 * This class interprets the result from googles sentiment analysis and assigned a value positive negative and neutral.
 * The values in the class need to be adjusted to fit the purpose better but requires a bit sample size for this purpose
 */
public class SentimentCalculator {
    /**
     * This is the minimum magnitude value of the analysed text if it is below this then it will return unsure
     */
    public static final double MAGNITUDE_MINIMUM = 0.5;
    /**
     * This is value for it being a positive sentiment
     */
    private static final double SENTIMENT_POSITIVE_VALUE = 0.25;
    /**
     * This is value for it being a negative sentiment
     */
    private static final double SENTIMENT_NEGATIVE_VALUE = (-0.25);

    /**
     * This method will return the users feeling based upon the result from google cloud sentiment analysis
     *
     * @param magnitude This is the magnitude of the analysed text. This is a measurement of the emotional language used in the text
     * @param sentiment This is the sentiment of the analysed text. This is a value between -1 and 1 being positive and negative
     * @param note      This is the user note to get how many words it is to then normalise the data to fet a overall value
     * @return This will return a String that is one of 4 possibilities Positive ,  Negative , Neutral and Unable to tell based upon
     * the results and processing of the users input
     */
    public static String feeling(double magnitude, double sentiment, String note) {
        //Checks if the magnitude meets the minimum
        if (magnitude > MAGNITUDE_MINIMUM) {
            double normalised = normaliseSentiment(magnitude, sentiment, note);
            Log.d("TAG", "feeling: " + normalised);
            if (normalised > SENTIMENT_POSITIVE_VALUE) {
                return "Positive"; // the value is positive
            } else if (normalised < SENTIMENT_NEGATIVE_VALUE) {
                return "Negative"; // The value is negative
            } else if (normalised > SENTIMENT_NEGATIVE_VALUE && normalised < SENTIMENT_POSITIVE_VALUE) {
                return "Neutral"; // the value is neural
            }
        }
        return "Unable to tell"; // The magnitude is too low
    }

    /**
     * This method will normalise the date
     *
     * @param Magnitude This is the magnitude of the analysed text. This is a measurement of the emotional language used in the text
     * @param Sentiment This is the sentiment of the analysed text. This is a value between -1 and 1 being positive and negative
     * @param note      This is the user note to get how many words it is to then normalise the data to fet a overall value
     * @return This method will return the normalised value of the sentiment
     */
    public static double normaliseSentiment(double Magnitude, double Sentiment, String note) {
        return (note.split(" ").length / Magnitude) * Sentiment; // calculates the normalised sentiment value.
    }
}
