package com.example.personalised_assistant.utils;

import android.app.Activity;

import com.example.personalised_assistant.R;

import org.json.JSONObject;

/**
 * This class handel all the leveling system 
 */
class Level {
    /**
     * This method will get and return the users level as a int updating the profile if the users is in setup mode 
     * 
     * @param globalVariables This is the global variables object used to get variables
     * @param activity This is the current activity used to write the
     * @return this will return the users level 
     */
    static int getLevel(GlobalVariables globalVariables, Activity activity) {
        try {
            int current_level = 1; // inits the level to one 
            long steps;  
            JSONObject temp = null;
            // Loads the profile 
            if (!globalVariables.getSetUpFlag() || globalVariables.isEmulator()) {
                 // Gets the values from the user profile needed to create level 
                 steps = globalVariables.getUserProfile().getInt("total_steps");
                 current_level = globalVariables.getUserProfile().getInt("level");
                 temp = globalVariables.getUserProfile().getJSONObject("achievements");
            }else{
                // gets the step count from samsung health 
                steps = globalVariables.getSamsungHealth().getTotalStepCount(); 
            }
            // this calculates the users level
            while (steps>(current_level * 1000 * 1.25)){
                current_level++;
                // if not setup this will add the users new achievements to their user profile 
                if (!globalVariables.getSetUpFlag()) {
                    // makes sure temp != null 
                    assert temp != null;
                    // adds the achievement 
                    temp.put("level_achievement_" + current_level, "true");
                    temp.put("number_of_achievements", temp.getInt("number_of_achievements") + 1);
                }
            }
            // checks to see if the setup flag is set or if in emulator mode since step count doesn't change 
            if (!globalVariables.getSetUpFlag()|| globalVariables.isEmulator()) {
                // inserts the new achievements into the profile  
                globalVariables.insertObjectIntoProfile("achievements",temp,activity);
                // inserts the new level into the profile
                globalVariables.insertKeyIntoProfile("level",String.valueOf(current_level),activity);
                // writes the user profile to disk 
                globalVariables.writeUserProfileToDisk(activity);
            }
            return current_level;
        }catch (Exception e){
            MessageAndTextUtils.error_alert(activity,
                    null,
                    activity.getString(R.string.ErrorGenLevel)+e.getMessage(),
                    activity.getString(R.string.Level));
            return 1;
        }
    }

}
