package com.example.personalised_assistant.watch_service;

import android.app.Activity;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.widget.Toast;

import com.example.personalised_assistant.R;
import com.example.personalised_assistant.utils.GlobalVariables;
import com.example.personalised_assistant.utils.MessageAndTextUtils;
import com.samsung.android.sdk.accessory.SA;
import com.samsung.android.sdk.accessory.SAAgent;
import com.samsung.android.sdk.accessory.SAPeerAgent;
import com.samsung.android.sdk.accessory.SASocket;

/**
 * This class is used to communicate with a tizen watch
 */
public class WatchService extends SAAgent {
    private static final Class<WatchConnectionHandler> socket = WatchConnectionHandler.class;
    private final IBinder localBinder = new LocalBinder();
    private WatchConnectionHandler watchConnectionService = null;

    public WatchService() {
        super("WatchService",socket);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        SA samsungWatchObject = new SA();
        try {
            samsungWatchObject.initialize(this);
        } catch (Exception e) {
            MessageAndTextUtils.alert((Activity) getApplicationContext(),
                    null,"Could not connect to watch service Error: "+e.getMessage(),
                    " Samsung Watch");
            stopSelf();
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return localBinder;
    }

    @Override
    protected void onServiceConnectionRequested(SAPeerAgent availableDevices) {
        if (availableDevices != null) {
            acceptServiceConnectionRequest(availableDevices);
        }
    }


    @Override
    protected void onError(SAPeerAgent available_devices, String errorMessage, int errorCode) {
        super.onError(available_devices, errorMessage, errorCode);
    }

    @Override
    protected void onServiceConnectionResponse(SAPeerAgent available_devices, SASocket socket, int result) {
        if (result == SAAgent.CONNECTION_SUCCESS) {
            this.watchConnectionService = (WatchConnectionHandler) socket;
            MessageAndTextUtils.alert((Activity) getApplicationContext(),null,"Connected to watch","Watch");
        } else {
            MessageAndTextUtils.alert((Activity) getApplicationContext(),null,"Failed To Connect To Watch","Watch");
        }
    }

    @Override
    protected void onFindPeerAgentsResponse(SAPeerAgent[] availableDevices, int foundDevices) {
        if ((foundDevices == SAAgent.PEER_AGENT_FOUND) && (availableDevices != null)) {
            for(SAPeerAgent peerAgent:availableDevices)
                requestServiceConnection(peerAgent);
        }
    }

    public class LocalBinder extends Binder {
        public WatchService getService() {
            return WatchService.this;
        }
    }

    /**
     * This method searches for available watches.
     */
    public void findPeers() {
        findPeerAgents();
    }

    /**
     * This method allows for data to be sent to the watch
     * @param data This is the data you want to send to the watch in a byte[] format
     * @return returns if the data was successfully send true else false
     */
    public boolean sendData(byte[] data, Activity activity) {
        boolean return_value = false;
        if (watchConnectionService != null) {
            try {
                watchConnectionService.send(getServiceChannelId(0), data);
                return_value = true;
            } catch (Exception e) {
                MessageAndTextUtils.alert(activity, null,getString(R.string.data_sending_watch_error), getString(R.string.watch_service));
                e.printStackTrace();
            }
        }
        return return_value;
    }


    class WatchConnectionHandler extends SASocket {
        WatchConnectionHandler() {
            super(WatchConnectionHandler.class.getName());
        }

        /**
         * This is the error function that is called if the sending data to the watch throws a error.
         * @param channelId This is the channel that the watch is connected on.
         * @param errorOnConnectionMessage This is the error message the watch has thrown
         * @param errorOnConnectionCode This is the error code that the watch has thrown
         */
        @Override
        public void onError(int channelId, String errorOnConnectionMessage, int errorOnConnectionCode) {
            MessageAndTextUtils.alert((Activity) getApplicationContext(),
                    null, getString(R.string.error_watch_connection)+
                            errorOnConnectionMessage+getString(R.string.code)+errorOnConnectionCode,
                    " Samsung Watch");
        }

        /**
         * This method is called when the watch sends data.
         * @param channelId This is the channel id that the watch is connected on
         * @param data This is the returned data from the watch
         */
        @Override
        public void onReceive(int channelId, byte[] data) {
            final String message = new String(data);
            GlobalVariables globalVariables = (GlobalVariables) getBaseContext();
            globalVariables.setWatchData(data);
            MessageAndTextUtils.alert((Activity) getApplicationContext(), null,getString(R.string.data_sending_watch_error), getString(R.string.watch_service));

            Toast.makeText(getBaseContext(),message, Toast.LENGTH_LONG).show();
        }

        @Override
        protected void onServiceConnectionLost(int reason) {
            watchConnectionService.close();
            watchConnectionService = null;
        }
    }
}

